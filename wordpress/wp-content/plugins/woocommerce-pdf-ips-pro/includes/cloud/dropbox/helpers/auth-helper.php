<?php

namespace WPO\WC\PDF_Invoices_Pro\Cloud\Dropbox\Helpers;

use WPO\WC\PDF_Invoices_Pro\Vendor\Stevenmaguire\OAuth2\Client\Provider\Dropbox;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( '\\WPO\\WC\\PDF_Invoices_Pro\\Cloud\\Dropbox\\Helpers\\Auth_Helper' ) ) :

class Auth_Helper extends Dropbox
{

	public function getBaseAuthorizationUrl()
	{
		return 'https://www.dropbox.com/oauth2/authorize';
	}

	public function getAuthorizationUrl( $options = array() )
	{
		$options['token_access_type'] = 'offline';
		return parent::getAuthorizationUrl( $options );
	}

}

endif; // class_exists
