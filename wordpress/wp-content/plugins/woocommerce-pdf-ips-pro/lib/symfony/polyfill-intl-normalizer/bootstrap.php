<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Modified by __root__ on 27-May-2021 using Strauss.
 * @see https://github.com/BrianHenryIE/strauss
 */

use WPO\WC\PDF_Invoices_Pro\Vendor\Symfony\Polyfill\Intl\WPO_WCPDF_IPS_PRO_Normalizer as p;

if (\PHP_VERSION_ID >= 80000) {
    return require __DIR__.'/bootstrap80.php';
}

if (!function_exists('normalizer_is_normalized')) {
    function normalizer_is_normalized($string, $form = p\WPO_WCPDF_IPS_PRO_Normalizer::FORM_C) { return p\WPO_WCPDF_IPS_PRO_Normalizer::isNormalized($string, $form); }
}
if (!function_exists('normalizer_normalize')) {
    function normalizer_normalize($string, $form = p\WPO_WCPDF_IPS_PRO_Normalizer::FORM_C) { return p\WPO_WCPDF_IPS_PRO_Normalizer::normalize($string, $form); }
}
