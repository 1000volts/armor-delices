<?php
/**
 * @license MIT
 *
 * Modified by __root__ on 27-May-2021 using Strauss.
 * @see https://github.com/BrianHenryIE/strauss
 */

class WPO_WCPDF_IPS_PRO_Normalizer extends WPO\WC\PDF_Invoices_Pro\Vendor\Symfony\Polyfill\Intl\WPO_WCPDF_IPS_PRO_Normalizer\WPO_WCPDF_IPS_PRO_Normalizer
{
    /**
     * @deprecated since ICU 56 and removed in PHP 8
     */
    public const NONE = 2;
    public const FORM_D = 4;
    public const FORM_KD = 8;
    public const FORM_C = 16;
    public const FORM_KC = 32;
    public const NFD = 4;
    public const NFKD = 8;
    public const NFC = 16;
    public const NFKC = 32;
}
