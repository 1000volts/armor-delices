<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Modified by __root__ on 27-May-2021 using Strauss.
 * @see https://github.com/BrianHenryIE/strauss
 */

use WPO\WC\PDF_Invoices_Pro\Vendor\Symfony\Polyfill\Intl\WPO_WCPDF_IPS_PRO_Normalizer as p;

if (!function_exists('normalizer_is_normalized')) {
    function normalizer_is_normalized(?string $string, ?int $form = p\WPO_WCPDF_IPS_PRO_Normalizer::FORM_C): bool { return p\WPO_WCPDF_IPS_PRO_Normalizer::isNormalized((string) $string, (int) $form); }
}
if (!function_exists('normalizer_normalize')) {
    function normalizer_normalize(?string $string, ?int $form = p\WPO_WCPDF_IPS_PRO_Normalizer::FORM_C): string|false { return p\WPO_WCPDF_IPS_PRO_Normalizer::normalize((string) $string, (int) $form); }
}
