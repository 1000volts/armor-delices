<?php

/**
 * This file is part of FPDI
 *
 * @package   setasign\Fpdi
 * @copyright Copyright (c) 2020 Setasign GmbH & Co. KG (https://www.setasign.com)
 * @license   http://opensource.org/licenses/mit-license The MIT License
 *
 * Modified by __root__ on 27-May-2021 using Strauss.
 * @see https://github.com/BrianHenryIE/strauss
 */

namespace WPO\WC\PDF_Invoices_Pro\Vendor\setasign\Fpdi;

/**
 * Class TcpdfFpdi
 *
 * This class let you import pages of existing PDF documents into a reusable structure for WPO_WCPDF_IPS_PRO_TCPDF.
 *
 * @deprecated Class was moved to \setasign\Fpdi\Tcpdf\Fpdi
 */
class TcpdfFpdi extends \WPO\WC\PDF_Invoices_Pro\Vendor\setasign\Fpdi\Tcpdf\Fpdi
{
    // this class is moved to \setasign\Fpdi\Tcpdf\Fpdi
}
