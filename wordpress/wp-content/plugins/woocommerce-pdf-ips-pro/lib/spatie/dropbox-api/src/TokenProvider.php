<?php
/**
 * @license MIT
 *
 * Modified by __root__ on 27-May-2021 using Strauss.
 * @see https://github.com/BrianHenryIE/strauss
 */

namespace WPO\WC\PDF_Invoices_Pro\Vendor\Spatie\Dropbox;

interface TokenProvider
{
    public function getToken(): string;
}
