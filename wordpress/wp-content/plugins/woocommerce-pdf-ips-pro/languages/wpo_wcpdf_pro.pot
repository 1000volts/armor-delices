# Copyright (C) 2021 Ewout Fernhout
# This file is distributed under the same license as the WooCommerce PDF Invoices & Packing Slips Professional plugin.
msgid ""
msgstr ""
"Project-Id-Version: WooCommerce PDF Invoices & Packing Slips Professional 2.7.4\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woocommerce-pdf-ips-pro\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-04-19T11:36:39-04:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: wpo_wcpdf_pro\n"

#. Plugin Name of the plugin
msgid "WooCommerce PDF Invoices & Packing Slips Professional"
msgstr ""

#. Plugin URI of the plugin
#. Author URI of the plugin
msgid "https://www.wpovernight.com/"
msgstr ""

#. Description of the plugin
msgid "Extended functionality for the WooCommerce PDF Invoices & Packing Slips plugin"
msgstr ""

#. Author of the plugin
msgid "Ewout Fernhout"
msgstr ""

#: includes/cloud/abstract-wcpdf-cloud-api.php:78
msgid "Dropbox"
msgstr ""

#: includes/cloud/abstract-wcpdf-cloud-api.php:84
msgid "Google Drive"
msgstr ""

#: includes/cloud/abstract-wcpdf-cloud-api.php:90
msgid "OneDrive"
msgstr ""

#: includes/cloud/abstract-wcpdf-cloud-api.php:151
msgid "Authorize %s cloud service!"
msgstr ""

#: includes/cloud/abstract-wcpdf-cloud-api.php:152
msgid "Visit %s via %sthis link%s to get an access code and enter this below:"
msgstr ""

#: includes/cloud/abstract-wcpdf-cloud-api.php:156
msgid "Authorize"
msgstr ""

#: includes/cloud/abstract-wcpdf-cloud-api.php:172
msgid "%s connection established! Access token: %s"
msgstr ""

#: includes/cloud/abstract-wcpdf-cloud-api.php:185
#: includes/wcpdf-pro-cloud-storage.php:206
#: includes/wcpdf-pro-cloud-storage.php:840
#: includes/wcpdf-pro-cloud-storage.php:1077
msgid "View logs"
msgstr ""

#: includes/cloud/abstract-wcpdf-cloud-api.php:186
msgid "%s authentication failed. Please try again or check the logs for details: %s"
msgstr ""

#: includes/cloud/gdrive/gdrive-api.php:575
msgid "A parent directory in GDrive was trashed, the file was transfered to the GDrive root directory."
msgstr ""

#: includes/cloud/templates/template-bulk-export-page.php:6
msgid "%s export"
msgstr ""

#: includes/cloud/templates/template-bulk-export-process.php:5
msgid "%s export finished"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:32
#: includes/documents/class-wcpdf-credit-note.php:41
#: includes/legacy/wcpdf-pro-legacy-functions.php:55
msgid "Credit Note"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:48
#: includes/legacy/wcpdf-pro-legacy-functions.php:81
msgid "credit-note"
msgid_plural "credit-notes"
msgstr[0] ""
msgstr[1] ""

#: includes/documents/class-wcpdf-credit-note.php:150
msgid "Display credit note date"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:158
msgid "Credit Note Date"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:159
msgid "Refund Date"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:166
msgid "Display credit note number"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:177
#: includes/documents/class-wcpdf-proforma.php:203
msgid "Number sequence"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:184
#: includes/documents/class-wcpdf-proforma.php:210
msgid "Main invoice numbering"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:185
msgid "Separate credit note numbering"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:193
msgid "Show original invoice number"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:204
msgid "Next credit note number (without prefix/suffix etc.)"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:245
msgid "Reset credit note number yearly"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:293
msgid "Use positive prices"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:299
msgid "Prices in Credit Notes are negative by default, but some countries (like Germany) require positive prices."
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:305
msgid "Use products & totals fallback"
msgstr ""

#: includes/documents/class-wcpdf-credit-note.php:311
msgid "If orders are refunded without setting products, credit notes will not contain these details. This option provides a fallback method by using data from the original order for the credit note. This may cause issues in some setups, so testing is recommended."
msgstr ""

#: includes/documents/class-wcpdf-proforma.php:32
#: includes/documents/class-wcpdf-proforma.php:41
#: includes/legacy/wcpdf-pro-legacy-functions.php:52
msgid "Proforma Invoice"
msgstr ""

#: includes/documents/class-wcpdf-proforma.php:47
#: includes/legacy/wcpdf-pro-legacy-functions.php:77
msgid "proforma-invoice"
msgid_plural "proforma-invoices"
msgstr[0] ""
msgstr[1] ""

#: includes/documents/class-wcpdf-proforma.php:176
msgid "Display proforma invoice date"
msgstr ""

#: includes/documents/class-wcpdf-proforma.php:184
msgid "Proforma Invoice Date"
msgstr ""

#: includes/documents/class-wcpdf-proforma.php:192
msgid "Display proforma invoice number"
msgstr ""

#: includes/documents/class-wcpdf-proforma.php:211
msgid "Separate proforma numbering"
msgstr ""

#: includes/documents/class-wcpdf-proforma.php:219
msgid "Next proforma invoice number (without prefix/suffix etc.)"
msgstr ""

#: includes/documents/class-wcpdf-proforma.php:260
msgid "Reset proforma invoice number yearly"
msgstr ""

#: includes/documents/class-wcpdf-proforma.php:278
msgid "Only when the final invoice is not available"
msgstr ""

#: includes/documents/class-wcpdf-proforma.php:279
msgid "Only when a proforma invoice is already created/emailed"
msgstr ""

#: includes/email-customer-credit-note.php:32
msgid "Customer Credit Note"
msgstr ""

#: includes/email-customer-credit-note.php:33
msgid "Customer Credit Note emails can be sent to the user with the PDF Credit Note attached."
msgstr ""

#: includes/email-customer-credit-note.php:39
msgid "Credit Note for order {order_number} from {order_date}"
msgstr ""

#: includes/email-customer-credit-note.php:40
msgid "Credit Note for order {order_number}"
msgstr ""

#: includes/email-customer-credit-note.php:41
msgid "A refund has been issued for your order, attached to this email you will find a credit note with the details."
msgstr ""

#: includes/email-customer-credit-note.php:194
msgid "Automatically send"
msgstr ""

#: includes/email-customer-credit-note.php:196
msgid "Automatically send email when order status is set to refunded"
msgstr ""

#: includes/email-customer-credit-note.php:200
#: includes/email-pdf-order-notification.php:239
msgid "Email subject"
msgstr ""

#: includes/email-customer-credit-note.php:202
#: includes/email-customer-credit-note.php:209
#: includes/email-customer-credit-note.php:217
#: includes/email-pdf-order-notification.php:241
#: includes/email-pdf-order-notification.php:254
#: includes/email-pdf-order-notification.php:263
msgid "Defaults to <code>%s</code>"
msgstr ""

#: includes/email-customer-credit-note.php:207
#: includes/email-pdf-order-notification.php:252
msgid "Email heading"
msgstr ""

#: includes/email-customer-credit-note.php:214
#: includes/email-pdf-order-notification.php:260
msgid "Email body text"
msgstr ""

#: includes/email-customer-credit-note.php:222
#: includes/email-pdf-order-notification.php:283
msgid "Email type"
msgstr ""

#: includes/email-customer-credit-note.php:224
#: includes/email-pdf-order-notification.php:285
msgid "Choose which format of email to send."
msgstr ""

#: includes/email-customer-credit-note.php:228
#: includes/email-pdf-order-notification.php:289
msgid "Plain text"
msgstr ""

#: includes/email-customer-credit-note.php:229
#: includes/email-pdf-order-notification.php:290
msgid "HTML"
msgstr ""

#: includes/email-customer-credit-note.php:230
#: includes/email-pdf-order-notification.php:291
msgid "Multipart"
msgstr ""

#: includes/email-pdf-order-notification.php:33
msgid "Order Notification"
msgstr ""

#: includes/email-pdf-order-notification.php:34
msgid "Order Notification emails can be sent to specified email addresses, automatically & manually."
msgstr ""

#: includes/email-pdf-order-notification.php:40
msgid "Order Notification for order {order_number} from {order_date}"
msgstr ""

#: includes/email-pdf-order-notification.php:41
msgid "Order Notification for order {order_number}"
msgstr ""

#: includes/email-pdf-order-notification.php:42
msgid "An order has been placed."
msgstr ""

#: includes/email-pdf-order-notification.php:217
msgid "Trigger"
msgstr ""

#: includes/email-pdf-order-notification.php:219
msgid "Choose the status that should trigger this email. Note that the 'Paid' status only works for automated payment gateways (Paypal, Stripe, etc), not for BACS, COD & Cheque."
msgstr ""

#: includes/email-pdf-order-notification.php:223
msgid "Manual"
msgstr ""

#: includes/email-pdf-order-notification.php:224
msgid "Order placed"
msgstr ""

#: includes/email-pdf-order-notification.php:225
msgid "Order processing"
msgstr ""

#: includes/email-pdf-order-notification.php:226
msgid "Order completed"
msgstr ""

#: includes/email-pdf-order-notification.php:227
msgid "Order paid"
msgstr ""

#: includes/email-pdf-order-notification.php:228
msgid "Order refunded"
msgstr ""

#: includes/email-pdf-order-notification.php:232
msgid "Recipient(s)"
msgstr ""

#: includes/email-pdf-order-notification.php:234
msgid "Enter recipients (comma separated) for this email. Use {customer} to send this email to the customer."
msgstr ""

#: includes/email-pdf-order-notification.php:246
msgid "Empty body"
msgstr ""

#: includes/email-pdf-order-notification.php:248
msgid "Don't include any text/html in the email body"
msgstr ""

#: includes/email-pdf-order-notification.php:269
msgid "Order items"
msgstr ""

#: includes/email-pdf-order-notification.php:271
msgid "Include order items table in email"
msgstr ""

#: includes/email-pdf-order-notification.php:276
msgid "Customer details"
msgstr ""

#: includes/email-pdf-order-notification.php:278
msgid "Include customer details in email"
msgstr ""

#: includes/views/bulk-export.php:30
msgid "From"
msgstr ""

#: includes/views/bulk-export.php:39
msgid "optional"
msgstr ""

#: includes/views/bulk-export.php:43
msgid "To"
msgstr ""

#: includes/views/bulk-export.php:51
msgid "Date type"
msgstr ""

#: includes/views/bulk-export.php:54
msgid "Order date"
msgstr ""

#: includes/views/bulk-export.php:55
msgid "Document date"
msgstr ""

#: includes/views/bulk-export.php:61
msgid "Filter status"
msgstr ""

#: includes/views/bulk-export.php:65
msgid "All statuses"
msgstr ""

#: includes/views/bulk-export.php:92
msgid "Only existing documents"
msgstr ""

#: includes/views/bulk-export.php:98
msgid "Skip free orders"
msgstr ""

#: includes/views/bulk-export.php:113
msgid "Download ZIP"
msgstr ""

#: includes/views/bulk-export.php:115
msgid "ZIP export disabled"
msgstr ""

#: includes/views/bulk-export.php:115
msgid "The PHP ZipArchive library could not found, contact your host to enable bulk downloading PDF files in a ZIP."
msgstr ""

#: includes/views/bulk-export.php:119
msgid "Export to %s"
msgstr ""

#: includes/views/bulk-export.php:129
msgid "Warning!"
msgstr ""

#: includes/views/bulk-export.php:129
msgid "zlib.output_compression is enabled in PHP for this site, this may cause issues when downloading ZIP files"
msgstr ""

#: includes/views/bulk-export.php:132
msgid "Only exporting a few orders? You can also export by selecting orders in the WooCommerce order overview and then select one of the actions from the bulk dropdown!"
msgstr ""

#: includes/wcpdf-pro-bulk-export.php:36
msgid "Bulk export"
msgstr ""

#: includes/wcpdf-pro-bulk-export.php:93
#: includes/wcpdf-pro-bulk-export.php:116
#: includes/wcpdf-pro-bulk-export.php:130
msgid "No orders found!"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:72
msgid "Cloud storage"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:91
msgid "Cloud storage settings"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:98
msgid "Enable"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:110
msgid "Cloud service"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:123
msgid "Upload all email attachments"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:146
msgid "Upload by order status"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:154
msgid "If you are already emailing the documents, leave these settings empty to avoid slowing down your site (use the setting above instead)"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:160
msgid "Destination folder"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:168
msgid "App folder (restricted access)"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:169
msgid "Main cloud service folder"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:171
msgid "Note: Reauthorization is required after changing this setting!"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:179
msgid "Enter a subfolder to use (optional)"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:187
msgid "Organize uploads in folders by year/month"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:199
msgid "Log all communication (debugging only!)"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:240
msgid "Connected to"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:243
msgid "Unlink %s account"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:623
msgid "Cloud service upload permission denied"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:630
msgid "File does not exist"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:637
msgid "Cloud service credentials not set"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:780
msgid "Please wait while your queued PDF documents are being uploaded to %s..."
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:782
msgid "Please wait while the upload queue is being cleared"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:807
msgid "Please wait while your PDF invoices are being uploaded to %s..."
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:809
msgid "Please wait while your PDF packing slips are being uploaded to %s..."
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:841
#: includes/wcpdf-pro-cloud-storage.php:1078
msgid "There were errors when trying to upload to %s, check the error log for details:"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:845
msgid "PDF invoices successfully uploaded to %s!"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:848
msgid "PDF packing slips successfully uploaded to %s!"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:851
#: includes/wcpdf-pro-cloud-storage.php:1080
msgid "PDF documents successfully uploaded to %s!"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:1082
msgid "Upload queue successfully cleared!"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:1101
msgid "Upload files"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:1102
msgid "Clear queue"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:1107
msgid "There are %s unfinished uploads in your the upload queue from WooCommerce PDF Invoices & Packing Slips to %s."
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:1130
#: includes/wcpdf-pro-cloud-storage.php:1131
msgid "PDF Invoices to"
msgstr ""

#: includes/wcpdf-pro-cloud-storage.php:1132
#: includes/wcpdf-pro-cloud-storage.php:1133
msgid "PDF Packing Slips to"
msgstr ""

#: includes/wcpdf-pro-functions.php:632
msgid "Packing Slip Number:"
msgstr ""

#: includes/wcpdf-pro-functions.php:641
msgid "Packing Slip Date:"
msgstr ""

#: includes/wcpdf-pro-functions.php:723
msgid "Original Invoice Number:"
msgstr ""

#: includes/wcpdf-pro-functions.php:976
msgid "%s (refund #%s) was marked as archived but not found on the server. A new version has been uploaded."
msgstr ""

#: includes/wcpdf-pro-functions.php:976
msgid "%s was marked as archived but not found on the server. A new version has been uploaded."
msgstr ""

#: includes/wcpdf-pro-settings.php:55
#: includes/wcpdf-pro-settings.php:61
msgid "Static files"
msgstr ""

#: includes/wcpdf-pro-settings.php:67
msgid "Select a file to attach"
msgstr ""

#: includes/wcpdf-pro-settings.php:68
msgid "Set file"
msgstr ""

#: includes/wcpdf-pro-settings.php:69
msgid "Remove file"
msgstr ""

#: includes/wcpdf-pro-settings.php:94
msgid "Address customization"
msgstr ""

#: includes/wcpdf-pro-settings.php:100
msgid "Billing address"
msgstr ""

#: includes/wcpdf-pro-settings.php:113
msgid "Shipping address"
msgstr ""

#: includes/wcpdf-pro-settings.php:126
msgid "Remove empty lines"
msgstr ""

#: includes/wcpdf-pro-settings.php:132
msgid "Enable this option if you want to remove empty lines left over from empty address/placeholder replacements"
msgstr ""

#: includes/wcpdf-pro-settings.php:138
msgid "Allow line breaks within custom fields"
msgstr ""

#: includes/wcpdf-pro-settings.php:166
msgid "Active user language"
msgstr ""

#: includes/wcpdf-pro-settings.php:174
msgid "Multilingual settings"
msgstr ""

#: includes/wcpdf-pro-settings.php:180
msgid "Document language"
msgstr ""

#: includes/wcpdf-pro-settings.php:187
msgid "Order/customer language"
msgstr ""

#: includes/wcpdf-pro-settings.php:188
msgid "Site default language"
msgstr ""

#: includes/wcpdf-pro-settings.php:214
msgid "Document title"
msgstr ""

#: includes/wcpdf-pro-settings.php:228
msgid "PDF filename"
msgstr ""

#: includes/wcpdf-pro-settings.php:238
msgid "Leave empty to use default. Placeholders like {{document_number}} and {{order_number}} can be used to include document numbers in the filename."
msgstr ""

#: includes/wcpdf-pro-settings.php:239
msgid "Warning! Your filename does not contain a unique identifier ({{order_number}}, {{document_number}}), this can lead to attachment mixups!"
msgstr ""

#: includes/wcpdf-pro-settings.php:248
msgid "Keep PDF on server"
msgstr ""

#: includes/wcpdf-pro-settings.php:254
msgid "Stores the PDF when generated for the first time and reloads this copy each time the document is requested. Please note this can take up considerable disk space on you server."
msgstr ""

#: includes/wcpdf-pro-settings.php:293
msgid "Display packing slip date"
msgstr ""

#: includes/wcpdf-pro-settings.php:304
msgid "Display packing slip number"
msgstr ""

#: includes/wcpdf-pro-settings.php:315
msgid "Next packing slip number (without prefix/suffix etc.)"
msgstr ""

#: includes/wcpdf-pro-settings.php:364
msgid "Only when a packing slip is already created/emailed"
msgstr ""

#: includes/wcpdf-pro-settings.php:381
msgid "Reset packing slip number yearly"
msgstr ""

#: includes/wcpdf-pro-settings.php:399
msgid "Subtract refunded item quantities from packing slip"
msgstr ""

#: includes/wcpdf-pro-settings.php:415
msgid "Hide virtual and downloadable products"
msgstr ""

#: includes/wcpdf-pro-settings.php:511
msgid "Save translations"
msgstr ""

#: includes/wcpdf-pro-settings.php:586
msgid "Translate"
msgstr ""

#: includes/wcpdf-pro-settings.php:613
msgid "<b>Warning!</b> Your WooCommerce PDF Invoices & Packing Slips template folder does not contain templates for credit notes and/or proforma invoices."
msgstr ""

#: includes/wcpdf-pro-settings.php:614
msgid "If you are using WP Overnight premium templates, please update to the latest version. Otherwise copy the template files located in %s and adapt them to your own template."
msgstr ""

#: includes/wcpdf-pro-settings.php:631
msgid "<b>Important note:</b> WooCommerce 2.2.7 or newer is required to print credit notes. You are currently using WooCommerce %s"
msgstr ""

#: includes/wcpdf-pro-settings.php:645
msgid "Pro"
msgstr ""

#: includes/wcpdf-pro-settings.php:719
msgid "Here you can modify the way the shipping and billing address are formatted in the PDF documents as well as add custom fields to them."
msgstr ""

#: includes/wcpdf-pro-settings.php:720
msgid "You can use the following placeholders in addition to regular text and html tags (like h1, h2, b):"
msgstr ""

#: includes/wcpdf-pro-settings.php:724
msgid "Billing fields"
msgstr ""

#: includes/wcpdf-pro-settings.php:725
msgid "Shipping fields"
msgstr ""

#: includes/wcpdf-pro-settings.php:726
msgid "Custom fields"
msgstr ""

#: includes/wcpdf-pro-settings.php:765
msgid "Leave empty to use the default formatting."
msgstr ""

#: includes/wcpdf-pro-writepanels.php:242
msgid "stored on server"
msgstr ""

#: includes/wcpdf-pro-writepanels.php:243
msgid "Yes"
msgstr ""

#: includes/wcpdf-pro-writepanels.php:243
msgid "No"
msgstr ""

#: templates/Simple/credit-note.php:54
msgid "Credit Note Number:"
msgstr ""

#: templates/Simple/credit-note.php:60
msgid "Credit Note Date:"
msgstr ""

#: templates/Simple/credit-note.php:114
msgid "Reason for refund"
msgstr ""

#: templates/Simple/proforma.php:54
msgid "Proforma Invoice Number:"
msgstr ""

#: templates/Simple/proforma.php:60
msgid "Proforma Invoice Date:"
msgstr ""

#: templates/Simple/proforma.php:118
msgid "Customer Notes"
msgstr ""

#: woocommerce-pdf-ips-pro.php:257
msgid "WooCommerce PDF Invoices & Packing Slips Professional requires %sWooCommerce%s to be installed & activated!"
msgstr ""

#: woocommerce-pdf-ips-pro.php:269
msgid "WooCommerce PDF Invoices & Packing Slips Professional requires PHP 5.6 or higher."
msgstr ""

#: woocommerce-pdf-ips-pro.php:270
#: woocommerce-pdf-ips-pro.php:283
msgid "How to update your PHP version"
msgstr ""

#: woocommerce-pdf-ips-pro.php:282
msgid "WooCommerce PDF Invoices & Packing Slips Professional <strong>Cloud Storage</strong> feature requires <strong>PHP 7.2.5 or higher</strong>."
msgstr ""

#: woocommerce-pdf-ips-pro.php:288
msgid "Hide this message"
msgstr ""

#: woocommerce-pdf-ips-pro.php:309
msgid "WooCommerce PDF Invoices & Packing Slips Professional requires at least version 2.0 of WooCommerce PDF Invoices & Packing Slips - get it %shere%s!"
msgstr ""
