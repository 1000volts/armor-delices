��    S      �      L      L     M     Q     Y  E   a     �  @   �     �     �          $     3     N  !   i     �     �     �  	   �  	   �     �     �     �       
   %     0  #   >  *   b     �     �     �     �     �  k   �  �   C  !   �     �  
   	      	  !   .	     P	     f	     t	     �	     �	     �	     �	  	   �	  	   
     
     )
     8
     K
     f
     v
     �
     �
     �
     �
     �
  +   �
          .  F   ;     �  M   �  
   �     �     �     
          &     9  �   K  -   �  /     '   ;  
   c  >   n     �  *   �  /   �  *   '  .   R  �  �     :     >     J  _   V  
   �  Q   �          %     @     N  )   b     �  +   �     �     �          ,     B     R     o  %   �     �     �     �  6   �  >   %     d     g     v  !   �  '   �  �   �  �   c  2   "      U     v  =   �  2   �  -   �     #  ,   3  2   `  5   �  "   �     �                    4     J  "   `     �     �  )   �     �     �  !        5  5   K     �     �  ^   �       _         �     �     �     �     �     �        �     2      1   3  2   e     �  I   �  +   �  /   "  =   R  9   �  F   �    To 12 hour 24 hour Additional content on processing email in case of local pickup orders Address An additional, optional address line for your business location. Apply & close Available variables: Background color Business Hours Business Hours header text Change status to Picked up Change status to Ready for pickup Content font color Content font size Content line height Customize Dark Font Display Time Format Display options Edit Pickup Location Email Notifications Email Type Email content Enable Picked up order status email Enable Ready for Pickup order status email From Go Pro Headers Background color Headers font color Headers font size Hi there. we thought you'd like to know that your recent order from {site_title} has been ready for pickup. Hi {customer_first_name}. Thank you for picking up your {site_title} order #{order_number}. We hope you enjoyed your shopping experience. Hide Local Pickup Section Heading Hide Table Headers Light Font Local Pickup Order Status Emails Local Pickup section heading text Local pickup workflow Location Name Mark order as picked up Mark order as ready for pickup My Account (orders history) Name & Special Instructions Order received page Picked Up Picked up Picked up (%s) Picked up (%s) Pickup Address Pickup Instruction Pickup Instructions Layout Pickup Location Pickup Locations Pickup address header text Please select a order... Preview order Processing order email Ready for Pickup Ready for Pickup (%s) Ready for Pickup (%s) Ready for pickup Save & close Select an order to preview and design the pickup instructions display. Select email type Select time format which you want to display in business hours for customers. Send Email Settings Special Instruction TABLE CONTENT TABLE HEADERS Table Border color Table Border size The Advanced Local Pickup (ALP) helps you handle local pickup orders more conveniently by extending the WooCommerce Local Pickup shipping method. The location name for your business location. The phone number for your business information. The special instruction for your store. Work Hours You will receive an email when your order is ready for pickup. Your Order is Ready for pickup Your order from {site_title} was picked up Your {site_title} order is now Ready for pickup the select for working days of your store. you must add location name and save to proceed Project-Id-Version: Advanced Local Pickup for WooCommerce
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-09-24 05:09+0000
PO-Revision-Date: 2021-08-12 09:42+0000
Last-Translator: 
Language-Team: עִבְרִית
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.0; wp-5.7-alpha-49931  ל 12 שעות 24 שעות תוכן נוסף לעיבוד דוא"ל במקרה של הזמנות איסוף מקומיות כתובת שורת כתובת אופציונלית נוספת למיקום העסק שלך. החל וסגור משתנים זמינים: צבע רקע שעות עסקים טקסט כותרת שעות פעילות שנה סטטוס לאסוף שנה סטטוס ל- מוכן לאיסוף צבע גופן התוכן גודל גופני התוכן גובה שורת התוכן התאמה אישית גופן כהה פורמט זמן תצוגה אפשרויות תצוגה ערוך את מיקום האיסוף הודעות דוא"ל סוג דוא"ל תוכן בדוא"ל אפשר אימייל סטטוס הזמנה שנאסף אפשר דוא"ל סטטוס הזמנה מוכן לאיסוף מ להתמקצע כותרות צבע רקע צבע גופן של כותרות גודל הגופן של הכותרות שלום שם. חשבנו שתרצה לדעת שההזמנה האחרונה שלך מ- {site_title} הייתה מוכנה לאיסוף. שלום {customer_first_name}. תודה שגילית את ההזמנה שלך ב- {site_title} מס '{order_number}. אנו מקווים שנהניתם מחוויית הקנייה שלכם. הסתר כותרת מדור איסוף מקומי הסתר כותרות שולחן גופן קל מיילים על סטטוס הזמנת איסוף מקומי טקסט כותרת מקטע איסוף מקומי זרימת עבודה לאיסוף מקומי שם מיקום סמן את ההזמנה כפי שנאספה סמן את ההזמנה כמוכנה לאיסוף החשבון שלי (היסטוריית הזמנות) שם והוראות מיוחדות ההזמנה התקבלה נאסף נאסף נאסף (%s) נאסף (%s) כתובת איסוף הוראת איסוף פריסת הוראות איסוף בחר מיקום מיקומי איסוף טקסט כותרת כתובת איסוף אנא בחר הזמנה ... סדר תצוגה מקדימה מעבד מייל של הזמנה מוכן לאיסוף מוכן לאיסוף (%s) מוכן לאיסוף (%s) מוכן לאיסוף שמור סגור בחר הזמנה לתצוגה מקדימה ועיצוב תצוגת הוראות האיסוף. בחר סוג דוא"ל בחר פורמט זמן שברצונך להציג בשעות הפעילות של לקוחות. שלח אימייל הגדרות הוראות מיוחדות תוכן לוח כותרות שולחן צבע גבול שולחן גודל גבול שולחן האיסוף המקומי המתקדם (ALP) מסייע לך לטפל בהזמנות איסוף מקומיות בצורה נוחה יותר על ידי הרחבת שיטת המשלוח לאיסוף מקומי של WooCommerce. שם המיקום של מיקום העסק שלך. מספר הטלפון לפרטי העסק שלך. ההוראות המיוחדות לחנות שלך. שעות עבודה תקבל מייל כאשר ההזמנה תהיה מוכנה לאיסוף. ההזמנה שלך מוכנה לאיסוף ההזמנה שלך מ- {site_title} נאספה הזמנת {site_title} שלך מוכנה כעת לאיסוף בחר עבור ימי עבודה של החנות שלך. עליך להוסיף שם מיקום ולשמור כדי להמשיך 