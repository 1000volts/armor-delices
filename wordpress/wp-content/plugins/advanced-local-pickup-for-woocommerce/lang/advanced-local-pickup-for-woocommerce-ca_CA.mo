��    Q      �      ,      ,     -     1     6     >  E   F     �  %   �  @   �     �     	          /     >     Y  !   t     �     �     �  	   �  	   �     �     �  
          #   $  *   H     s     x     �     �     �     �  k   �  �   :  !   �     �  
   �      	  !   %	     G	     U	     m	     �	     �	     �	  	   �	  	   �	     �	     

     
     ,
     G
     W
     r
     �
     �
     �
  +   �
     �
     �
  F        R  M   d  
   �     �     �     �     �        �     -   �  /   �  '     
   *  >   5     t  *   �  /   �  *   �  .     \  H     �     �  	   �  	   �  _   �     '  ,   /  T   \     �     �     �     �  "   �       &   7     ^  '   ~  "   �     �     �  "   �  %        6  !   S  =   u  P   �               #     +     F  %   f  w   �  �     8   �  "   �       >     .   T     �  !   �  1   �  &   �          1     L     U     ^     ~     �  )   �     �  -   �          <  7   U     �  5   �     �     �  e     +   k  R   �     �               4     H     f  �   �  -   6  ?   d  0   �     �  N   �  /   4  1   d  B   �  7   �  :       To  To  12 hour 24 hour Additional content on processing email in case of local pickup orders Address Advanced Local Pickup for WooCommerce An additional, optional address line for your business location. Apply & close Available variables: Background color Business Hours Business Hours header text Change status to Picked up Change status to Ready for pickup Content font color Content font size Content line height Customize Dark Font Display Time Format Email Notifications Email Type Email content Enable Picked up order status email Enable Ready for Pickup order status email From General Settings Go Pro Headers Background color Headers font color Headers font size Hi there. we thought you'd like to know that your recent order from {site_title} has been ready for pickup. Hi {customer_first_name}. Thank you for picking up your {site_title} order #{order_number}. We hope you enjoyed your shopping experience. Hide Local Pickup Section Heading Hide Table Headers Light Font Local Pickup Order Status Emails Local Pickup section heading text Location Name Mark order as picked up Mark order as ready for pickup My Account (orders history) Name & Special Instructions Order received page Picked Up Picked up Picked up (%s) Picked up (%s) Pickup Address Pickup Instruction Pickup Instructions Layout Pickup Location Pickup address header text Please select a order... Preview order Processing order email Ready for Pickup Ready for Pickup (%s) Ready for Pickup (%s) Ready for pickup Save & close Select an order to preview and design the pickup instructions display. Select email type Select time format which you want to display in business hours for customers. Send Email Special Instruction TABLE CONTENT TABLE HEADERS Table Border color Table Border size The Advanced Local Pickup (ALP) helps you handle local pickup orders more conveniently by extending the WooCommerce Local Pickup shipping method. The location name for your business location. The phone number for your business information. The special instruction for your store. Work Hours You will receive an email when your order is ready for pickup. Your Order is Ready for pickup Your order from {site_title} was picked up Your {site_title} order is now Ready for pickup the select for working days of your store. you must add location name and save to proceed PO-Revision-Date: 2021-10-21 09:13+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Loco https://localise.biz/
Language: ca_CA
Project-Id-Version: Plugins - Advanced Local Pickup for WooCommerce - Development (trunk)
Language-Team: Catalan (Canada) Per a
 Per a
 12 hores
 24 hores
 Contingut addicional en processar el correu electrònic en cas de comandes de recollida locals
 adreça Recollida local avançada per a WooCommerce
 Una línia d’adreça opcional addicional per a la ubicació de la vostra empresa.
 Aplica i tanca
 Variables disponibles:
 Color de fons
 Horari laboral
 Text de capçalera Horari laboral
 Canvieu l'estat a Recollit
 Canvieu l'estat a A punt per recollir
 Color de la font del contingut
 Mida del tipus de lletra del contingut
 Alçada de la línia de contingut
 Personalitzar Tipus de lletra fosc
 Format d’hora de visualització
 Notificacions per correu electrònic
 Tipus de correu electrònic
 Contingut per correu electrònic
 Activa el correu electrònic d'estat de la comanda recollida
 Activeu el correu electrònic d'estat de la comanda Preparat per a la recollida
 Des de
 Configuració general
 Go Pro
 Capçaleres Color de fons
 Color de lletra de capçaleres
 Mida de la lletra de les capçaleres
 Hola. hem pensat que voldríeu saber que la vostra comanda recent de {site_title} ja estava preparada per recollir-la.
 Hola, {customer_first_name}. Gràcies per recollir la vostra comanda {site_title} núm. {Order_number}. Esperem que us hagi agradat la vostra experiència de compra.
 Amaga l'encapçalament de la secció de recollida local
 Amaga les capçaleres de la taula
 Font de llum
 Correu electrònic d’estat de la comanda de recollida local
 Text de capçalera de la secció Local Pickup
 Nom de la ubicació
 Marca la comanda com a recollida
 Marca la comanda com a preparada per recollir-la
 El meu compte (historial de comandes)
 Nom i instruccions especials
 Pàgina de comanda rebuda
 Recollit Recollit Recollit (% s)
 Recollit (% s)
 Adreça de recollida
 Instruccions de recollida
 Disseny de les instruccions de recollida
 Ubicació de recollida
 Text de capçalera de l'adreça de recollida
 Seleccioneu una comanda ...
 Previsualitza l’ordre
 S'està processant el correu electrònic de la comanda
 Llest per a la recollida
 A punt per recollir (% s)
 A punt per recollir (% s)
 Llest per a la recollida
 Desa i tanca
 Seleccioneu una comanda per previsualitzar i dissenyar la pantalla de les instruccions de recollida.
 Seleccioneu el tipus de correu electrònic
 Seleccioneu el format d'hora que vulgueu mostrar als clients en horari comercial.
 Envia un correu electrònic
 Instrucció especial
 CONTINGUT DE LA TAULA
 CAPÇALES DE TAULA
 Color de la vora de la taula
 Mida de la vora del quadre
 La recollida local avançada (ALP) us ajuda a gestionar les comandes de recollida local amb més comoditat ampliant el mètode d’enviament de la recollida local de WooCommerce.
 El nom de la ubicació de la vostra empresa.
 El número de telèfon de la informació de la vostra empresa.
 La instrucció especial per a la vostra botiga.
 Horari laboral
 Rebràs un correu electrònic quan la comanda estigui a punt per recollir-la.
 La vostra comanda està a punt per recollir-la
 La vostra comanda de {site_title} es va recollir
 La vostra comanda de {site_title} ja està a punt per recollir-la
 la selecció dels dies laborables de la vostra botiga.
 heu d'afegir el nom de la ubicació i desar per continuar
 