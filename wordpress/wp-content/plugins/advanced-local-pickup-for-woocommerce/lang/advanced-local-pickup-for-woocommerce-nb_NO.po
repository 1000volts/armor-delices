msgid ""
msgstr ""
"Project-Id-Version: Advanced Local Pickup for WooCommerce\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-01-05 12:01+0000\n"
"PO-Revision-Date: 2021-08-11 06:29+0000\n"
"Language-Team: Norwegian (Bokmål)\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.0; wp-5.6.2\n"
"Last-Translator: \n"
"Language: nb_NO"

#: include/views/wclp-edit-location-form.php:413
msgid " To"
msgstr " Til"

#: include/views/wclp-edit-location-form.php:315
msgid "12 hour"
msgstr "12 timer"

#: include/views/wclp-edit-location-form.php:314
msgid "24 hour"
msgstr "24 timer"

#: include/wc-local-pickup-admin.php:1079
#: include/wc-local-pickup-admin.php:1080
#: include/wc-local-pickup-admin.php:1084
msgid "Additional content on processing email in case of local pickup orders"
msgstr "Tilleggsinnhold om behandling av e -post ved lokale hentebestillinger"

#: include/views/wclp-edit-location-form.php:62
msgid "Address"
msgstr "Adresse"

#: include/views/wclp-edit-location-form.php:128
msgid "An additional, optional address line for your business location."
msgstr "En ekstra, valgfri adresselinje for bedriftens beliggenhet."

#: include/wc-local-pickup-admin.php:1003
#: include/views/wclp-edit-location-form.php:452
msgid "Apply & close"
msgstr "Påfør og lukk"

#: include/customizer/wc-pickup-email-customizer.php:164
#: include/customizer/wc-pickup-email-customizer.php:188
#: include/customizer/wc-pickup-email-customizer.php:233
#: include/customizer/wc-ready-pickup-email-customizer.php:219
#: include/customizer/wc-ready-pickup-email-customizer.php:243
#: include/customizer/wc-ready-pickup-email-customizer.php:288
msgid "Available variables:"
msgstr "Tilgjengelige variabler:"

#: include/customizer/wclp-pickup-instruction-customizer.php:456
msgid "Background color"
msgstr "Bakgrunnsfarge"

#: include/customizer/wclp-pickup-instruction-customizer.php:273
#: include/customizer/wclp-pickup-instruction-customizer.php:346
#: include/customizer/wclp-pickup-instruction-customizer.php:361
#: include/customizer/wclp-pickup-instruction-customizer.php:528
#: include/views/wclp-edit-location-form.php:197
msgid "Business Hours"
msgstr "Arbeidstid"

#: include/customizer/wclp-pickup-instruction-customizer.php:354
msgid "Business Hours header text"
msgstr "Topptekst for åpningstider"

#: include/customizer/wclp-pickup-instruction-customizer.php:505
msgid "Content font color"
msgstr "Innholdets skriftfarge"

#: include/customizer/wclp-pickup-instruction-customizer.php:473
msgid "Content font size"
msgstr "Innholdets skriftstørrelse"

#: include/customizer/wclp-pickup-instruction-customizer.php:521
msgid "Content line height"
msgstr "Innholdslinjehøyde"

#: include/wc-local-pickup-admin.php:199 include/wc-local-pickup-admin.php:200
#: include/views/wclp_setting_tab.php:59 include/views/wclp_setting_tab.php:106
#| msgid "Customizer"
msgid "Customize"
msgstr "Tilpass"

#: include/views/wclp_setting_tab.php:53 include/views/wclp_setting_tab.php:100
msgid "Dark Font"
msgstr "Dark Font"

#: include/views/wclp_setting_tab.php:6
msgid "Display options"
msgstr "Visningsalternativer"

#: include/views/wclp-edit-location-form.php:309
msgid "Display Time Format"
msgstr "Vis tidsformat"

#: include/views/wclp-edit-location-form.php:18
msgid "Edit Pickup Location"
msgstr "Rediger henteplassering"

#: include/customizer/wc-pickup-email-customizer.php:210
#: include/customizer/wc-ready-pickup-email-customizer.php:265
msgid "Email content"
msgstr "E -postinnhold"

#: include/customizer/wclp-customizer.php:54
msgid "Email Notifications"
msgstr "E-postvarsler"

#: include/customizer/wc-ready-pickup-email-customizer.php:174
msgid "Email Type"
msgstr "E -posttype"

#: include/customizer/wc-pickup-email-customizer.php:144
msgid "Enable Picked up order status email"
msgstr "Aktiver E -post med hentet bestillingsstatus"

#: include/customizer/wc-ready-pickup-email-customizer.php:199
msgid "Enable Ready for Pickup order status email"
msgstr "Aktiver status for e -post med bestillingsstatus for Klar for henting"

#: include/views/wclp-edit-location-form.php:412
msgid "From"
msgstr "Fra"

#: include/wc-local-pickup-admin.php:201 include/wc-local-pickup-admin.php:202
msgid "Go Pro"
msgstr "Bli profesjonell"

#: include/customizer/wclp-pickup-instruction-customizer.php:376
msgid "Headers Background color"
msgstr "Overskrifter Bakgrunnsfarge"

#: include/customizer/wclp-pickup-instruction-customizer.php:426
msgid "Headers font color"
msgstr "Overskriftens skriftfarge"

#: include/customizer/wclp-pickup-instruction-customizer.php:394
msgid "Headers font size"
msgstr "Skriftstørrelse på overskrifter"

#: include/customizer/wc-ready-pickup-email-customizer.php:117
msgid ""
"Hi there. we thought you'd like to know that your recent order from "
"{site_title} has been ready for pickup."
msgstr ""
"Hei der. Vi trodde du ville vite at den siste bestillingen din fra "
"{site_title} er klar for henting."

#: include/customizer/wc-pickup-email-customizer.php:117
msgid ""
"Hi {customer_first_name}. Thank you for picking up your {site_title} order "
"#{order_number}. We hope you enjoyed your shopping experience."
msgstr ""
"Hei {customer_first_name}. Takk for at du hentet {site_title} bestillingen "
"din #{order_number}. Vi håper du likte din shoppingopplevelse."

#: include/customizer/wclp-pickup-instruction-customizer.php:210
msgid "Hide Local Pickup Section Heading"
msgstr "Skjul overskrift for lokal henting"

#: include/customizer/wclp-pickup-instruction-customizer.php:312
msgid "Hide Table Headers"
msgstr "Skjul bordhoder"

#: include/views/wclp_setting_tab.php:52 include/views/wclp_setting_tab.php:99
msgid "Light Font"
msgstr "Lys skrift"

#: include/customizer/wclp-customizer.php:32
msgid "Local Pickup Order Status Emails"
msgstr "Lokal henting av bestillingsstatus"

#: include/customizer/wclp-pickup-instruction-customizer.php:229
msgid "Local Pickup section heading text"
msgstr "Overskriftstekst for lokal henting"

#: include/views/wclp_setting_tab.php:20
msgid "Local pickup workflow"
msgstr "Lokal henting arbeidsflyt"

#: include/views/wclp-edit-location-form.php:40
msgid "Location Name"
msgstr "Stedsnavn"

#: include/wc-local-pickup-admin.php:1065
msgid "My Account (orders history)"
msgstr "Min konto (ordrehistorikk)"

#: include/views/wclp-edit-location-form.php:24
msgid "Name & Special Instructions"
msgstr "Navn og spesielle instruksjoner"

#: include/wc-local-pickup-admin.php:1064
msgid "Order received page"
msgstr "Bestillingsside mottatt"

#: include/customizer/wc-ready-pickup-email-customizer.php:183
msgid "Picked Up"
msgstr "Plukket opp"

#: include/views/wclp_setting_tab.php:80
msgid "Picked up"
msgstr "Plukket opp"

#: include/wc-local-pickup-admin.php:457
#, php-format
msgid "Picked up (%s)"
msgid_plural "Picked up (%s)"
msgstr[0] "Hentet (%s)"
msgstr[1] "Hentet (%s)"

#: include/customizer/wclp-pickup-instruction-customizer.php:323
#: include/customizer/wclp-pickup-instruction-customizer.php:338
msgid "Pickup Address"
msgstr "Henteadresse"

#: include/customizer/wclp-pickup-instruction-customizer.php:331
msgid "Pickup address header text"
msgstr "Adresseoverskriftstekst for henting"

#: include/customizer/wclp-pickup-instruction-customizer.php:221
#: include/customizer/wclp-pickup-instruction-customizer.php:235
#: include/customizer/wc-ready-pickup-email-customizer.php:122
msgid "Pickup Instruction"
msgstr "Instruksjon for henting"

#: include/customizer/wclp-customizer.php:46
msgid "Pickup Instructions Layout"
msgstr "Pickup Instruksjoner Oppsett"

#: include/wc-local-pickup-admin.php:197 include/wc-local-pickup-admin.php:198
msgid "Pickup Location"
msgstr "Hentested"

#: include/views/wclp-edit-location-form.php:16
msgid "Pickup Locations"
msgstr "Pickup Steder"

#: include/customizer/wclp-pickup-instruction-customizer.php:192
#: include/customizer/wc-ready-pickup-email-customizer.php:156
msgid "Please select a order..."
msgstr "Velg en bestilling ..."

#: include/customizer/wclp-pickup-instruction-customizer.php:188
#: include/customizer/wc-ready-pickup-email-customizer.php:152
msgid "Preview order"
msgstr "Forhåndsvisning"

#: include/wc-local-pickup-admin.php:1063
msgid "Processing order email"
msgstr "Behandler ordre -e -post"

#: include/customizer/wc-ready-pickup-email-customizer.php:182
msgid "Ready for Pickup"
msgstr "Klar til å hentes"

#: include/views/wclp_setting_tab.php:33
msgid "Ready for pickup"
msgstr "Klar til å hentes"

#: include/wc-local-pickup-admin.php:445
#| msgid "Ready for Pickup"
msgid "Ready for Pickup (%s)"
msgid_plural "Ready for Pickup (%s)"
msgstr[0] "Klar for henting (%s)"
msgstr[1] "Klar for henting (%s)"

#: include/views/wclp-edit-location-form.php:27
#: include/views/wclp-edit-location-form.php:65
#: include/views/wclp-edit-location-form.php:200
msgid "Save & close"
msgstr "Lagre og lukk"

#: include/customizer/wclp-pickup-instruction-customizer.php:189
msgid "Select an order to preview and design the pickup instructions display."
msgstr ""
"Velg en bestilling for å forhåndsvise og utforme skjermbildet for henting."

#: include/customizer/wc-ready-pickup-email-customizer.php:178
msgid "Select email type"
msgstr "Velg e -posttype"

#: include/views/wclp-edit-location-form.php:309
msgid ""
"Select time format which you want to display in business hours for customers."
msgstr "Velg tidsformat som du vil vise i åpningstider for kunder."

#: include/views/wclp_setting_tab.php:57 include/views/wclp_setting_tab.php:104
msgid "Send Email"
msgstr "Send e-post"

#: include/views/wclp_setting_tab.php:4
msgid "Settings"
msgstr "Innstillinger"

#: include/views/wclp-edit-location-form.php:50
msgid "Special Instruction"
msgstr "Spesiell instruksjon"

#: include/customizer/wclp-pickup-instruction-customizer.php:250
msgid "Table Border color"
msgstr "Bordgrense farge"

#: include/customizer/wclp-pickup-instruction-customizer.php:266
msgid "Table Border size"
msgstr "Bordgrensestørrelse"

#: include/customizer/wclp-pickup-instruction-customizer.php:441
msgid "TABLE CONTENT"
msgstr "TABELLINNHOLD"

#: include/customizer/wclp-pickup-instruction-customizer.php:296
msgid "TABLE HEADERS"
msgstr "BORDSHOVEDE"

#. Description of the plugin
msgid ""
"The Advanced Local Pickup (ALP) helps you handle local pickup orders more "
"conveniently by extending the WooCommerce Local Pickup shipping method."
msgstr ""
"Advanced Local Pickup (ALP) hjelper deg med å håndtere lokale "
"pickupbestillinger lettere ved å utvide WooCommerce Local Pickup-"
"forsendelsesmetoden."

#: include/views/wclp-edit-location-form.php:40
msgid "The location name for your business location."
msgstr "Stedsnavnet for virksomhetsstedet ditt."

#: include/views/wclp-edit-location-form.php:185
msgid "The phone number for your business information."
msgstr "Telefonnummeret til bedriftsinformasjonen din."

#: include/views/wclp-edit-location-form.php:324
msgid "the select for working days of your store."
msgstr "velg for arbeidsdager i butikken din."

#: include/views/wclp-edit-location-form.php:50
msgid "The special instruction for your store."
msgstr "Den spesielle instruksjonen for butikken din."

#: include/views/wclp-edit-location-form.php:324
msgid "Work Hours"
msgstr "Arbeidstimer"

#: include/views/wclp-edit-location-form.php:44
msgid "you must add location name and save to proceed"
msgstr "du må legge til stedsnavn og lagre for å fortsette"

#: include/wc-local-pickup-admin.php:1083
msgid "You will receive an email when your order is ready for pickup."
msgstr "Du vil motta en e -post når bestillingen er klar for henting."

#: include/customizer/wc-pickup-email-customizer.php:115
msgid "Your order from {site_title} was picked up"
msgstr "Bestillingen din fra {site_title} ble hentet"

#: include/customizer/wc-ready-pickup-email-customizer.php:116
msgid "Your Order is Ready for pickup"
msgstr "Bestillingen din er klar for henting"

#: include/customizer/wc-ready-pickup-email-customizer.php:115
msgid "Your {site_title} order is now Ready for pickup"
msgstr "Bestillingen din fra {site_title} er nå klar for henting"
