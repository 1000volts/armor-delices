<?php

namespace GtmEcommerceWooPro\Lib;

use GtmEcommerceWoo\Lib\EventStrategy;
use GtmEcommerceWoo\Lib\Service\EventStrategiesService;
use GtmEcommerceWooPro\Lib\Service\GtmSnippetService;
use GtmEcommerceWooPro\Lib\Service\SettingsService;
use GtmEcommerceWoo\Lib\Service\PluginService;
use GtmEcommerceWoo\Lib\Service\ThemeValidatorService;
use GtmEcommerceWoo\Lib\Service\EventInspectorService;

use GtmEcommerceWoo\Lib\Util\WpSettingsUtil;
use GtmEcommerceWoo\Lib\Util\WcOutputUtil;
use GtmEcommerceWooPro\Lib\Util\WcTransformerUtil;

use GtmEcommerceWooPro\Lib\EventStrategy as EventStrategyPro;

class Container extends \GtmEcommerceWoo\Lib\Container {
	public function __construct() {
		$snakeCaseNamespace = 'gtm_ecommerce_woo';
		$spineCaseNamespace = 'gtm-ecommerce-woo';

		$wpSettingsUtil    = new WpSettingsUtil( $snakeCaseNamespace, $spineCaseNamespace );
		$wcTransformerUtil = new WcTransformerUtil();
		$wcOutputUtil      = new WcOutputUtil();

		$eventStrategies = array(
			new EventStrategyPro\AddToCartStrategy( $wcTransformerUtil, $wcOutputUtil ),
			new EventStrategyPro\PurchaseStrategy( $wcTransformerUtil, $wcOutputUtil ),
			new EventStrategyPro\ViewItemListStrategy( $wcTransformerUtil, $wcOutputUtil ),
			new EventStrategyPro\ViewItemStrategy( $wcTransformerUtil, $wcOutputUtil ),
			new EventStrategyPro\BeginCheckoutStrategy( $wcTransformerUtil, $wcOutputUtil ),
			new EventStrategyPro\RemoveFromCartStrategy( $wcTransformerUtil, $wcOutputUtil ),
			new EventStrategyPro\SelectItemStrategy( $wcTransformerUtil, $wcOutputUtil ),
			// new EventStrategyPro\RefundStrategy($wcTransformerUtil, $wcOutputUtil),
		);

		$events = array_map(function( $eventStrategy) {
			return $eventStrategy->getEventName();
		}, $eventStrategies);

		$this->eventStrategiesService = new EventStrategiesService( $wpSettingsUtil, $eventStrategies );
		$this->gtmSnippetService      = new GtmSnippetService( $wpSettingsUtil );
		$this->settingsService        = new SettingsService( $wpSettingsUtil, $events, [] );
		$this->pluginService          = new PluginService( $spineCaseNamespace );
		$this->themeValidatorService  = new ThemeValidatorService($snakeCaseNamespace, $spineCaseNamespace, $wcTransformerUtil, $wpSettingsUtil, $wcOutputUtil, $events);
		$this->eventInspectorService  = new EventInspectorService( $wpSettingsUtil );

	}
}
