<?php

namespace GtmEcommerceWooPro\Lib\EventStrategy;

class AddToCartStrategy extends \GtmEcommerceWoo\Lib\EventStrategy\AddToCartStrategy {
	public function singleProduct() {
		global $product;
		// if product is null then this must be other WP post
		if (is_null($product)) {
			return false;
		}
		if (is_product() && false === $this->firstPost) {
			$this->firstPost = true;
			if ( get_class( $product ) === 'WC_Product_Variable' ) {
				$itemsById         = array();
				$itemsByAttributes = array();
				foreach ( $product->get_available_variations( 'object' ) as $variation ) {
					$item                          = $this->wcTransformer->getItemFromProductVariation( $variation );
					$items[ $variation->get_id() ] = $item;
					$attributes                    = $variation->get_variation_attributes();
					$attributes['item_id']         = $variation->get_id();
					$itemsByAttributes[]           = $attributes;
				}
				return $this->onCartSubmitScriptVariable( $itemsByAttributes, $items );
			} else {
				$item = $this->wcTransformer->getItemFromProduct( $product );
				$this->onCartSubmitScript( $item );
			}
		}
	}

	public function onCartSubmitScriptVariable( $attributes, $items ) {
		$this->wcOutput->globalVariable( 'gtm_ecommerce_woo_items_by_attributes', $attributes );
		$this->wcOutput->globalVariable( 'gtm_ecommerce_woo_items_by_id', $items );
		$this->wcOutput->script(
			<<<EOD
jQuery(document).on('submit', '.cart', function(ev) {
    var quantity = jQuery('[name="quantity"]', ev.currentTarget).val();
    var product_id = jQuery('[name="add-to-cart"]', ev.currentTarget).val();
    var attributes = jQuery('.variations select').get().reduce(function(agg, el) {agg[el.name] = jQuery(el).val(); return agg;}, {});
    var attribute = (gtm_ecommerce_woo_items_by_attributes || []).filter(function(el) {

        for (var key in attributes) {
          if (!el[key] || el[key] !== attributes[key]) {
            return false;
          }
        }
        return true;
    });
    if (!attribute.length || attribute.length === 0) {
        return false;
    }

    var item = gtm_ecommerce_woo_items_by_id[attribute[0].item_id];
    item.quantity = quantity;
    dataLayer.push({
      'event': 'add_to_cart',
      'ecommerce': {
        'items': [item]
      }
    });
});
EOD
		);

	}
}
