<?php

namespace GtmEcommerceWooPro\Lib\EventStrategy;

use GtmEcommerceWoo\Lib\GaEcommerceEntity\Event;

/**
 * When a user sees a list of items/offerings
 * <td class="product-remove">
							<a href="http://docker.local/?page_id=6&amp;remove_item=45c48cce2e2d7fbdea1afc51c7c6ad26&amp;_wpnonce=f2b331db52" class="remove" aria-label="Remove this item" data-product_id="9" data-product_sku="WCCLITESTP">×</a>                      </td>
 */
class RemoveFromCartStrategy extends \GtmEcommerceWoo\Lib\EventStrategy\AbstractEventStrategy {

	protected $eventName = 'remove_from_cart';

	public function defineActions() {
		return array(
			'wp_head' => array( $this, 'afterCart' ),
		);
	}

	public function afterCart() {
		global $woocommerce;
		if (is_cart()) {
			$itemsByProductId = array();
			foreach ( $woocommerce->cart->get_cart() as $item ) {
				$product                                = wc_get_product( $item['data']->get_id() );
				$itemsByProductId[ $product->get_id() ] = $this->wcTransformer->getItemFromProduct( $product );
			}
			$this->onRemoveLinkClick( $itemsByProductId );
		}
	}

	public function onRemoveLinkClick( $items ) {
		$this->wcOutput->globalVariable( 'gtm_ecommerce_woo_items_by_product_id', $items );
		$this->wcOutput->script(
			<<<EOD
jQuery(document).on('click', '.remove', function(ev) {
    var product_id = jQuery(ev.currentTarget).attr('data-product_id');
    var item = gtm_ecommerce_woo_items_by_product_id[product_id];
    dataLayer.push({
      'event': 'remove_from_cart',
      'ecommerce': {
        'items': [item]
      }
    });
});
EOD
		);
	}
}
