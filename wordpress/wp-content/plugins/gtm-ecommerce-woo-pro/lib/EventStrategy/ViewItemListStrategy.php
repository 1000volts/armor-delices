<?php

namespace GtmEcommerceWooPro\Lib\EventStrategy;

use GtmEcommerceWoo\Lib\GaEcommerceEntity\Event;

/**
 * When a user sees a list of items/offerings
 * https://developers.google.com/tag-manager/ecommerce-ga4#measure_productitem_list_viewsimpressions
 * https://developers.google.com/gtagjs/reference/ga4-events#view_item_list
 */
class ViewItemListStrategy extends \GtmEcommerceWoo\Lib\EventStrategy\AbstractEventStrategy {

	protected $eventName = 'view_item_list';
	protected $itemListName = null;
	protected $trackedProductIds = [];
	protected $firstProductId = null;

	public function defineActions() {
		return array(
			'wp_head' => array( $this, 'wpHead' ),
			'the_post' => [[$this, 'shopLoop'], 11],
			'wp_footer'  => array( $this, 'afterShopLoop' ),
		);
	}

	public function wpHead() {
		$this->items = array();
		$this->index = 0;

		// if we are on a product page then we skip first product
		if (is_product()) {
			$this->firstProduct = true;
		} else {
			$this->firstProduct = false;
		}


		$this->itemListName = 'default';
		$this->itemListId   = '0';

		if ( is_product_category() ) {
			$cat                = get_queried_object();
			$this->itemListName = 'category_' . $cat->name;
			$this->itemListId   = 'category_' . $cat->term_id;
		}

		if ( is_product_tag() ) {
			$cat                = get_queried_object();
			$this->itemListName = 'tag_' . $cat->name;
			$this->itemListId   = 'tag_' . $cat->term_id;
		}
	}

	public function shopLoop() {
		global $product;
		// sometimes it may be called before header, we want to ignore it
		if (is_null($this->itemListName)) {
			return false;
		}
		if (is_a($product, 'WC_Product')) {
			if (true !== $this->firstProduct) {
				// ensure we track every product once
				if (in_array($product->get_id(), $this->trackedProductIds)) {
					return false;
				}
				// ignore products discovered as first product
				if ($this->firstProductId == $product->get_id()) {
					return false;
				}
				$item = $this->wcTransformer->getItemFromProduct( $product );
				$item->setIndex( $this->index );
				$item->setItemListName( $this->itemListName );
				$item->setItemListId( $this->itemListId );

				$this->items[] = $item;
				$this->trackedProductIds[] = $product->get_id();
				$this->index++;
			} else {
				$this->firstProductId = $product->get_id();
			}
			$this->firstProduct = false;
		}

	}

	public function afterShopLoop() {
		if (is_array($this->items) && count($this->items) > 0) {
			$event = new Event( 'view_item_list' );
			$event->setItems( $this->items );
			$this->wcOutput->dataLayerPush( $event );
		}
	}
	// hook to woocommerce_shop_loop to build the list
	// woocommerce_after_shop_loop to drop it in the page

}
