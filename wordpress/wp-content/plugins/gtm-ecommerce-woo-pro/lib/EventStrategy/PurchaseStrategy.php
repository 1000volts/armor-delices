<?php

namespace GtmEcommerceWooPro\Lib\EventStrategy;

class PurchaseStrategy extends \GtmEcommerceWoo\Lib\EventStrategy\PurchaseStrategy {
	public function thankyou( $orderId ) {
		if ( '1' === get_post_meta( $orderId, 'gtm_ecommerce_woo_purchase_event_tracked', true ) ) {
			return;
		}
		$event = $this->wcTransformer->getPurchaseFromOrderId($orderId);

		$this->wcOutput->dataLayerPush($event);

		update_post_meta( $orderId, 'gtm_ecommerce_woo_purchase_event_tracked', '1' );
	}
}
