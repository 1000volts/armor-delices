<?php

namespace GtmEcommerceWooPro\Lib\EventStrategy;

use GtmEcommerceWoo\Lib\GaEcommerceEntity\Event;

/**
 * When a user sees a list of items/offerings
 */
class BeginCheckoutStrategy extends \GtmEcommerceWoo\Lib\EventStrategy\AbstractEventStrategy {
	protected $eventName = 'begin_checkout';

	public function defineActions() {
		return array(
			'woocommerce_before_checkout_form' => array( $this, 'beforeCheckoutForm' ),
		);
	}

	public function beforeCheckoutForm() {
		global $woocommerce;
		$wcTransformer = $this->wcTransformer;
		$items         = array_map(
			function( $item ) use ( $wcTransformer ) {
				$product = wc_get_product( $item['data']->get_id() );
				return $this->wcTransformer->getItemFromProduct( $product );
			},
			$woocommerce->cart->get_cart()
		);
		$event         = new Event( 'begin_checkout' );
		$event->setItems( array_values( $items ) );
		$this->wcOutput->dataLayerPush( $event );
	}

}
