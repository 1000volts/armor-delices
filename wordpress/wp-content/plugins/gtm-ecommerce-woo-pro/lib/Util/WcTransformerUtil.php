<?php

namespace GtmEcommerceWooPro\Lib\Util;

use GtmEcommerceWoo\Lib\GaEcommerceEntity\Event;
use GtmEcommerceWoo\Lib\GaEcommerceEntity\Item;

/**
 * Logic to handle embedding Gtm Snippet
 */
class WcTransformerUtil extends \GtmEcommerceWoo\Lib\Util\WcTransformerUtil {

	public function getRefundFromOrderId( $orderId, $refunds ) {
		$order = wc_get_order( $orderId );
		$event = new Event( 'refund' );
		$event->setTransationId( $order->get_order_number() );

		foreach ( $refunds as $refund ) {
			foreach ( $refund->get_items() as $key => $orderItem ) {
				$item           = $this->getItemFromOrderItem( $orderItem );
				$item->quantity = -$item->quantity;
				$event->addItem( $item );
			}
		}

		return $event;
	}

	/**
	 * Url: https://woocommerce.github.io/code-reference/classes/WC-Product-Variation.html
	 */
	public function getItemFromProductVariation( $product ) {
		$item = new Item( $product->get_name() );
		$item->setItemId( $product->get_id() );
		$item->setPrice( $product->get_price() );
		// $item->setItemBrand('');
		$productCats = get_the_terms( $product->get_id(), 'product_cat' );
		if ( is_array( $productCats ) ) {
			$categories = array_map(
				function( $category ) {
					return $category->name; },
				$productCats
			);
			$item->setItemCategories( $categories );
		}
		$item = apply_filters('gtm_ecommerce_woo_item', $item, $product);
		return $item;
	}
}
