<?php

class LpcAdminOrderBanner extends LpcComponent {
    /** @var LpcLabelQueries */
    protected $lpcLabelQueries;

    /** @var LpcShippingMethods */
    protected $lpcShippingMethods;

    /** @var LpcLabelGenerationOutward */
    protected $lpcOutwardLabelGeneration;

    /** @var LpcLabelGenerationInward */
    protected $lpcInwardLabelGeneration;

    /** @var LpcAdminNotices */
    protected $lpcAdminNotices;

    /** @var LpcOutwardLabelDb */
    protected $outwardLabelDb;

    /** @var LpcBordereauDownloadAction */
    protected $bordereauDownloadAction;

    /** @var LpcCapabilitiesPerCountry */
    private $capabilitiesPerCountry;

    /** @var LpcCustomsDocumentsApi */
    private $customsDocumentsApi;

    public function __construct(
        LpcLabelQueries $lpcLabelQueries = null,
        LpcShippingMethods $lpcShippingMethods = null,
        LpcLabelGenerationOutward $lpcOutwardLabelGeneration = null,
        LpcLabelGenerationInward $lpcInwardLabelGeneration = null,
        LpcAdminNotices $lpcAdminNotices = null,
        LpcOutwardLabelDb $outwardLabelDb = null,
        LpcBordereauDownloadAction $bordereauDownloadAction = null,
        LpcCapabilitiesPerCountry $capabilitiesPerCountry = null,
        LpcCustomsDocumentsApi $customsDocumentsApi = null
    ) {
        $this->lpcLabelQueries           = LpcRegister::get('labelQueries', $lpcLabelQueries);
        $this->lpcShippingMethods        = LpcRegister::get('shippingMethods', $lpcShippingMethods);
        $this->lpcOutwardLabelGeneration = LpcRegister::get('labelGenerationOutward', $lpcOutwardLabelGeneration);
        $this->lpcInwardLabelGeneration  = LpcRegister::get('labelGenerationInward', $lpcInwardLabelGeneration);
        $this->lpcAdminNotices           = LpcRegister::get('lpcAdminNotices', $lpcAdminNotices);
        $this->outwardLabelDb            = LpcRegister::get('outwardLabelDb', $outwardLabelDb);
        $this->bordereauDownloadAction   = LpcRegister::get('bordereauDownloadAction', $bordereauDownloadAction);
        $this->capabilitiesPerCountry    = LpcRegister::get('capabilitiesPerCountry', $capabilitiesPerCountry);
        $this->customsDocumentsApi       = LpcRegister::get('customsDocumentsApi', $customsDocumentsApi);
    }

    public function init() {
        add_action(
            'current_screen',
            function ($currentScreen) {
                if ('post' === $currentScreen->base && 'shop_order' === $currentScreen->post_type) {
                    LpcHelper::enqueueStyle(
                        'lpc_order_banner',
                        plugins_url('/css/orders/lpc_order_banner.css', LPC_ADMIN . 'init.php'),
                        null
                    );

                    LpcHelper::enqueueScript(
                        'lpc_order_banner',
                        plugins_url('/js/orders/lpc_order_banner.js', LPC_ADMIN . 'init.php'),
                        null,
                        ['jquery-core']
                    );

                    LpcLabelQueries::enqueueLabelsActionsScript();
                }
            }
        );

        add_action('save_post', [$this, 'generateLabel'], 10, 3);
        add_action('save_post', [$this, 'sendCustomsDocuments'], 11, 3);
    }

    public function bannerContent($post) {
        $orderId = $post->ID;
        $order   = wc_get_order($post);

        if (empty($this->lpcShippingMethods->getColissimoShippingMethodOfOrder($order))) {
            $warningMessage = __('This order is not shipped by Colissimo', 'wc_colissimo');

            echo '<div class="lpc__admin__order_banner__warning"><span>' . $warningMessage . '</span></div>';

            return;
        }

        $trackingNumbers = [];
        $labelFormat     = [];

        $this->lpcLabelQueries->getTrackingNumbersByOrdersId($trackingNumbers, $labelFormat, [$orderId]);

        $trackingNumbersForOrder = !empty($trackingNumbers[$orderId]) ? $trackingNumbers[$orderId] : [];

        $args  = [];
        $items = $order->get_items();

        $labelDetails = $this->outwardLabelDb->getAllLabelDetailByOrderId($order->get_id());

        $alreadyGeneratedLabelItems = [];
        foreach ($labelDetails as $detail) {
            if (empty($detail)) {
                continue;
            }
            $detail = json_decode($detail, true);
            $this->lpcOutwardLabelGeneration->addItemsToAlreadyGeneratedLabel($alreadyGeneratedLabelItems, $detail);
        }

        $args['lpc_order_items'] = [];

        foreach ($items as $item) {
            $product = $item->get_product();

            $quantity = $item->get_quantity();

            if (!empty($alreadyGeneratedLabelItems[$item->get_id()])) {
                $quantity -= $alreadyGeneratedLabelItems[$item->get_id()]['qty'];
            }

            $args['lpc_order_items'][] = [
                'id'       => $item->get_id(),
                'name'     => $item->get_name(),
                'qty'      => $quantity,
                'weight'   => empty($product->get_weight()) ? 0 : $product->get_weight(),
                'price'    => $product->get_price(),
                'base_qty' => $item->get_quantity(),
            ];
        }

        $bordereauLinks = [];
        foreach ($trackingNumbersForOrder as $outward => $inward) {
            $bordereauID   = $this->outwardLabelDb->getBordereauFromTrackingNumber($outward);
            $bordereauLink = '';
            if (!empty($bordereauID[0])) {
                $bordereauLink = $this->bordereauDownloadAction->getBorderauDownloadLink($bordereauID[0]);
            }
            if (!empty($bordereauLink)) {
                $bordereauLinks[$outward] = [
                    'link' => $bordereauLink,
                    'id'   => $bordereauID[0],
                ];
            }
        }

        $args['postId']               = $orderId;
        $args['lpc_tracking_numbers'] = $trackingNumbersForOrder;
        $args['lpc_label_formats']    = $labelFormat;
        $args['lpc_label_queries']    = $this->lpcLabelQueries;
        $args['lpc_redirection']      = LpcLabelQueries::REDIRECTION_WOO_ORDER_EDIT_PAGE;
        $args['lpc_packaging_weight'] = LpcHelper::get_option('lpc_packaging_weight', 0);
        $args['lpc_shipping_costs']   = empty($order->get_shipping_total()) ? 0 : $order->get_shipping_total();
        $args['lpc_bordereauLinks']   = $bordereauLinks;
        $args['lpc_customs_needed']   = false;

        if (!empty($trackingNumbersForOrder)) {
            $countryCode = $order->get_shipping_country();
            $date        = date('Y-m-d');

            if ('GF' === $countryCode || ('GP' === $countryCode && $date > '2021-12-31') || ('MQ' === $countryCode && $date > '2022-01-31') || ('YT' === $countryCode && $date > '2022-03-31') || ('RE' === $countryCode && $date > '2022-05-31')) {
                $args['lpc_customs_needed'] = $this->capabilitiesPerCountry->getIsCn23RequiredForDestination($countryCode);
            }
            if ($args['lpc_customs_needed']) {
                // Options needed to send the documents
                $args['lpc_documents_types'] = [
                    'C50'                   => __('Custom clearance bordereau', 'wc_colissimo') . ' (C50)',
                    'CERTIFICATE_OF_ORIGIN' => __('Original certificate', 'wc_colissimo'),
                    'CN23'                  => __('Customs declaration', 'wc_colissimo') . ' (CN23)',
                    'EXPORT_LICENCE'        => __('Export license', 'wc_colissimo'),
                    'COMMERCIAL_INVOICE'    => __('Parcel invoice', 'wc_colissimo'),
                    'COMPENSATION'          => __('Compensation report', 'wc_colissimo'),
                    'DAU'                   => __('Unique administrative document', 'wc_colissimo') . ' (DAU)',
                    'DELIVERY_CERTIFICATE'  => __('Delivery certificate', 'wc_colissimo'),
                    'LABEL'                 => __('Label', 'wc_colissimo'),
                    'PHOTO'                 => __('Picture', 'wc_colissimo'),
                    'SIGNATURE'             => __('Proof of delivery', 'wc_colissimo'),
                ];
                asort($args['lpc_documents_types']);
                $args['lpc_documents_types']['OTHER'] = __('Other document', 'wc_colissimo');
                $args['lpc_documents_types']          = array_merge(['' => __('Document type', 'wc_colissimo')], $args['lpc_documents_types']);

                // Get the already sent documents
                $args['lpc_sent_documents'] = get_post_meta($orderId, 'lpc_customs_sent_documents', true);
                $args['lpc_sent_documents'] = empty($args['lpc_sent_documents']) ? [] : json_decode($args['lpc_sent_documents'], true);
            }
        }

        echo LpcHelper::renderPartial('orders' . DS . 'lpc_admin_order_banner.php', $args);
    }

    public function generateLabel($post_id, $post, $update) {
        $slug = 'shop_order';

        if (
            !is_admin()
            || $slug != $post->post_type
            || !isset($_REQUEST['lpc__admin__order_banner__generate_label__action'])
            || empty($_REQUEST['lpc__admin__order_banner__generate_label__action'])
        ) {
            return;
        }

        if (empty($_REQUEST['lpc__admin__order_banner__generate_label__items-id'])) {
            return;
        }

        $allItemsId = unserialize(sanitize_text_field(wp_unslash($_REQUEST['lpc__admin__order_banner__generate_label__items-id'])));

        $items = [];
        foreach ($allItemsId as $oneItemId) {
            if (!isset($_REQUEST[$oneItemId . '-checkbox']) || 'on' !== $_REQUEST[$oneItemId . '-checkbox']) {
                continue;
            }

            $items[$oneItemId]['price']  = isset($_REQUEST[$oneItemId . '-price']) ? sanitize_text_field(wp_unslash($_REQUEST[$oneItemId . '-price'])) : 0;
            $items[$oneItemId]['qty']    = isset($_REQUEST[$oneItemId . '-qty']) ? sanitize_text_field(wp_unslash($_REQUEST[$oneItemId . '-qty'])) : 0;
            $items[$oneItemId]['weight'] = isset($_REQUEST[$oneItemId . '-weight']) ? sanitize_text_field(wp_unslash($_REQUEST[$oneItemId . '-weight'])) : 0;
        }

        if (empty($items)) {
            $this->lpcAdminNotices->add_notice('lpc_notice', 'notice-warning', __('You need to select at least one item to generate a label', 'wc_colissimo'));

            return;
        }

        $order         = wc_get_order($post_id);
        $packageWeight = isset($_REQUEST['lpc__admin__order_banner__generate_label__package_weight']) ? sanitize_text_field(wp_unslash($_REQUEST['lpc__admin__order_banner__generate_label__package_weight'])) : 0;
        $totalWeight   = isset($_REQUEST['lpc__admin__order_banner__generate_label__total_weight__input']) ? sanitize_text_field(wp_unslash($_REQUEST['lpc__admin__order_banner__generate_label__total_weight__input'])) : 0;
        $shippingCosts = isset($_REQUEST['lpc__admin__order_banner__generate_label__shipping_costs']) ? sanitize_text_field(wp_unslash($_REQUEST['lpc__admin__order_banner__generate_label__shipping_costs'])) : 0;

        $customParams = [
            'packageWeight' => $packageWeight,
            'totalWeight'   => $totalWeight,
            'items'         => $items,
            'shippingCosts' => $shippingCosts,
        ];

        $outwardOrInward = isset($_REQUEST['lpc__admin__order_banner__generate_label__outward_or_inward']) ? sanitize_text_field(wp_unslash($_REQUEST['lpc__admin__order_banner__generate_label__outward_or_inward'])) : '';

        if ('outward' === $outwardOrInward || 'both' === $outwardOrInward) {
            $this->lpcOutwardLabelGeneration->generate($order, $customParams);
        }

        if ('inward' === $outwardOrInward || ('both' === $outwardOrInward && 'yes' !== LpcHelper::get_option('lpc_createReturnLabelWithOutward', 'no'))) {
            $this->lpcInwardLabelGeneration->generate($order, $customParams);
        }
    }

    public function sendCustomsDocuments($post_id, $post, $update) {
        if (!is_admin() || 'shop_order' !== $post->post_type || !isset($_FILES['lpc__customs_document'])) {
            return;
        }

        $sentDocuments = get_post_meta($post_id, 'lpc_customs_sent_documents', true);
        $sentDocuments = empty($sentDocuments) ? [] : json_decode($sentDocuments, true);

        $documentsPerLabel = $_FILES['lpc__customs_document']; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized,WordPress.Security.ValidatedSanitizedInput.MissingUnslash
        foreach ($documentsPerLabel['name'] as $parcelNumber => $documentTypes) {
            foreach ($documentTypes as $documentType => $documentNames) {
                foreach ($documentNames as $documentNumber => $oneDocumentName) {
                    try {
                        $document   = $documentsPerLabel['tmp_name'][$parcelNumber][$documentType][$documentNumber];
                        $documentId = $this->customsDocumentsApi->storeDocument($documentType, $parcelNumber, $document, $oneDocumentName);

                        // Old version of API maybe, keep this test
                        $dotPosition = strrpos($documentId, '.');
                        if (!empty($dotPosition)) {
                            $documentId = substr($documentId, 0, $dotPosition);
                        }

                        $sentDocuments[$parcelNumber][$documentId] = [
                            'documentName' => $oneDocumentName,
                            'documentType' => $documentType,
                        ];
                    } catch (Exception $e) {
                        $this->lpcAdminNotices->add_notice('lpc_notice', 'notice-error', $e->getMessage());
                    }
                }
            }
        }

        update_post_meta(
            $post_id,
            'lpc_customs_sent_documents',
            json_encode($sentDocuments)
        );
    }
}
