<?php
$trackingNumbers        = isset($args['lpc_tracking_numbers']) ? $args['lpc_tracking_numbers'] : [];
$labelFormat            = isset($args['lpc_label_formats']) ? $args['lpc_label_formats'] : [];
$orderItems             = isset($args['lpc_order_items']) && is_array($args['lpc_order_items']) ? $args['lpc_order_items'] : [];
$weightUnity            = LpcHelper::get_option('woocommerce_weight_unit', '');
$currency               = get_woocommerce_currency_symbol(get_woocommerce_currency());
$shippingCosts          = isset($args['lpc_shipping_costs']) ? $args['lpc_shipping_costs'] : 0;
$bordereauLinks         = isset($args['lpc_bordereauLinks']) ? $args['lpc_bordereauLinks'] : [];
$customsDocumentsNeeded = isset($args['lpc_customs_needed']) ? $args['lpc_customs_needed'] : false;
?>

<div class="lpc__admin__order_banner">
	<div class="lpc__admin__order_banner__header">
		<div data-lpc-tab="label_listing" class="lpc__admin__order_banner__tab lpc__admin__order_banner__header__listing nav-tab nav-tab-active">
            <?php echo __('Labels listing', 'wc_colissimo'); ?>
		</div>
		<div data-lpc-tab="generate_label" class="lpc__admin__order_banner__tab lpc__admin__order_banner__header__generation nav-tab">
            <?php echo __('Labels generation', 'wc_colissimo'); ?>
		</div>
        <?php if ($customsDocumentsNeeded) { ?>
			<div data-lpc-tab="send_documents" class="lpc__admin__order_banner__tab lpc__admin__order_banner__header__documents nav-tab">
                <?php echo __('Customs documents', 'wc_colissimo'); ?>
			</div>
        <?php } ?>
	</div>
	<div class="lpc__admin__order_banner__content lpc__admin__order_banner__generate_label" style="display: none">
		<div class="lpc__admin__order_banner__generate_label__div">
			<table class="wp-list-table widefat fixed striped">
				<thead>
					<tr>
						<th class="check-column"><input type="checkbox" class="lpc__admin__order_banner__generate_label__item__check_all" checked="checked"></th>
						<th><?php echo __('Item', 'woocommerce'); ?></th>
						<th><?php echo sprintf(__('Unit price (%s)', 'wc_colissimo'), $currency); ?></th>
						<th><?php echo __('Quantity', 'wc_colissimo'); ?></th>
						<th><?php echo sprintf(__('Unit weight (%s)', 'wc_colissimo'), $weightUnity); ?></th>
					</tr>
				</thead>
				<tbody>
                    <?php
                    $allItemsId = [];
                    foreach ($orderItems as $oneItem) {
                        $allItemsId[] = $oneItem['id'];
                        ?>
						<tr>
							<td class="lpc__admin__order_banner__generate_label__item__td__checkbox check-column">
								<input type="checkbox"
									   data-item-id="<?php echo $oneItem['id']; ?>"
									   class="lpc__admin__order_banner__generate_label__item__checkbox"
                                    <?php echo empty($oneItem['qty']) ? '' : 'checked'; ?>
									   name="<?php echo $oneItem['id'] . '-checkbox'; ?>"
									   id="<?php echo $oneItem['id'] . '-checkbox'; ?>"
								></td>
							<td><?php echo $oneItem['name']; ?></td>
							<td><input type="number"
									   class="lpc__admin__order_banner__generate_label__item__price"
									   data-item-id="<?php echo $oneItem['id']; ?>"
									   value="<?php echo $oneItem['price']; ?>"
									   name="<?php echo $oneItem['id'] . '-price'; ?>"
									   min="0"
									   step="any"
									   readonly="readonly"
								></td>
							<td><input
										style="display: inline-block; width: 50px"
										type="number"
										class="lpc__admin__order_banner__generate_label__item__qty"
										data-item-id="<?php echo $oneItem['id']; ?>"
										value="<?php echo $oneItem['qty']; ?>"
										step="1"
										min="0"
										name="<?php echo $oneItem['id'] . '-qty'; ?>"
										id="<?php echo $oneItem['id'] . '-qty'; ?>"
								><span style="margin-left: 5px">/</span><span style="margin-left: 5px"><?php echo $oneItem['base_qty']; ?></span></td>
							<td><input type="number"
									   class="lpc__admin__order_banner__generate_label__item__weight"
									   data-item-id="<?php echo $oneItem['id']; ?>"
									   value="<?php echo $oneItem['weight']; ?>"
									   min="0"
									   step="any"
									   readonly="readonly"
									   name="<?php echo $oneItem['id'] . '-weight'; ?>"
								></td>
						</tr>
                    <?php } ?>
				</tbody>
			</table>
			<div class="lpc__admin__order_banner__generate_label__edit_value__container">
				<span class="woocommerce-help-tip" data-tip="
				<?php
                echo __(
                    'Editing prices and weights may create inconsistency between CN23 or labels and invoice. Edit these values only if you really need it.',
                    'wc_colissimo'
                );
                ?>
				">
				</span>
                <?php echo __('Edit prices and weights', 'wc_colissimo'); ?>
				<span class="lpc__admin__order_banner__generate_label__edit_value woocommerce-input-toggle woocommerce-input-toggle--disabled"></span>
			</div>
			<div class="lpc__admin__order_banner__generate_label__shipping_costs__container">
				<label for="lpc__admin__order_banner__generate_label__shipping_costs">
                    <?php echo sprintf(__('Shipping costs (%s)', 'wc_colissimo'), $currency); ?>
				</label>
				<input type="number"
					   min="0"
					   step="any"
					   class="lpc__admin__order_banner__generate_label__shipping_costs"
					   id="lpc__admin__order_banner__generate_label__shipping_costs"
					   name="lpc__admin__order_banner__generate_label__shipping_costs"
					   value="<?php echo $shippingCosts; ?>"
					   readonly="readonly"
				>
			</div>
			<div class="lpc__admin__order_banner__generate_label__package_weight__container">
				<label for="lpc__admin__order_banner__generate_label__package_weight">
                    <?php echo sprintf(__('Packaging weight (%s)', 'wc_colissimo'), $weightUnity); ?>
				</label>
				<input type="number"
					   min="0"
					   step="any"
					   class="lpc__admin__order_banner__generate_label__package_weight"
					   name="lpc__admin__order_banner__generate_label__package_weight"
					   id="lpc__admin__order_banner__generate_label__package_weight"
					   value="<?php echo $args['lpc_packaging_weight']; ?>"
					   readonly="readonly"
				>
			</div>
			<div class="lpc__admin__order_banner__generate_label__total_weight__container">
                <?php echo __('Total weight (items + packaging)', 'wc_colissimo'); ?> :
				<span class="lpc__admin__order_banner__generate_label__total_weight"></span><?php echo ' ' . $weightUnity; ?>
				<input type="hidden" name="lpc__admin__order_banner__generate_label__weight__unity" value="<?php echo $weightUnity; ?>">
				<input type="hidden" name="lpc__admin__order_banner__generate_label__total_weight__input">
			</div>
			<div class="lpc__admin__order_banner__generate_label__generate-label-button__container">
				<select name="lpc__admin__order_banner__generate_label__outward_or_inward">
					<option value="outward"><?php echo __('Outward label', 'wc_colissimo'); ?></option>
					<option value="inward"><?php echo __('Inward label', 'wc_colissimo'); ?></option>
					<option value="both"><?php echo __('Outward and inward labels', 'wc_colissimo'); ?></option>
				</select>
				<button type="button" class="button button-primary lpc__admin__order_banner__generate_label__generate-label-button"><?php echo __(
                        'Generate',
                        'wc_colissimo'
                    ); ?></button>
			</div>
		</div>
	</div>
	<div class="lpc__admin__order_banner__content lpc__admin__order_banner__label_listing">
        <?php if (empty($trackingNumbers)) {
            $message = __(
                'You don\'t have any label for this order. To generate one, please check the "Labels generation" tab',
                'wc_colissimo'
            );

            echo '<br><div class="lpc__admin__order_banner__warning"><span>' . $message . '</span></div>';
        } else { ?>
			<table class="wp-list-table widefat fixed striped">
				<thead>
					<tr>
						<th><?php echo __('Outward labels', 'wc_colissimo'); ?></th>
						<th><?php echo __('Bordereau', 'wc_colissimo'); ?></th>
						<th><?php echo __('Inward labels', 'wc_colissimo'); ?></th>
					</tr>
				</thead>
				<tbody class="lpc__admin__order_banner__label_listing__body">
                    <?php
                    foreach ($trackingNumbers as $outwardTrackingNumber => $inwardTrackingNumbers) {
                        ?>
						<tr>
							<td>
                                <?php
                                if ('no_outward' !== $outwardTrackingNumber) {
                                    $trackingLink = $args['lpc_label_queries']->getOutwardLabelLink($args['postId'], $outwardTrackingNumber); ?>
									<a target="_blank" href="<?php echo esc_url($trackingLink); ?>">
                                        <?php esc_html_e($outwardTrackingNumber); ?>
									</a>
									<br>
                                    <?php echo $args['lpc_label_queries']->getOutwardLabelsActionsIcons(
                                        $outwardTrackingNumber,
                                        $labelFormat[$outwardTrackingNumber],
                                        $args['lpc_redirection']
                                    );
                                } ?>
							</td>
							<td>
                                <?php
                                if ('no_outward' !== $outwardTrackingNumber) {
                                    if (!empty($bordereauLinks[$outwardTrackingNumber])) {
                                        ?>
										<a target="_blank" href="<?php echo esc_url($bordereauLinks[$outwardTrackingNumber]['link']); ?>">
                                            <?php esc_html_e(sprintf(__('Bordereau n°%d', 'wc_colissimo'), $bordereauLinks[$outwardTrackingNumber]['id'])); ?>
										</a>
                                        <?php
                                    }
                                }
                                ?>
							</td>
							<td>
                                <?php foreach ($inwardTrackingNumbers as $inwardTrackingNumber) { ?>
                                    <?php echo $inwardTrackingNumber; ?>
									<br>
                                    <?php echo $args['lpc_label_queries']->getInwardLabelsActionsIcons(
                                        $inwardTrackingNumber,
                                        $labelFormat[$inwardTrackingNumber],
                                        $args['lpc_redirection']
                                    ); ?>
									<br>
                                <?php } ?>
							</td>
						</tr>
                    <?php } ?>
				</tbody>
			</table>
        <?php } ?>
	</div>
	<div class="lpc__admin__order_banner__content lpc__admin__order_banner__send_documents" style="display: none">
		<template id="lpc__admin__order_banner__send_documents__template">
			<tr>
				<td>
					<select class="lpc__admin__order_banner__document__type">
                        <?php
                        foreach ($args['lpc_documents_types'] as $documentType => $description) {
                            echo '<option value="' . esc_attr($documentType) . '">' . $description . '</option>';
                        }
                        ?>
					</select>
				</td>
				<td>
					<input
							type="file"
							name="lpc__customs_document[__PARCELNUMBER__][__TYPE__][]"
							class="lpc__admin__order_banner__document__file"
							disabled="disabled" />
				</td>
			</tr>
		</template>
		<table class="wp-list-table widefat striped">
			<thead>
                <?php foreach ($trackingNumbers as $outwardTrackingNumber => $inwardTrackingNumbers) { ?>
					<tr>
						<th><?php echo $outwardTrackingNumber; ?></th>
						<td class="lpc__admin__order_banner__send_documents__container">
							<table>
								<tbody class="lpc__admin__order_banner__send_documents__listing">
                                    <?php
                                    if (!empty($args['lpc_sent_documents'][$outwardTrackingNumber])) {
                                        foreach ($args['lpc_sent_documents'][$outwardTrackingNumber] as $oneDocument) {
                                            ?>
											<tr class="lpc__customs__sent__document">
												<td>
                                                    <?php esc_html_e($args['lpc_documents_types'][$oneDocument['documentType']]); ?>
												</td>
												<td>
                                                    <?php esc_html_e($oneDocument['documentName']); ?>
												</td>
											</tr>
                                            <?php
                                        }
                                    }
                                    ?>
								</tbody>
							</table>
							<div class="text-center">
								<button type="button"
										class="button lpc__admin__order_banner__send_documents__more"
										data-lpc-parcelnumber="<?php esc_attr_e($outwardTrackingNumber); ?>">
                                    <?php esc_html_e('Add an other document', 'wc_colissimo'); ?>
								</button>
								<button type="submit" class="button button-primary lpc__admin__order_banner__send_documents__listing__send_button">
                                    <?php esc_html_e('Submit the documents', 'wc_colissimo'); ?>
								</button>
							</div>
						</td>
					</tr>
                <?php } ?>
			</thead>
		</table>
		<div style="margin-top: 1rem;">
            <?php esc_html_e('In accordance with the customs regulation, it is necessary to provide documents related to the parcels for the customs.', 'wc_colissimo'); ?>
			<br />
            <?php esc_html_e('It is possible to send these documents through the parcel tracking tool or from here using the plugin.', 'wc_colissimo'); ?>
		</div>
	</div>
	<input type="hidden" name="lpc__admin__order_banner__generate_label__action" value="0">
	<input type="hidden" name="lpc__admin__order_banner__generate_label__items-id" value="<?php echo serialize($allItemsId); ?>">
</div>
