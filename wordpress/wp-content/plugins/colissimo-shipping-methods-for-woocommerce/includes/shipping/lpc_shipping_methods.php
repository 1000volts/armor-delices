<?php

class LpcShippingMethods extends LpcComponent {

    public function init() {
        add_action(
            'woocommerce_init',
            function () {
                require_once LPC_INCLUDES . 'shipping' . DS . 'lpc_expert.php';
                require_once LPC_INCLUDES . 'shipping' . DS . 'lpc_nosign.php';
                require_once LPC_INCLUDES . 'shipping' . DS . 'lpc_relay.php';
                require_once LPC_INCLUDES . 'shipping' . DS . 'lpc_sign.php';
            }
        );

        add_action(
            'woocommerce_shipping_init',
            function () {
                require_once LPC_INCLUDES . 'shipping' . DS . 'lpc_expert.php';
                require_once LPC_INCLUDES . 'shipping' . DS . 'lpc_nosign.php';
                require_once LPC_INCLUDES . 'shipping' . DS . 'lpc_relay.php';
                require_once LPC_INCLUDES . 'shipping' . DS . 'lpc_sign.php';
            }
        );

        add_filter(
            'woocommerce_shipping_methods',
            function ($shippingMethods) {
                if (class_exists('LpcExpert')) {
                    $shippingMethods[LpcExpert::ID] = LpcExpert::class;
                } else {
                    $shippingMethods['lpc_expert'] = 'LpcExpert';
                }

                if (class_exists('LpcNoSign')) {
                    $shippingMethods[LpcNoSign::ID] = LpcNoSign::class;
                } else {
                    $shippingMethods['lpc_nosign'] = 'LpcNoSign';
                }

                if (class_exists('LpcRelay')) {
                    $shippingMethods[LpcRelay::ID] = LpcRelay::class;
                } else {
                    $shippingMethods['lpc_relay'] = 'LpcRelay';
                }

                if (class_exists('LpcSign')) {
                    $shippingMethods[LpcSign::ID] = LpcSign::class;
                } else {
                    $shippingMethods['lpc_sign'] = 'LpcSign';
                }

                return $shippingMethods;
            }
        );

        add_filter('woocommerce_cart_shipping_method_full_label', [$this, 'addShippingIcon'], 10, 2);
    }

    public function getAllShippingMethods() {
        // can't use ::ID here because WC may not yet be defined
        return [
            'lpc_expert',
            'lpc_nosign',
            'lpc_relay',
            'lpc_sign',
        ];
    }

    public function getAllShippingMethodsWithName() {
        // can't use ::ID here because WC may not yet be defined
        return [
            'lpc_expert' => __('Colissimo Expert', 'wc_colissimo'),
            'lpc_nosign' => __('Colissimo without signature', 'wc_colissimo'),
            'lpc_relay'  => __('Colissimo relay', 'wc_colissimo'),
            'lpc_sign'   => __('Colissimo with signature', 'wc_colissimo'),
        ];
    }

    public function getAllColissimoShippingMethodsOfOrder(WC_Order $order) {
        $shipping_methods  = $order->get_shipping_methods();
        $shippingMethodIds = array_map(
            function (WC_Order_item_Shipping $v) {
                return ($v->get_method_id());
            },
            $shipping_methods
        );

        return array_intersect($this->getAllShippingMethods(), $shippingMethodIds);
    }

    public function getColissimoShippingMethodOfOrder(WC_Order $order) {
        $shippingMethod = $this->getAllColissimoShippingMethodsOfOrder($order);

        return reset($shippingMethod);
    }

    public function addShippingIcon($label, $method) {
        $img = '';
        if ('yes' === WC_Admin_Settings::get_option('display_logo') && in_array($method->get_method_id(), $this->getAllShippingMethods())) {
            $url   = plugins_url('/images/colissimo_icon.png', LPC_INCLUDES . 'init.php');
            $class = 'lpc_shipping_icon lpc_shipping_icon_' . $method->get_method_id();
            $style = 'max-width: 40px; display:inline; vertical-align: middle;';
            $img   = '<img src="' . $url . '" style="' . $style . '" class="' . $class . '"> ';
        }

        return $img . $label;
    }

}
