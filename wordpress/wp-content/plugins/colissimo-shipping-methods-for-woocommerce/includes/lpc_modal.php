<?php

/**
 * Class LpcModal
 */
class LpcModal {
    protected $templateId;
    protected $content;
    protected $title;

    public function __construct($content, $title = null, $templateId = null) {
        if (empty($templateId)) {
            $templateId = uniqid();
        }
        $this->templateId = $templateId;

        $this->content = $content;
        $this->title   = $title;

        $backboneUrl = plugins_url('backbone-modal.min.js', LpcHelper::getWooCommerceDir() . '/assets/js/admin/backbone-modal.min.js');
        LpcHelper::enqueueScript('wc-backbone-modal', null, $backboneUrl, ['wp-backbone']);
        $modalJS = plugins_url('/js/modal.js', __FILE__);
        LpcHelper::enqueueScript('lpc_modal', $modalJS, $modalJS, ['wc-backbone-modal']);

        $modalCSS = plugins_url('/css/modal.css', __FILE__);
        LpcHelper::enqueueStyle('lpc_modal', $modalCSS, $modalCSS);
    }

    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    public function echo_modal() {
        include LPC_INCLUDES . 'partials' . DS . 'modal' . DS . 'modal.php';

        return $this;
    }

    public function echo_button($buttonContent = null, $callback = null) {
        if (null === $buttonContent) {
            $buttonContent = __('Apply', 'wc_colissimo');
        }

        if (!empty($callback)) {
            $callback = 'data-lpc-callback="' . esc_attr($callback) . '"';
        }

        include LPC_INCLUDES . 'partials' . DS . 'modal' . DS . 'button.php';

        return $this;
    }

    public function echo_link($aContent = null, $callback = null) {
        if (null === $aContent) {
            $aContent = __('Apply', 'wc_colissimo');
        }

        if (!empty($callback)) {
            $callback = 'data-lpc-callback="' . esc_attr($callback) . '"';
        }

        include LPC_INCLUDES . 'partials' . DS . 'modal' . DS . 'link.php';

        return $this;
    }

    public function echo_modalAndButton($buttonContent = null) {
        return $this->echo_button($buttonContent)->echo_modal();
    }

    public function echo_modalAndLink($aContent = null) {
        return $this->echo_link($aContent)->echo_modal();
    }

}
