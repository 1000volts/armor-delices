��          �      �       H  W   I     �     �     �     �     �  A   �  �   )     �     �     �     �  @     7  P  k   �  *   �          1     E  
   V  V   a  �   �     �     �     �     �  @   �         	                                         
               An Extension for WooCommerce that lets you Preview Emails, without having to send them. Back to Admin Area Choose Email Choose Order Digamber Pradhan Mail to Note: E-mails require orders to exist before you can preview them Only use this field if you have particular orders, that are not listed above in the Choose Order Field. Type the Order ID only. Example: 90 Preview E-mails for WooCommerce Preview Emails Search Orders https://digamberpradhan.com/ https://www.digamberpradhan.com/preview-e-mails-for-woocommerce/ PO-Revision-Date: 2020-04-12 15:44:38+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - Preview E-mails for WooCommerce - Stable (latest release)
 Une extension pour WooCommerce qui vous laisse prévisualiser les emails, sans avoir besoin de les envoyer. Retour à l’interface d’administration Choisir l’email Choisir la commande Digamber Pradhan Envoyer à Note : les emails ont besoin de commandes existantes pour pouvoir les prévisualiser. Utiliser uniquement ce champ si vous avez des commandes spécifiques, qui ne sont pas listées ci-dessus dans le champ Choisir une commande. Saisir l’identifiant de commande uniquement. Exemple : 90 Preview E-mails for WooCommerce Prévisualiser les emails Rechercher des commandes https://digamberpradhan.com/ https://www.digamberpradhan.com/preview-e-mails-for-woocommerce/ 