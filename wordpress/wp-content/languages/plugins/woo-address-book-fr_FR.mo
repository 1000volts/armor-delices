��          �       ,      ,     -     =     V     c     t     {  d   �     �     �     
  B         c  G   |     �     �     �    �       )   )     S     `  
   q     |  y   �     �           8  (   R     {  E   �     �  
   �     �   Add New Address Add New Shipping Address Address Book Address nickname Delete Edit Gives your customers the option to store multiple shipping addresses and retrieve them on checkout.. Hall Internet Marketing Make Primary Shipping Address Book The following addresses are available during the checkout process. WooCommerce Address Book WooCommerce Address Book requires WooCommerce and has been deactivated. https://www.hallme.com/ optional required PO-Revision-Date: +0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - WooCommerce Address Book - Stable (latest release)
 Ajouter une nouvelle adresse Ajouter une nouvelle adresse de livraison Vos adresses Nom de l'adresse Supprimer  Modifier Donne la possibilité à vos clients d'enregistrer plusieurs adresses de livraison afin de les proposer lors du paiement. Hall Internet Marketing Choisir comme adresse principale Vos adresses de livraison Les adresses suivantes sont disponibles. WooCommerce Address Book WooCommerce Address Book requiert WooCommerce et a été désactivé. https://www.hallme.com/ facultatif obligatoire 