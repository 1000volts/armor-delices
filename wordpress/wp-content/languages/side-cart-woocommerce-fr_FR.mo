��    *      l  ;   �      �     �     �     �     �     �     �  	   �               $     <  
   E     P  ,   a  
   �     �     �     �     �     �     �     �  .        B     J  1   O     �     �  .   �  1   �     �     �          $     +     4     8     >     R     X     a  9  {     �     �     �  	   �     �     	     !  "   *     M     Z     z     �  
   �  8   �     �     �     	     	     4	     F	     Z	  
   p	  Y   {	     �	     �	  (   �	     
  	   
  <   (
  a   e
     �
     �
     �
       
                    	   7     A     M            
                                  '                            )       $   %                                                *          	      &             !                    #   "   (           %s applied successfully Add Address updated Apply Apply Coupon Available Coupons Calculate Calculate Shipping Cart Emptied Coupon has been removed Discount Empty Cart Enter Promo Code Enter your address to view shipping options. Get %s off Have a Promo Code? Item added back Item added to cart Item removed Item updated Only %s% in stock Please enter promo code Please use checkout form to calculate shipping Price:  Qty: Quantity can only be purchased in multiple of %s% Saved Shipping Shipping costs are calculated during checkout. Shipping options will be updated during checkout. Shipping to: Shipping updated Something went wrong Submit Subtotal Tax Total Unavailable Coupons Undo? [Remove] shoppers have bought this Project-Id-Version: 
PO-Revision-Date: 2021-05-26 15:47+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.3
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_attr_e
Last-Translator: 
Language: side
X-Poedit-SearchPath-0: .
 %s appliqué Ajouter au panier Adresse mise à jour Appliquer Ajouter un code promo Codes promo disponibles Calculer Calculer les frais d’expédition Panier vidé Le code promo a été supprimé Promo Panier vide Code promo Entrez votre adresse pour voir les options de livraison. Obtenez %s de reduction Vous avez un code promo ? Produit remis au panier Produit ajouté au panier Produit supprimé Produit mis à jour Plus que %s% en stock Code promo Merci d’utiliser le formulaire de validation de commande pour les frais d’expédition Prix :  Quantité : Choisissez une quantité multiple de %s% Sauvegardé Livraison Les coûts d’expédition seront calculé lors du paiement. Les options d’expéditions seront mise à jour dans le formulaire de validation de la commande. Envoyer à : Livraison mise à jour Une erreur est survenue Ok Sous-total TVA Total Codes promo non disponibles Annuler ? [Supprimer] ils ont aussi aimé 