const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const THEME_NAME = process.env["npm_package_name"] || "my-theme";
exports.THEME_NAME = THEME_NAME;

exports.compiler = function (mode) {
  const PRODUCTION = mode === "production";
  return {
    mode: mode,
    entry: "./src/index.js",

    output: {
      filename: "assets/bundle.[hash].js",
      path: path.resolve(__dirname, "dist", THEME_NAME),
      publicPath: "/wp-content/themes/" + THEME_NAME + '/',
    },

    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            PRODUCTION ? MiniCssExtractPlugin.loader : "style-loader",
            "css-loader",
            "postcss-loader",
            "sass-loader",
          ],
        },
        {
          test: /\.css$/,
          use: [
            PRODUCTION ? MiniCssExtractPlugin.loader : "style-loader",
            "css-loader",
          ],
        },
        {
          test: /\.(png|jpe?g|gif)$/,
          use: [
            {
              loader: "file-loader",
              options: { outputPath: 'files' }
            },
          ],
          exclude: /node_modules/,
        },
        {
          test: /\.(svg|eot|ttf|woff)$/,
          use: [
            {
              loader: "file-loader",
              options: { outputPath: 'files' }
            },
          ],
          exclude: /node_modules/,
        },
      ],
      // loaders: [
      // { test: /\.js$/, loader: 'babel?presets=env', exclude: /node_modules/ },
      // ]
    },

    plugins: [
      new HtmlWebpackPlugin({
        title: false,
        filename: "header.php",
        template: "src/templates/header.php",
        inject: "head",
        hash: true,
        minify: false,
      }),
      new CleanWebpackPlugin(),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: path.resolve(__dirname, "src", "templates"),
            to: ".",
            filter: function (path) {
              return !path.endsWith("src/templates/header.php");
            },
          },
        ],
      }),
      new MiniCssExtractPlugin({
        filename: "assets/bundle.[hash].css",
        chunkFilename: "[id].css",
        ignoreOrder: false,
      }),
    ],
  };
};
