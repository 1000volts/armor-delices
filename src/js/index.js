(function ($) {
  $(function () {
    const $main = $("#Main"),
      $body = $("body");
    
    $(function() { 
      AOS.init({ offset: 100, duration:700, easing:"ease-out-quad", once:!0, disable: window.innerWidth < 1024 });
      window.addEventListener('load', AOS.refresh);
     });   

      
    // Déclenche l'affichage du panier latéral lors du click sur le bouton panier du header
    $("#desktop-cart-open").click(function (evt) {
      evt.preventDefault();
      $(".xoo-wsc-modal").toggleClass("xoo-wsc-cart-active");
      $("body").toggleClass("xoo-wsc-cart-active");
      return false;
    });

    // MOBILE = Déclenche l'affichage du panier latéral lors du click sur le bouton panier du header
    $("#mobile-cart-open").click(function (evt) {
      evt.preventDefault();
      $(".xoo-wsc-modal").toggleClass("xoo-wsc-cart-active");
      $("body").toggleClass("xoo-wsc-cart-active");
      return false;
    });



    // masquer le label code promo lorque l'input est actif
    $("input#coupon_code").focusin(function() {
      $( this ).prev( "label" ).fadeOut( 500 );
    });

    $("input#coupon_code").focusout(function() {

      if( $(this).val() ) {
        return false;
      } else {
        $( this ).prev( "label" ).fadeIn( 500 );
      }
    });



    // Affichage du menu de compte au clic sur le bouton de compte
    jQuery(function ($) {
      var $sidebar = $("#login-sidebar"),
        $toggleButton = $("#desktop-login-open"),
        $toggleButtonMobile = $("#mobile-login-open"),
        $toggleButtonMobileBurger = $("#mobile-burger-login-open"),
        $createAccountButtonCheckout = $(".login-button-create-account-checkout"),
        $sidebarMask = $sidebar.find(".Sidebar-mask"),
        $sidebarClose = $sidebar.find(".Sidebar-closeBtn"),
        $sidebarContent = $sidebar.find(".Sidebar-content"),
        hasErrors = !!$sidebar.find(".woocommerce-error").length;

      $sidebar.each(function () {
        // Seulement s'il y a une sidebar

        // On force l'affichage si on trouve des erreurs dans le HTML de la sidebar
        if (hasErrors) {
          $sidebar.removeClass("hidden").show();
        }

        // Fonction de fermeture de la sidebar
        function close() {
          $sidebar
            .addClass("hidden")
            .animate({ display: "none" }, 250, function () {
              $sidebar.hide();
            });
          $body.removeClass("body-lock");
        }

        // Fonction d'ouverture de la sidebar
        function open() {
          $sidebar.show().removeClass("hidden");
          $body.addClass("body-lock");
        }

        // Fermeture au clic sur le background ou le bouton de fermeture
        $sidebarMask.click(close);
        $sidebarClose.click(close);

        $toggleButton.click(function (evt) {
          evt.preventDefault();
          open();
          return false;
        });

        $toggleButtonMobileBurger.click(function (evt) {
          evt.preventDefault();
          open();
          return false;
        });

        $createAccountButtonCheckout.click(function (evt) {
          evt.preventDefault();
          switchToAccountCreation();
          open();
          return false;
        });
        
      });

      // Gestion de la bascule connexion <> création de compte
      const $sidebarLogin = $("#LoginSidebarContent"),
        $sidebarRegister = $("#RegisterSidebarContent"),
        $createAccountButton = $("#login-button-create-account"),
        $backToLoginButton = $("#back-to-login-button"),
        $showLoginButton = $("a.showlogin"), // visible quand on essaie de s'inscrire avec un mail existant
        hasLoginErrors = !!$sidebarLogin.find(".woocommerce-error").length,
        hasRegisterErrors =
          !!$sidebarRegister.find(".woocommerce-error").length;

      function switchToAccountCreation() {
        $sidebarLogin.hide();
        $sidebarRegister.show();
      }

      function switchToLogin() {
        $sidebarLogin.show();
        $sidebarRegister.hide();
      }

      $createAccountButton.click(function (evt) {
        evt.preventDefault();
        switchToAccountCreation();
        return false;
      });

      $backToLoginButton.click(function (evt) {
        evt.preventDefault();
        switchToLogin();
        return false;
      });

      $showLoginButton.click(function (evt) {
        evt.preventDefault();
        switchToLogin();
        return false;
      });

      if (hasLoginErrors) {
        switchToLogin();
      }
      if (hasRegisterErrors) {
        switchToAccountCreation();
      }
    });


    // Popin Newsletter

    $('#cta-open-newsletter').click(function() {
      $('#popin-newsletter').show().removeClass("hidden");
      $body.addClass("body-lock");
    })

    $('#popin-newsletter-close').click(function() {
      $('#popin-newsletter')
      .addClass("hidden")
      .animate({ display: "none" }, 250, function () {
        $('#popin-newsletter').hide();
      });
      $body.removeClass("body-lock");
    })

    $('#popin-newsletter-mask').click(function() {
      $('#popin-newsletter')
      .addClass("hidden")
      .animate({ display: "none" }, 250, function () {
        $('#popin-newsletter').hide();
      });
      $body.removeClass("body-lock");
    })




    // Timeline
    $(".TimelineCheckout").each(function () {
      const $steps = $(".TimelineCheckout-item", this),
        $body = $(document.body);
      const $navButtons = $(".wpmc-nav-button");

      function updateTimeline() {
        const isCart = $body.hasClass("woocommerce-cart") && 1,
          isCheckout = $body.hasClass("woocommerce-checkout"),
          isLogin = isCheckout && !!$(".wpmc-step-login.current").length && 2,
          isAddresses =
            isCheckout &&
            (!!$(".wpmc-step-billing.current").length ||
              !!$(".wpmc-step-shipping.current").length) &&
            3,
          isPayment =
            isCheckout &&
            (!!$(".wpmc-step-review.current").length ||
              !!$(".wpmc-step-payment.current").length) &&
            4,
          isConfirm = $body.hasClass("woocommerce-order-received") && 5;
        const currentPage =
          isCart || isLogin || isAddresses || isPayment || isConfirm;

        $steps.each(function () {
          const $step = $(this);
          switch (this.id) {
            case "timeline-item-cart":
              $step.toggleClass("active", !!isCart);
              $step.toggleClass("valid", currentPage > 1);
              return;
            case "timeline-item-login":
              $step.toggleClass("active", !!isLogin);
              $step.toggleClass("valid", currentPage > 2);
              return;
            case "timeline-item-addresses":
              $step.toggleClass("active", !!isAddresses);
              $step.toggleClass("valid", currentPage > 3);
              return;
            case "timeline-item-payment":
              $step.toggleClass("active", !!isPayment);
              $step.toggleClass("valid", currentPage > 4);
              return;
            case "timeline-item-confirm":
              $step.toggleClass("active", !!isConfirm);
              $step.toggleClass("valid", currentPage > 5);
              return;
          }
        });
      }
      updateTimeline();
      $navButtons.click(function () {
        updateTimeline();
        // in case there is a slight delay before updating
        window.setTimeout(updateTimeline, 250);
        window.setTimeout(updateTimeline, 500);
        window.setTimeout(updateTimeline, 1000);
        window.setTimeout(updateTimeline, 1500);
      });
    });

    // Supprime le bouton "Ignorer l'identification dans le checkout"
    // Supprime le recap panier et la navigation etape suivante/panier sur la page de login du checkout (pas trouvé mieux…)
    //if ($(".wpmc-step-login").length > 0) {
    //  $(".mv_cart_totals, .wpmc-nav-wrapper, #wpmc-skip-login").remove();
    //}

    //SLIDERS
    $(".ProductsSlider-slides").slick({
      speed: 300,
      slidesToShow: 2,
      infinite: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    $(".Row-slick").slick({
      prevArrow: false,
      nextArrow: false,
      mobileFirst: true,
      centerMode: true,
      centerPadding: '10%',
      responsive: [
         {
            breakpoint: 768,
            settings: "unslick",
            centerPadding: '10%',
         }
      ]
    });

    $(".PolaroidSlider").slick({
      slidesToShow: 1,
      autoplay: true,
      infinite: true,
      cssEase: 'ease-out',
      speed: 500,
      dots: false,
      prevArrow: false,
      nextArrow: false,

    });

    $('.PolaroidSlider-left').click(function(){
      $('.PolaroidSlider').slick('slickPrev');
    })
    
    $('.PolaroidSlider-right').click(function(){
      $('.PolaroidSlider').slick('slickNext');
    })

    $(".up-sells > .products").slick({
      speed: 300,
      slidesToShow: 3,
      infinite: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          },
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          },
        },
      ],
    });

    // ACCORDEON FOOTER

    $('.FooterAccordion').click(function() {
      // verif si la fleche de l'accordeon est visible pour s'assurer d'etre en mobile
      if($(this).children('.FooterAccordion-btnArrow').css('display') == 'block') {
        if ($(this).next('.FooterAccordion-panel').is(':visible')) {
          $(this).next('.FooterAccordion-panel').slideUp();
          $(this).toggleClass('open');
          return false;
        } else {
          $(this).next('.FooterAccordion-panel').slideDown();
          $(this).toggleClass('open');
          return false;
        }
      }

    });


      // MENU BURGER

    $('#burgeropen').click(function() {
      $('#burger').show().removeClass("hidden");
      $body.addClass("body-lock");
    })
    
    $('#burgerclose').click(function() {
      $('#burger')
      .addClass("hidden")
      .animate({ display: "none" }, 250, function () {
        $('#burger').hide();
      });
      $body.removeClass("body-lock");
    })
    
    $('#burgermask').click(function() {
      $('#burger')
      .addClass("hidden")
      .animate({ display: "none" }, 250, function () {
        $('#burger').hide();
      });
      $body.removeClass("body-lock");
    })
        //myaccount
    $(".MenuBurger-myAccount .menu-item").click(function( evt ) {
      evt.preventDefault();
      $('.MenuBurger-myAccountExtra').toggleClass('open');
    })


    // STICKY HEADER

    var previousScroll = 0,

    headerHeight = $('#header').height();
    headerSticky = $('#headerSticky');

    $(window).scroll(function() {
      var currentScroll = $(this).scrollTop();
      if(currentScroll > headerHeight) {

          if (currentScroll > previousScroll) {
            headerSticky.removeClass('show');
          } else {
            headerSticky.show().addClass('show');
          }

      } else {
        headerSticky.removeClass('show');
      }

    previousScroll = currentScroll;

    });


    // POPIN POUR COMMANDE EN ATTENTE DE PAIEMENT

    // la popin est que sur la homepage
    if($body.hasClass("home")) {

      $stickyOrderWaiting = $('.StickyOrderWaiting');
      $stickyOrderWaitingClose = $('.StickyOrderWaiting-close');
      $stickyMessage = $('.StickyMessage'); // si on a un message en plus (ex : op ecolodge)

      $stickyOrderWaitingClose.click(function() {
        $stickyOrderWaiting.addClass('hidden');
        $stickyOrderWaiting.animate({ display: "none" }, 500, function () {
          $stickyOrderWaiting.hide();
          if($stickyMessage.length) {
            // on deplace le message en bas de l'écran si la popin est fermée
            $stickyMessage.addClass('moving');
          }
        });
      })

    }


    // MODIF SUR GALERY IMAGE WOOCOMMERCE (POUR VARIATIONS PRODUIT)

    // ne concerne que la page détail produit
    if( $body.hasClass('single-product') ) {

      $galleryPhotos = $('.woocommerce-product-gallery > ol.flex-control-nav');

      if($galleryPhotos.length) {
        // on enlève la class qui activ le mauvais changement de photos sur woocommerce lors du choix d'une variation
        $galleryPhotos.removeClass('flex-control-nav');

      }

    }

    
  });



})(jQuery, this);

