<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="SliderHome">
            <?php echo do_shortcode('[smartslider3 slider="2"]'); ?>
		</section>
		<!-- /section -->

        <section class="TemplateHome-section">
            <div class="PageTitle">
                <h2 class="PageTitle-h2">Nos produits</h2>
                <p class="PageTitle-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
            <div class="woocommerce">
                <ul class="products columns-2">
                    <?php
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => 2
                            );
                        $loop = new WP_Query( $args );
                        if ( $loop->have_posts() ) {
                            while ( $loop->have_posts() ) : $loop->the_post();
                                wc_get_template_part( 'content', 'product' );
                            endwhile;
                        } else {
                            echo __( 'No products found' );
                        }
                        wp_reset_postdata();
                    ?>
                </ul><!--/.products-->
            </div>
            <div class="TemplateHome-seeAll">
                <a href="<?php echo get_permalink( get_option( 'woocommerce_shop_page_id' ) ); ?>" title="<?php _e('Voir tous nos produits','woothemes'); ?>" class="Button"><?php _e('Voir tous nos produits','woothemes'); ?></a>
            </div>
        </section>

        <section class="TemplateHome-section">
            <div class="PageTitle">
                <h2 class="PageTitle-h2">La marque</h2>
                <p class="PageTitle-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
            <section class="HomeBrandItem">
                <div class="HomeBrandItem-img"></div>
                <div class="HomeBrandItem-txtContainer">
                    <h3 class="HomeBrandItem-title">Notre histoire</h3>
                    <p class="HomeBrandItem-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean posuere lorem enim, at congue nisi dapibus et.</p>
                </div>
            </section>                
            <section class="HomeBrandItem">
                <div class="HomeBrandItem-img"></div>
                <div class="HomeBrandItem-txtContainer">
                    <h3 class="HomeBrandItem-title">Notre savoir faire</h3>
                    <p class="HomeBrandItem-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean posuere lorem enim, at congue nisi dapibus et.</p>
                </div>
            </section>                
            <section class="HomeBrandItem">
                <div class="HomeBrandItem-img"></div>
                <div class="HomeBrandItem-txtContainer">
                    <h3 class="HomeBrandItem-title">Nos engagements</h3>
                    <p class="HomeBrandItem-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean posuere lorem enim, at congue nisi dapibus et.</p>
                </div>
            </section>                
        </section>

        <section class="TemplateHome-section">
            <div class="PageTitle">
                <h2 class="PageTitle-h2">Nos idées recettes</h2>
                <p class="PageTitle-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
            <div class="TemplateHome-seeAll">
                <a href="#" class="Button">Voir toutes nos recettes</a>
            </div>
        </section>


	</main>

<?php get_footer(); ?>
