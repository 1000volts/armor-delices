<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version'    => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);


require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';
require 'inc/wordpress-shims.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
	$storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

	require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
	require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';
	require 'inc/nux/class-storefront-nux-starter-content.php';
}

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */



/*------------------------------------*\
	WOOCOMMERCE ACTIONS
\*------------------------------------*/

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


//Change number or products per row to 2

add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 2; // 3 products per row
	}
}


// Enleve le nombre de resultat sur la page catalogue

add_action( 'after_setup_theme', 'my_remove_product_result_count', 99 );
function my_remove_product_result_count() { 
    remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
    remove_action( 'woocommerce_after_shop_loop' , 'woocommerce_result_count', 20 );
}


// Enleve le nombre de resultat sur la page catalogue

add_action( 'after_setup_theme', 'my_remove_product_order', 99 );
function my_remove_product_order() { 
    remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering', 30 );
}


// Enleve la description courte et les metas d'une fiche produit

add_action( 'after_setup_theme', 'my_remove_product_excerpt', 99 );
function my_remove_product_excerpt() { 
    remove_action( 'woocommerce_single_product_summary' , 'woocommerce_template_single_excerpt', 20 );
    remove_action( 'woocommerce_single_product_summary' , 'woocommerce_template_single_meta', 40 );
}

// Ajoute la description longue sous le nom du produit sur detail produit

add_action( 'after_setup_theme', 'add_description_to_product', 99 );
function add_description_to_product() { 
    add_action( 'woocommerce_single_product_summary' , 'woocommerce_product_description_tab', 6 );
}

// Enleve le titre "Description" de la description
add_filter( 'woocommerce_product_description_heading', '__return_null' );


// Ajoute la réassurance sur le detail produit

add_action( 'after_setup_theme', 'add_reassurance', 99 );
function add_reassurance() { 
    add_action( 'woocommerce_single_product_summary' , 'mv_display_reassurance', 60 );
}

// Ajoute les valeures de la marque sur le detail produit

add_action( 'after_setup_theme', 'add_brand_values', 99 );
function add_brand_values() { 
    add_action( 'woocommerce_after_single_product_summary' , 'mv_display_brand_values', 05 );
}


// Masque le titre produits apparentés 
add_filter( 'woocommerce_product_related_products_heading', function(){ return ''; } );


/*------------------------------------*\
	WOOCOMMERCE FUNCTIONS
\*------------------------------------*/

// Afficher la réassurance

if ( ! function_exists( 'mv_display_reassurance' ) ) {
	/**
	 * Affiche la réassurance
	 */
	function mv_display_reassurance() {
		get_template_part('parts/woocommerce-mv/reassurance');
	}
}

// Afficher les valeures de la marque

if ( ! function_exists( 'mv_display_brand_values' ) ) {
	/**
	 * Affiche la réassurance
	 */
	function mv_display_brand_values() {
		get_template_part('parts/woocommerce-mv/brand-values');
	}
}


// Ajoute un onglet ingrédient dans le tab du détail produit

add_filter( 'woocommerce_product_tabs', 'woo_ingredients_tab' );
function woo_ingredients_tab( $tabs ) {
	// Adds the new tab
	$tabs['ingredients_tab'] = array(
		'title' 	=> __( 'Ingrédients', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'woo_ingredient_tab_content'
	);
	return $tabs;
}

function woo_ingredient_tab_content() {
	// The new tab content
    get_template_part('parts/woocommerce-mv/ingredients');
}


// Ajoute un onglet ingrédient dans le tab du détail produit

add_filter( 'woocommerce_product_tabs', 'woo_infos_nutri_tab' );
function woo_infos_nutri_tab( $tabs ) {
	// Adds the new tab
	$tabs['info_nutri_tab'] = array(
		'title' 	=> __( 'Informations nutritionnelles', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'woo_infos_nutri_tab_content'
	);
	return $tabs;
}

function woo_infos_nutri_tab_content() {
	// The new tab content
    get_template_part('parts/woocommerce-mv/infos-nutritionnelles');
}


// Supprime les onglets inutiles d'une fiche produit
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;
}

// Affiche le titre pour les produits apparentés

if ( ! function_exists( 'mv_display_title_related_products' ) ) {
	/**
	 * Affiche la réassurance
	 */
	function mv_display_title_related_products() {
		get_template_part('parts/woocommerce-mv/related-products-title');
	}
}


