<div class="InfosNutritionnelles">

    <p class="InfosNutritionnelles-title">Valeurs nutritionnelles moyennes pour 100g</p>
    <ul class="InfosNutritionnelles-list">

        <li class="InfosNutritionnelles-listItem">
            <span class="InfosNutritionnelles-ListItemlabel">Énergie (kJ)</span>
            <span class="InfosNutritionnelles-ListItemValue">2029</span>
        </li>

        <li class="InfosNutritionnelles-listItem">
            <span class="InfosNutritionnelles-ListItemlabel">Énergie (kcal)</span>
            <span class="InfosNutritionnelles-ListItemValue">484</span>
        </li>

        <li class="InfosNutritionnelles-listItem">
            <span class="InfosNutritionnelles-ListItemlabel">Matières grasses (g)</span>
            <span class="InfosNutritionnelles-ListItemValue">20</span>
        </li>

        <li class="InfosNutritionnelles-listItem">
            <span class="InfosNutritionnelles-ListItemlabel">Dont acides gras saturés (g)</span>
            <span class="InfosNutritionnelles-ListItemValue">7</span>
        </li>

    </ul>

</div>