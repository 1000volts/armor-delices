<div class="Ingredients">

    <div class="Ingredients-content">
        <p class="Ingredients-title">Ingrédients</p>
        <p class="Ingredients-content">Farine de blé, sucre, œufs (23%), beurre concentré (22%), lait demi-écrémé.</p>
    </div>

    <div class="Ingredients-enSAvoirPlus">
        <span>En savoir plus sur le produit ?</span>
        <a href="#" class="Button">Consultez notre FAQ ou Contactez nous</a>
    </div>

</div>