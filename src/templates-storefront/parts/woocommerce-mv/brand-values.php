<div class="BrandValues">

    <div class="BrandValues-item">
        <span class="BrandValues-icon"></span>
        <span class="BrandValues-value">Fabriqué en Bretagne</span>
    </div>
    <div class="BrandValues-item">
        <span class="BrandValues-icon"></span>
        <span class="BrandValues-value">Des ingrédients rigoureusement sélectionnés</span>
    </div>
    <div class="BrandValues-item">
        <span class="BrandValues-icon"></span>
        <span class="BrandValues-value">La qualité, au cœur de notre engagement</span>
    </div>

</div>