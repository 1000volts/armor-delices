

<?php get_header(); ?>

<div id="primary" class="content-area">

	<main role="main" id="Main" class="Template404">

		<!-- Container -->
		<div class="Container">

			<!-- section -->
			<section>

			<div class="PageTitle">
				<h1 class="page-title">Page introuvable</h1>
			</div>

				<!-- contenu -->
				<section id="post-<?php the_ID(); ?>" class="PageContent">

					<div class="Template404-content">

						<h2>La page que vous recherchez est introuvable</h2>

						<div class="Template404-links">
							<a href="<?php echo home_url(); ?>" class="Button">Retour à l'accueil</a>
							<a href="<?php echo wc_get_page_permalink( 'shop' ); ?>" class="Button">La boutique en ligne</a>
						</div>

					</div>

				</section>
				<!-- /contenu -->


			</section>
			<!-- /section -->

		</div>
		<!-- /Container -->

	</main>

</div>

<?php get_footer(); ?>
