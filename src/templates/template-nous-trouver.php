<?php

/*
 * Template Name: Template - Où nous trouver
 */

$subtitlePage = get_field("subtitle_page");

?>


<?php get_header(); ?>

	<div id="primary" class="content-area">

		<main role="main" id="Main" class="TemplateOuTrouver">

			<!-- section -->
			<section>

                <div class="PageTitle">
                    <h1 class="page-title"><?php the_title();?></h1>
                    <?php if($subtitlePage) : ?>
                        <p class="PageTitle-txt"><?php echo $subtitlePage; ?></p>
                    <?php endif; ?>
                </div>

				<!-- section 1 -->
                <?php 

                    $findus_section_1_grp = get_field('findus_section_1_grp');

                ?>
				<section class="TemplateOuTrouver-sectionIntro">
                    <div class="Container">
                        <div class="Row">
                            <div class="Col Col-6 Col-sm-12" data-aos="fade-right">

                                <?php if($findus_section_1_grp['findUs_section_1_img']) : ?>
                                    <img src="<?php echo $findus_section_1_grp['findUs_section_1_img']; ?>" alt="Carte de france">
                                <?php endif; ?>

                            </div>
                            <div class="Col Col-6 Col-sm-12" data-aos="fade-left">

                                <?php if($findus_section_1_grp['findUs_section_1_img']) : ?>
                                    <div class="TemplateOuTrouver-sectionIntro-content Gutenberg">
                                        <?php echo $findus_section_1_grp['findus_section_1_txt']; ?>
                                    </div>
                                <?php endif; ?>                      

                            </div>
                        </div>
                    </div>
				</section>
				<!-- /section 1 -->

				<!-- section 2 -->
				<section class="TemplateOuTrouver-sectionCarte">
                    <div class="Container">
                        <div class="TemplateOuTrouver-sectionCarte-titre">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/illust_loupe_madeleine@2x.png" alt="" class="TemplateOuTrouver-sectionCarte-titre-loupe" data-aos="fade-up" data-aos-delay="600">
                            <h2 data-aos="fade-up" data-aos-delay="400">Trouver un magasin<br/> proche de chez vous</h2>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/line_dashed_arc_right.png" alt="" class="TemplateOuTrouver-sectionCarte-titre-ligne" data-aos="fade-up" data-aos-delay="800">
                        </div>
                    </div>
                    <div class="TemplateOuTrouver-sectionCarte-carte">
                        <div class="Container">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/double_paquet_madeleine_nature_cut@2x.png" 
                            alt="" class="TemplateOuTrouver-sectionCarte-carte-sachets" data-aos="fade" data-aos-delay="1400">
                            <?php echo do_shortcode("[wpsl]"); ?>
                        </div>
                        <div class="TemplateOuTrouver-backtohome" data-aos="fade-up">
                            <a href="<?php echo (get_permalink(64)); ?>" class="Button Button--white">retour à l’accueil</a>
                        </div>
                    </div>
				</section>
				<!-- /section 2 -->



			</section>
			<!-- /section -->

		</main>

	</div>

<?php get_footer(); ?>