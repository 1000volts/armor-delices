<?php

/*
 * Template Name: Template histoire
 */


$step1Title = get_field("history_step1_title");
$step2Title = get_field("history_step2_title");
$step3Title = get_field("history_step3_title");
$step4Title = get_field("history_step4_title");

$step1Txt = get_field("history_step1_txt");
$step2Txt = get_field("history_step2_txt");
$step3Txt = get_field("history_step3_txt");
$step4Txt = get_field("history_step4_txt");

?>


<?php get_header(); ?>

	<main role="main" class="TemplateHistoire" id="Main">


		<!-- section -->
		<section>

            <div class="PageTitle">
                <h1 class="page-title"><?php the_title();?></h1>
                <p class="PageTitle-txt">Des terres de tradition naissent des produits de légende</p>
            </div>

            <div class="Container">

                <!-- history -->
                <div class="History">

                    <div class="HistoryStep HistoryStep--1">
                        <div class="History-line History-line--1" data-aos="fade-right"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line1.png" alt=""></div>
                        <div class="History-mobile-line History-mobile-line--1"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line_mobile_1.png" alt=""></div>
                        <div class="HistoryStep-content">
                            <div class="HistoryStep-colImg" data-aos="fade-right" data-aos-delay="200">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step1.png" 
                                     srcset="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step1@2x.png" alt="">
                            </div>
                            <div class="HistoryStep-colTxt" data-aos="fade-left" data-aos-delay="400">

                                <?php if($step1Title) : ?>
                                    <h2><?php echo($step1Title); ?></h2>
                                <?php endif; ?>

                                <?php if($step1Txt) : ?>
                                    <?php echo($step1Txt); ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>

                    <div class="HistoryStep HistoryStep--2">
                        <div class="History-line History-line--2"  data-aos="fade-left" data-aos-delay="600"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line2.png" alt=""></div>
                        <div class="HistoryStep-content">
                            <div class="HistoryStep-colImg" data-aos="fade-left" data-aos-delay="800">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step2.png" 
                                     srcset="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step2@2x.png" alt="">
                            </div>
                        </div>
                    </div>


                    <div class="HistoryStep HistoryStep--3">
                        <div class="History-line History-line--3" data-aos="fade-left"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line3.png" alt=""></div>
                        <div class="History-mobile-line History-mobile-line--2"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line_mobile_2.png" alt=""></div>
                        <div class="HistoryStep-content">
                            <div class="HistoryStep-colTxt" data-aos="fade-right" data-aos-delay="400" data-aos-anchor=".HistoryStep--3">

                                <?php if($step2Title) : ?>
                                    <h2><?php echo($step2Title); ?></h2>
                                <?php endif; ?>

                                <?php if($step2Txt) : ?>
                                    <?php echo($step2Txt); ?>
                                <?php endif; ?>

                            </div>
                            <div class="HistoryStep-colImg" data-aos="fade-left" data-aos-delay="200" data-aos-anchor=".HistoryStep--3">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step3.png" 
                                     srcset="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step3@2x.png" alt="">
                            </div>
                        </div>
                    </div>  

                    <div class="HistoryStep HistoryStep--4">
                        <div class="History-line History-line--4" data-aos="fade-right"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line4.png" alt=""></div>
                        <div class="History-mobile-line History-mobile-line--3"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line_mobile_3.png" alt=""></div>
                        <div class="HistoryStep-content">
                            <div class="HistoryStep-colImg" data-aos="fade-right" data-aos-delay="200">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step4.png" 
                                     srcset="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step4@2x.png" alt="">
                            </div>
                            <div class="HistoryStep-colTxt" data-aos="fade-left" data-aos-delay="400">

                                <?php if($step3Title) : ?>
                                    <h2><?php echo($step3Title); ?></h2>
                                <?php endif; ?>

                                <?php if($step3Txt) : ?>
                                    <?php echo($step3Txt); ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div> 

                    <div class="HistoryStep HistoryStep--5">
                        <div class="History-line History-line--5" data-aos="fade-right"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line5.png" alt=""></div>
                        <div class="History-mobile-line History-mobile-line--4"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line_mobile_4.png" alt=""></div>
                        <div class="HistoryStep-content">
                            <div class="HistoryStep-colTxt" data-aos="fade-right" data-aos-delay="800">
                                <?php if($step4Title) : ?>
                                    <h2><?php echo($step4Title); ?></h2>
                                <?php endif; ?>
                                
                                <?php if($step4Txt) : ?>
                                    <?php echo($step4Txt); ?>
                                <?php endif; ?>

                            </div>
                            <div class="HistoryStep-colImg" data-aos="fade-left" data-aos-delay="400">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step5.png" 
                                     srcset="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step5@2x.png" alt="">
                            </div>
                        </div>
                    </div> 

                    <div class="HistoryStep HistoryStep--6">
                        <div class="History-line History-line--6" data-aos="fade-left" data-aos-delay="400"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line6.png" alt=""></div>
                        <div class="HistoryStep-content">
                            <div class="HistoryStep-colImg" data-aos="fade-right" data-aos-delay="1000">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step6.png" 
                                     srcset="<?php echo get_template_directory_uri(); ?>/img/histoire/histoire_step6@2x.png" alt="">
                            </div>
                        </div>
                        <div class="History-line History-line--7" data-aos="fade-right" data-aos-delay="800"><img src="<?php echo get_template_directory_uri(); ?>/img/histoire/line7.png" alt=""></div>
                    </div> 

                </div>
                <!-- /history -->

                <div class="TemplateHistoire-backToHome" data-aos="fade-up">
                    <a href="<?php echo (get_permalink(64)); ?>" class="Button">retour à l’accueil</a>
                </div>

            </div>



		</section>
		<!-- /section -->

	</main>

<?php get_footer(); ?>
