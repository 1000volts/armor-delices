<?php

/*
 * Template Name: Template FAQ
 */

$subtitlePage = get_field("subtitle_page");

?>

<?php get_header(); ?>

	<div id="primary" class="content-area">

		<main role="main" id="Main" class="TemplateFAQ">

			<!-- Container -->
			<div class="Container">

				<!-- section -->
				<section>

                    <div class="PageTitle">
                        <h1 class="page-title"><?php the_title();?></h1>
                            <?php if($subtitlePage) : ?>
                                <p class="PageTitle-txt"><?php echo $subtitlePage; ?></p>
                            <?php endif; ?>
                    </div>

					<!-- contenu -->
					<section id="post-<?php the_ID(); ?>" class="PageContent">

                        <div class="Gutenberg">

					        <?php the_content(); ?>

                        </div>

					</section>
					<!-- /contenu -->


				</section>
				<!-- /section -->

			</div>
			<!-- /Container -->

		</main>

	</div>

<?php get_footer(); ?>

