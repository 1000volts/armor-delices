

			<?php /* <div class="StickyContact">
				<a href="<?php echo get_permalink(74); ?>" class="Button Button--icon">
				<span class="Button-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/help_outline_white_24dp.svg" alt=""></span>
				<span class="Button-txt"><?php _e('Nous contacter','woothemes'); ?></span>
				</a>
			</div>
			*/ ?>

			
			<?php // popin commande en attente de paiement ?>

			<?php if( is_front_page() && is_user_logged_in() ) : ?>
				
				<?php 
					// recupère la derniere commande en attente de paiement
					$url_last_waiting_order = get_user_order_waiting_for_payment_url();

					$args = array (
						'order_url' => $url_last_waiting_order,
					);
				?>
			
				<?php if( $url_last_waiting_order != "" ) : // si commande en attente on affiche le message ?> 
												
						<div class="StickyOrderWaiting" data-aos="fade-up" data-aos-delay="300">
							<div class="StickyOrderWaiting-container">
								<button class="StickyOrderWaiting-close"></button>
								<?php get_template_part("parts/message-order-waiting-payment", null, $args); ?>
							</div>
						</div>					

				<?php endif; ?>
			
			<?php endif; ?>

			<?php // fin popin commande en attente de paiement ?>
			

			<?php // pastille ecolodge ?>
			<?php if(show_ecolodge()) : ?>
				<div class="StickyMessage" data-aos="fade-up">
					<a href="https://www.jeu-bien-etre-armor-delices.fr/" target="_blank" rel="noreferrer noopener">
						<img src="<?php echo get_template_directory_uri();?>/img/ecolodge/ecolodge_pastille.png" 
						alt="À gagner, un séjour bien-être en Bretagne">
					</a>
				</div>
			<?php endif; ?>
		

			<!-- footer -->
			<footer class="Footer" role="contentinfo">

			<div class="FooterReassurance">
				<div class="Container">
					<div class="Row">
						<div class="Col Col-3 Col-xs-6">
						<div class="FooterReassurance-item">
							<a href="<?php echo get_permalink(80); ?>" class="FooterReassurance-linkFAQ">
								<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_reassurance_cb2.png" 
									 class="FooterReassurance-icon" height="40" width="40" alt="Paiement sécurisé">
								<p class="FooterReassurance-title">Paiement sécurisé</p>
								<p class="FooterReassurance-txt">CB, Visa, Mastercard</p>
							</a>
						</div>
						</div>

						<div class="Col Col-3 Col-xs-6">
						<div class="FooterReassurance-item">
							<a href="<?php echo get_permalink(80); ?>" class="FooterReassurance-linkFAQ">
								<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_reassurance_shipping2.png" 
								 class="FooterReassurance-icon" height="40" width="40" alt="Expédition soignée">
								<p class="FooterReassurance-title">Expédition soignée</p>
								<p class="FooterReassurance-txt">sous 72h ouvrées</p>
							</a>
						</div>
						</div>

						<div class="Col Col-3 Col-xs-6">
						<div class="FooterReassurance-item">
							<a href="<?php echo get_permalink(80); ?>" class="FooterReassurance-linkFAQ">
								<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_reassurance_delivery2.png" 
								 class="FooterReassurance-icon" height="40" width="40" alt="Livraison">
								<p class="FooterReassurance-title">Livraison</p>
								<p class="FooterReassurance-txt">offerte dès 50€ TTC</p>
							</a>
						</div>
						</div>

						<div class="Col Col-3 Col-xs-6">
						<div class="FooterReassurance-item">
							<span class="FooterReassurance-linkFAQ FooterReassurance-linkFAQ--fake">
								<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_reassurance_letter2.png" 
								 class="FooterReassurance-icon" height="40" width="40" alt="Mots doux">
								<p class="FooterReassurance-title">Mots doux</p>
								<p class="FooterReassurance-txt">dans votre colis</p>
							</span>
						</div>
						</div>
					</div>
				</div>
			</div>

			<div class="FooterLinks">
				<div class="Container">
					<div class="Row">
						<div class="Col Col-3 Col-sm-6 Col-xs-12  Col-xs--fullWidth">
							<div class="FooterLinks-bloc FooterLinks-bloc--first"> 
								<span class="FooterLinks-title FooterAccordion">Coordonnées<button class="FooterAccordion-btnArrow"></button></span>
								<div class="FooterAddress FooterAccordion-panel">
									<?php 

										$store_address     = get_option( 'woocommerce_store_address' );
										$store_address_2   = get_option( 'woocommerce_store_address_2' );
										$store_city        = get_option( 'woocommerce_store_city' );
										$store_postcode    = get_option( 'woocommerce_store_postcode' );
										$store_mail = get_field("options_info_mag_mail", 'options');
										
									?>
									<span class="FooterAddress-name">Armor Délices</span>
									<span class="FooterAddress-street"><?php echo $store_address_2; ?></span>
									<span class="FooterAddress-city"><?php echo $store_postcode . " " . $store_city; ?></span>
									<span class="FooterAddress-mail"><a href="<?php echo get_permalink(74); ?>" class="Button Button--medium Button--white">Nous contacter</a></span>

									<div class="FooterAddress-groupe">
										<a href="https://www.gouters-magiques.com/" target="_blank" rel="noreferrer noopener">
											<img src="<?php echo get_template_directory_uri();?>/img/groupe_gouters_magiques@2x.png" alt="Les marques du groupe Goûters Magiques" class="FooterAddress-groupeLogos">
										
										<p class="FooterAddress-groupeTxt">Armor Délices, une marque du groupe <span>Goûters&nbsp;Magiques</span></p></a>
									</div>
								</div>
							</div>
						</div>
						
						<div class="Col Col-3 Col-sm-6 Col-xs-12 Col-xs--fullWidth">
							<div class="FooterLinks-bloc"> 
								<span class="FooterLinks-title FooterAccordion">Plan du site<button class="FooterAccordion-btnArrow"></button></span>
								<div class="FooterLinks-list FooterAccordion-panel">
									<a href="<?php echo get_permalink(66); ?>">Notre histoire</a>
									<a href="<?php echo get_permalink(68); ?>">Notre savoir-faire</a>
									<a href="<?php echo get_permalink(70); ?>">Nos engagements</a>
									<a href="<?php echo get_permalink(6); ?>">Boutique en ligne</a>
									<a href="<?php echo get_permalink(74); ?>">Nous contacter</a>
									<a href="<?php echo get_permalink(80); ?>">FAQ</a>
								</div>
							</div>
						</div> 
						<div class="Col Col-3 Col-sm-6 Col-xs-12 Col-xs--fullWidth">
							<div class="FooterLinks-bloc"> 
								<span class="FooterLinks-title FooterAccordion">Informations<button class="FooterAccordion-btnArrow"></button></span>
								<div class="FooterLinks-list FooterAccordion-panel">
									<a href="<?php echo get_permalink(84); ?>">Mentions légales et CGU</a>
									<a href="<?php echo get_permalink(86); ?>">CGV</a>
									<a href="<?php echo get_permalink(3); ?>">Politique de confidentialité</a>
									<hr class="FooterLinks-sep">
									<a href="https://www.gouters-magiques.com/offre-emplois-groupe-gouters-magiques/p7.html" target="_blank" rel="noreferrer noopener">Recrutement</a>
									<a href="https://www.mangerbouger.fr/" target="_blank" rel="noreferrer noopener">Manger Bouger</a>
								</div>
							</div>
						</div>
						<div class="Col Col-3 Col-sm-6 Col-xs-12 Col-xs--fullWidth">
							<div class="FooterLinks-bloc FooterLinks-bloc--newsletter"> 
								<span class="FooterLinks-title">Newsletter</span>
								<div class="Form Form--newsletter">
									<p class="FooterLinks-info">Inscrivez-vous à la newsletter pour recevoir toutes nos offres exclusives.</p>
									<button class="Button Button--medium Button--white" id="cta-open-newsletter">
											<?php _e('S\'inscrire','woothemes'); ?>
									</button>
								</div>							
								<div class="Footer-socials">
									<?php get_template_part('parts/socials', null, array('color' => 'white')); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</footer>
			<!-- /footer -->


		</div>
		<!-- /root -->

		<?php wp_footer(); ?>


		<script>
  var rellax = new Rellax('.rellax', {
	//wrapper: '.TemplateHome-section--brand'
  });
</script>

		

	</body>
</html>
