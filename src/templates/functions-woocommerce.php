<?php

///////////////// Styles woocommerce off
add_filter('woocommerce_enqueue_styles', '__return_false');


///////////////// Permet l'interaction avec Woocommerce 
function mytheme_add_woocommerce_support()
{
    add_theme_support('woocommerce');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support( 'wc-product-gallery-slider' );
}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');


/*------------------------------------*\
	WOOCOMMERCE ACTIONS
\*------------------------------------*/


//Change number or products per row to 2

add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
    function loop_columns()
    {
        return 3; // 3 products per row
    }
}


//Enleve le breadcrumb exclusif à woocomerce
add_action('after_setup_theme', 'mv_remove_woo_breadcrumb', 99);
function mv_remove_woo_breadcrumb()
{
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
}


// Enleve le nombre de resultat sur la page catalogue

add_action('after_setup_theme', 'my_remove_product_result_count', 99);
function my_remove_product_result_count()
{
    remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
    remove_action('woocommerce_after_shop_loop', 'woocommerce_result_count', 20);
}


// Enleve le nombre de resultat sur la page catalogue

add_action('after_setup_theme', 'my_remove_product_order', 99);
function my_remove_product_order()
{
    remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
}


// Affiche les infos nécessaire sur le détail produit

add_action('after_setup_theme', 'my_remove_product_excerpt', 99);
function my_remove_product_excerpt()
{
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
}


// Ajoute la description longue sous le nom du produit sur detail produit

add_action('after_setup_theme', 'add_description_to_product', 99);
function add_description_to_product()
{
    add_action('woocommerce_single_product_summary', 'woocommerce_product_description_tab', 6);
}


// Enleve le titre "Description" de la description du produit
add_filter('woocommerce_product_description_heading', '__return_null');


// Ajoute la réassurance sur le detail produit

add_action('after_setup_theme', 'add_reassurance', 99);
function add_reassurance()
{
    add_action('woocommerce_single_product_summary', 'mv_display_reassurance', 60);
}


// Ajoute les valeures de la marque sur le detail produit

add_action('after_setup_theme', 'add_brand_values', 99);
function add_brand_values()
{
    add_action('woocommerce_after_single_product_summary', 'mv_display_brand_values', 12);
}


// Ajoute un carroussel sur page catalog

function add_catalog_slider()
{
    add_action('woocommerce_before_main_content', 'mv_display_catalog_slider', 05);
}

// Masque le titre produits apparentés 
add_filter('woocommerce_product_related_products_heading', function () {
    return '';
});


// Affiche le poids d'un produit sur la miniature
//add_action('woocommerce_shop_loop_item_title',  'mv_display_product_weight');

// Affiche le poids d'un produit sur la miniature
add_action('woocommerce_before_shop_loop_item_title',  'mv_product_title_container_before');
add_action('woocommerce_after_shop_loop_item_title',  'mv_product_title_container_after');

// Affiche dans le panier latéral un texte type "Plus que xx euros pour livraison gratuite"
add_action('xoo_wsc_after_products', 'mv_free_shipping_cart_notice');

// Ajoute une div container sur les pages woocommerce
add_action('woocommerce_before_main_content', 'mv_add_container_for_wc');
add_action('woocommerce_after_main_content', 'mv_close_container_for_wc');

// Créer les blocs pour les distributeur
add_action('woocommerce_after_shipping_rate', 'mv_show_shipping_item', 20, 2);

// Affiche le recap de la commande sur des étapes du checkout
add_action('woocommerce_checkout_before_customer_details', 'mv_show_recap_checkout');

// Ajoute un onglet ingrédient dans le tab du détail produit
add_filter('woocommerce_product_tabs', 'mv_ingredients_tab');

// Ajoute un onglet ingrédient dans le tab du détail produit
add_filter('woocommerce_product_tabs', 'woo_infos_nutri_tab');

// Ajoute un onglet description dans le tab du détail produit
add_filter('woocommerce_product_tabs', 'mv_description_tab');

// Enleve le champ d'infos additionnels dans le checkout
add_filter('woocommerce_enable_order_notes_field', '__return_false', 9999);
add_filter('woocommerce_checkout_fields', 'remove_order_notes');

// Titre au dessus du bloc inscription dans le checkout
add_filter('woocommerce_checkout_logged_in_message', function ($text) {
    return 'Je m\'identifie';
});

// Masque le bloc code promo dans le checkout
remove_action('wpmc-woocommerce_checkout_coupon_form', 'woocommerce_checkout_coupon_form', 10);

// Supprimme le bloc related product dans la fiche produit
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

// Supprime le bloc cross selling dans le panier
remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');

// Affiche le texte combiné de la description courte + poids + prix unitaire d'un produit
add_action('woocommerce_single_product_summary', 'mv_add_exerpt_price_weight', 6);

// Ajoute une div pour clear les floats sous le product summary
add_action('woocommerce_after_single_product_summary', 'mv_add_clear_div', 05);

// Ajoute un bonjour Nom + prénom avant le formulaire d'édition de compte 
add_action('woocommerce_before_edit_account_form', 'mv_add_hello_user_edit_account', 10, 0);

// Modifie le nom des colonnes sur le tableau d'historique des commandes
add_filter('woocommerce_account_orders_columns', 'mv_change_orders_tab_heading_name', 10, 1);

// Supprime l'information de stock sur la fiche produit
add_filter('woocommerce_get_stock_html', '__return_empty_string', 10, 2);

// Supprime les onglets inutiles d'une fiche produit
add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);

// Gestion des menus sur la page mon compte
add_filter('woocommerce_account_menu_items', 'my_account_menu_order');

// Ajout du champ civilité sur le formulaire d'inscription    
add_action('woocommerce_register_form_start', 'mv_add_register_form_field');

// Ajoute le prix selon la quantité sur une page produit
add_action('woocommerce_after_add_to_cart_quantity', 'woocommerce_total_product_price', 31);

// Affiche un message disponible sur une fiche produit
add_filter('woocommerce_get_availability', 'custom_override_get_availability', 10, 2);

// Ajouter une checkbox pour les CGV sur le formulaire d'inscription
add_action('woocommerce_register_form', 'mv_add_registration_privacy_policy', 11);

// Déplie le bandeau code promo automatiquement (attention le toggl est masqué en css)
add_action('wp_footer', 'woocommerce_show_coupon', 99);

// Enregistrement de la valeur du champ civilité
add_action('woocommerce_created_customer', 'mv_save_register_fields');

// Ajoute un champ un nom au formulaire d'inscription
add_action('woocommerce_register_form_start', 'mv_add_name_woo_account_registration');

// Rend les champs noms et prénom obligatoire dans le formulaire d'inscription
add_action('woocommerce_created_customer', 'mv_save_name_fields');

// Validation et verification du champ civilité
add_action('woocommerce_register_post', 'mv_validate_field', 10, 3);

// Rend le champ checkbox des CGV obligatoire
add_filter('woocommerce_registration_errors', 'bbloomer_validate_privacy_registration', 10, 3);

// Redirection des pages de catégories (inutiles) vers la page du catalog
add_action('template_redirect', 'wc_redirect_to_shop');

// redirection du dashboard "mon compte" vers la page orders
add_action('template_redirect', 'misha_redirect_to_orders_from_dashboard');

// changement du label pour le code promo
add_filter('gettext', 'bt_rename_coupon_field_on_cart', 10, 3);

// Change le label du bouton de paiement de la commande
add_filter('woocommerce_order_button_text', 'custom_order_button_text', 1);

// champ téléphone optionnel sur formulaire checkout
//add_filter( 'woocommerce_billing_fields', 'ts_unrequire_wc_phone_field');

// disable le champs pays
//add_filter( 'woocommerce_shipping_fields', 'checkout_shipping_country_field_disabled' );

// disable le champs pays
//add_filter('woocommerce_billing_fields', 'checkout_billing_country_field_disabled');

// Affiche les horaires d'ouverture (utilisé dans l'email local pickup)
add_action('mv_advanced_local_pickup_custom_hours', 'mv_display_open_hours', 10);

// Message d'information pour le numéro de téléphone au niveau du checkout
add_action('woocommerce_after_checkout_billing_form', 'mv_show_info_for_phone_pickup', 10);

// Message en cas d'aucune option de livraison dispo
add_filter('woocommerce_no_shipping_available_html', 'my_custom_no_shipping_message');
add_filter('woocommerce_cart_no_shipping_available_html', 'my_custom_no_shipping_message');
add_filter('woocommerce_shipping_may_be_available_html', 'my_custom_no_shipping_message');

// Dans le panier, ajouter le message pour les commandes types CE
add_action('woocommerce_after_cart_totals', 'mv_pro_order_message', 10);
    
// disable le champs pays
//add_filter('woocommerce_after_checkout_billing_form', 'checkout_country_hidden_fields_replacement');

// Redirection sur la homepage après logout
add_filter('logout_url', 'redirect_after_logout', 10, 2);

// Modifie le total sur la facture + ajout d'une ligne TVA
//add_action("mv_add_add_line_invoice_totals", "mv_add_tva_pdf_invoice_totals", 10, 3);

// Bloque la commande à 30kg 
add_action('woocommerce_check_cart_items','check_cart_weight');

// Paramètres du flexslider woocommerce sur la page détail produit
add_filter( 'woocommerce_single_product_carousel_options', 'customslug_single_product_carousel_options', 99, 1 );

// Ajoute un message après l'historique de commande d'un client
add_action('woocommerce_before_account_orders_pagination', 'mv_add_message_order_history');

// Modifie les labels et ajoute le montant de la tva pour la livraison sur détail commande
add_filter( 'woocommerce_get_order_item_totals', 'renaming_shipping_order_item_totals', 10, 3 );

// affiche les informations du vendeur, utilisé sur les factures et note de remboursement
add_action('mv_shop_info_invoice', 'show_shop_infos_on_invoice', 10, 0);
add_action('mv_shop_info_credit_note', 'show_shop_infos_on_invoice', 10, 0);

// enleve le détail de la commande sur la page de thank you
remove_action('woocommerce_thankyou', 'woocommerce_order_details_table', 10);

// deplacement de l'ajout au panier sur page détail produit
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 6 );


/*------------------------------------*\
	WOOCOMMERCE FUNCTIONS
\*------------------------------------*/


// Bloque la commande à 30kg 

function check_cart_weight(){

    $weight = WC()->cart->cart_contents_weight;
    $weightMax = 30000;

        if( $weight > $weightMax ){
            wc_add_notice( sprintf( __( '<strong>Votre commande dépasse le poids maximum pris en charge via la boutique en ligne.</strong><br/>
            Nous vous invitons à prendre contact avec le gestionnaire de boutique à <a href="mailto:eboutique@armor-delices.com">eboutique@armor-delices.com</a>
             ainsi que pour toute commande professionnelle (Comité d’entreprise, association …). Nous reviendrons vers vous sous 24h ouvrées.<br/>
            Merci de votre compréhension !', 'woocommerce' ), $weight ), 'error' );
        }

}


// Modifie le nom des colonnes sur le tableau d'historique des commandes

function mv_change_orders_tab_heading_name($columns)
{
    $columns['order-number'] = __('N°', 'woocommerce');
    $columns['order-date'] = __('Date', 'woocommerce');
    $columns['order-status'] = __('Statut', 'woocommerce');
    $columns['order-total'] = __('Montant', 'woocommerce');
    $columns['order-actions'] = __('', 'woocommerce');

    return $columns;
}


// Ajoute un bonjour Nom + prénom avant le formulaire d'édition de compte 

function mv_add_hello_user_edit_account()
{

    $currentUser = get_current_user_id();
    $currentUserMeta = get_user_meta($currentUser);
?>

    <div class="woocommerce-MyAccount-hello">
        Bonjour
        <span class="woocommerce-MyAccount-hello-firstname"><?php echo $currentUserMeta['first_name'][0]; ?></span>
        <span class="woocommerce-MyAccount-hello-lastname"><?php echo $currentUserMeta['last_name'][0]; ?></span>
    </div>

    <?php
}


// Ajoute une div vide pour clear les float

if (!function_exists('mv_add_clear_div')) {

    function mv_add_clear_div()
    {

        echo '<div style="clear:both;"></div>';
    }
}


// Affiche le texte combiné de la description courte + poids + prix unitaire d'un produit

if (!function_exists('mv_add_exerpt_price_weight')) {

    function mv_add_exerpt_price_weight()
    {
        global $product;        

        if ($product->get_short_description()) {
            $short_description = $product->get_short_description() . " - ";
        } else {
            $short_description = "";
        }

        if ($product->get_weight()) {
            $product_weight = $product->get_weight() . "g - ";
        }  else {
            $product_weight = "";
        }

        $productExcerptPriceWeight = $short_description . $product_weight . $product->get_price_html();

    ?>

        <div class="mv-product-excerpt-weight-price"><?php echo $productExcerptPriceWeight; ?></div>
    <?php
    }
}

// Affiche le recap de la commande

if (!function_exists('mv_show_recap_checkout')) {

    function mv_show_recap_checkout()
    {

        get_template_part("parts/woocommerce-mv/mv-cart-totals");
    }
}

// Créer les blocs pour les distributeur

if (!function_exists('mv_show_shipping_item')) {

    function mv_show_shipping_item($method, $index)
    {

        get_template_part("parts/shipping-item", null, array('id' => $method->instance_id, 'label' => $method->label, 'cost' => $method->cost, 'for' => $method->id, 'tax' => $method->taxes));
    }
}


// Ajoute une div pour englober les infos txt d'un produit sur une miniature

if (!function_exists('mv_add_container_for_wc')) {

    function mv_add_container_for_wc()
    {
        echo '<div class="Container">';
    }
}


// Ajoute une div pour englober les infos txt d'un produit sur une miniature

if (!function_exists('mv_add_container_for_wc')) {

    function mv_add_container_for_wc()
    {
        echo '<div class="Container">';
    }
}
if (!function_exists('mv_close_container_for_wc')) {

    function mv_close_container_for_wc()
    {
        echo '</div>';
    }
}


// Retourne le montant minimum d'une commande pour une livraison gratuite

function get_free_shipping_minimum($zone_name = 'France')
{

    if (!isset($zone_name)) return null;

    $result = null;
    $zone = null;

    $zones = WC_Shipping_Zones::get_zones();
    foreach ($zones as $z) {
        if ($z['zone_name'] == $zone_name) {
            $zone = $z;
        }
    }

    if ($zone) {
        $shipping_methods_nl = $zone['shipping_methods'];
        $free_shipping_method = null;
        foreach ($shipping_methods_nl as $method) {
            if ($method->id == 'free_shipping') {
                $free_shipping_method = $method;
                break;
            }
        }

        if ($free_shipping_method) {
            $result = $free_shipping_method->min_amount;
        }
    }

    return $result;
}


// Affiche dans le panier latéral un texte type "Plus que xx euros pour livraison gratuite"

if (!function_exists('mv_product_title_container_before')) {

    function mv_free_shipping_cart_notice()
    {

        $min_amount = get_free_shipping_minimum();
        $current = WC()->cart->subtotal;

        if ($current < $min_amount) {
            echo '<div class="PayLeftForShipping"> <p>Plus que <span>' . wc_price($min_amount - $current) . '</span> euros pour profiter des frais de livraison offerts</p></div>';
        }
    }
}


// Ajoute une div pour englober les infos txt d'un produit sur une miniature

if (!function_exists('mv_product_title_container_before')) {

    function mv_product_title_container_before()
    {
        echo '<div class="product-infosContainer">';
    }
}

// Ajoute une div pour englober les infos txt d'un produit sur une miniature

if (!function_exists('mv_product_title_container_after')) {

    function mv_product_title_container_after()
    {
        echo '</div>';
    }
}


// Affiche le poids d'un produit sur la miniature

if (!function_exists('mv_display_product_weight')) {

    function mv_display_product_weight()
    {
        global $product;
        $productWeight = $product->get_weight();
        if ($productWeight) {
            echo '<span class="product-weight"> - ' . $productWeight . 'g</span>';
        }
    }
}


// Afficher la réassurance

if (!function_exists('mv_display_reassurance')) {

    function mv_display_reassurance()
    {
        get_template_part('parts/woocommerce-mv/reassurance');
    }
}


// Afficher les valeures de la marque (selon champs acf)

if (!function_exists('mv_display_brand_values')) {

    function mv_display_brand_values()
    {
        if(is_product()) { 

            global $product;
            $product_id = $product->get_id();

            $showTabsAndValue = get_field("tab_description_display",$product_id );

            if($showTabsAndValue) {
                get_template_part('parts/woocommerce-mv/brand-values');
            }

        }
    }
}


// Afficher le slider du catalogue

if (!function_exists('mv_display_catalog_slider')) {

    function mv_display_catalog_slider()
    {
        get_template_part('parts/woocommerce-mv/catalog-slider');
    }
}


// Ajoute un onglet ingrédient dans le tab du détail produit

function mv_ingredients_tab($tabs)
{
    // Adds the new tab
    $tabs['ingredients_tab'] = array(
        'title'     => __('Ingrédients', 'woocommerce'),
        'priority'     => 40,
        'callback'     => 'woo_ingredient_tab_content'
    );
    return $tabs;
}


// Ajoute un onglet ingrédient dans le tab du détail produit

function woo_infos_nutri_tab($tabs)
{
    // Adds the new tab
    $tabs['info_nutri_tab'] = array(
        'title'     => __('Valeurs nutritionnelles', 'woocommerce'),
        'priority'     => 50,
        'callback'     => 'woo_infos_nutri_tab_content'
    );
    return $tabs;
}


// Ajoute un onglet description dans le tab du détail produit

function mv_description_tab($tabs)
{
    // Adds the new tab
    $tabs['info_description_tab'] = array(
        'title'     => __('Description', 'woocommerce'),
        'priority'     => 30,
        'callback'     => 'mv_info_description_tab_content'
    );
    return $tabs;
}


// Charge le template pour les ingrédients d'un produit

function woo_ingredient_tab_content()
{
    get_template_part('parts/woocommerce-mv/tab-ingredients');
}


// Charge le template pour les infos nutritionnelles d'un produit

function woo_infos_nutri_tab_content()
{
    get_template_part('parts/woocommerce-mv/tab-infos-nutritionnelles');
}


// Charge le template pour les infos nutritionnelles d'un produit

function mv_info_description_tab_content()
{
    get_template_part('parts/woocommerce-mv/tab-produit-description');
}


// Supprime les onglets inutiles par défault de woocommerce dans fiche produit

function woo_remove_product_tabs($tabs)
{

    unset($tabs['description']);          // Remove the description tab
    unset($tabs['reviews']);             // Remove the reviews tab
    unset($tabs['additional_information']);      // Remove the additional information tab

    return $tabs;
}


// Masque les onglets et les valeurs de la marque selon champs ACF

function remove_product_tabs( $tabs ) {
    unset( $tabs['ingredients_tab'] );
    unset( $tabs['info_nutri_tab'] );
    unset( $tabs['info_description_tab'] );
    return $tabs;
}


// supprime les onglets de la fiche produit selon champs acf

add_action( 'template_redirect', 'template_product_whitout_tabs_and_value' );

function template_product_whitout_tabs_and_value() {

    if(is_product()) {

        $showTabsAndValue = get_field("tab_description_display");

        if(!$showTabsAndValue) {
            add_filter( 'woocommerce_product_tabs', 'remove_product_tabs', 98, 1 );
        }

    }

}



// Affiche le titre pour les produits apparentés

if (!function_exists('mv_display_title_related_products')) {
    function mv_display_title_related_products()
    {
        get_template_part('parts/woocommerce-mv/related-products-title');
    }
}


// Gestion des menus sur la page mon compte

function my_account_menu_order()
{
    $menuOrder = array(
        'edit-account'        => __('Mes informations', 'woocommerce'),
        'orders'             => __('Mes commandes', 'woocommerce'),
        'edit-address'       => __('Mes adresses', 'woocommerce'),
        'customer-logout'    => __('Déconnexion', 'woocommerce'),
    );
    return $menuOrder;
}


// Ajoute des boutons plus et moins sur la fiche produit et dans le panier

//add_action('woocommerce_before_add_to_cart_quantity', 'ts_quantity_minus_sign');
function ts_quantity_minus_sign()
{
    echo '<button type="button" data-type="remove" class="change_quantity_button minus">-</button>';
}

//add_action('woocommerce_after_add_to_cart_quantity', 'ts_quantity_plus_sign');
function ts_quantity_plus_sign()
{
    echo '<button type="button" data-type="add" class="change_quantity_button plus">+</button>';
}


// Ajoute le prix selon la quantité sur une page produit

function woocommerce_total_product_price()
{
    global $woocommerce, $product;
    // let's setup our divs
    echo sprintf('<div id="product_total_price">%s</div>', wc_price(wc_get_price_to_display($product)));
    ?>
    <script>
        jQuery(function($) {

            <?php 
                function get_data($product) {
                    $productData = (object) [];
                    $productData->attributes = $product->get_attributes();
                    $productData->image = wc_get_product_attachment_props( $product->get_image_id() );
                    $productData->unitPriceExcludingTax = wc_get_price_excluding_tax($product);
                    $productData->unitPriceIncludingTax = wc_get_price_including_tax($product);
                    $productData->displayedPrice = wc_get_price_to_display($product);
                    $productData->minQuantity = $product->get_min_purchase_quantity();
                    $productData->maxQuantity = $product->get_max_purchase_quantity();
                    return $productData;
                }
            ?>

            const baseProduct = <?php 
                echo json_encode(get_data($product));
            ?>;

            const variations = <?php 
                try {
                    $available_variations = $product->get_available_variations();

                    $variationsData = (object) [];
                    foreach ( $available_variations as $variationRaw ) {
                        $variationId = $variationRaw["variation_id"];
                        $variation = wc_get_product($variationId);
                        $variationsData->$variationId = get_data($variation);
                    }

                    echo json_encode($variationsData);
                } catch(Error $e) {
                    echo json_encode([]);
                }
            ?>;

            // Globally set the product
            let product = baseProduct;

            var htmlTemplate = <?php echo json_encode(wc_price(9888.77)); ?>;
            var $quantity = $('[name=quantity]');

            function evaluateTotalPrice() {
                var displayedPrice = product.displayedPrice;
                var unitPriceIncludingTax = product.unitPriceIncludingTax;
                var unitPriceExcludingTax = product.unitPriceExcludingTax;
                var quantity = parseFloat($quantity.val());
                var taxRatio = Math.round((unitPriceIncludingTax / unitPriceExcludingTax) * 1000) / 1000;
                var totalPrice = (unitPriceExcludingTax * quantity) * (displayedPrice === unitPriceExcludingTax ? 1 : taxRatio);
                var totalPriceStr = Number(totalPrice).toFixed(2).match(/^(\d*)(\d?\d?\d)[,.](\d+)$/);
                // Met à jour le HTML en suivant le formatage de la devise
                $('#product_total_price').html(htmlTemplate.replace(/9(\s*)888([,\.])77/, '' + totalPriceStr[1] + '$1' + totalPriceStr[2] + '$2' + totalPriceStr[3]));
            }

            $quantity.change(evaluateTotalPrice); // Actualise au changement de la valeur du champ
            evaluateTotalPrice(); // Actualise au chargement de la page

            // ajout pour les variations      
            const $variations = $('[data-type=variation-select]');
            
            //!\ It's probably not the first, but the one which attributes matches the selected attribute_name
            $variations.first().parent().parent().addClass('active');

            $variations.click(function(event) {
                event.preventDefault();

                $variations.parent().parent().removeClass('active');
                $(this).parent().parent().addClass('active');

                const id = $(this).attr('data-id');
                product = variations[id];
                // une variation peut avoir plusieurs attributs -> on sélectionne tous les select qui composent cette variation.
                $('.variations select').each(function() {
                    const $this = $(this);
                    const attributeName = $this.attr('data-attribute_name').replace(/^attribute_/, '');
                    $this.val((product.attributes || [])[attributeName]).change();
                });
                evaluateTotalPrice();

                // Update main image
                const productImage = (product && product.image && product.image.url) || (baseProduct && baseProduct.image && baseProduct.image.url);
                if(productImage)
                    $('.mv_product_image_hero > img').attr('src', productImage)
            });

            $('form .change_quantity_button').click(function() { // Gestion du + et -
                var minQuantity = product.minQuantity;
                var maxQuantity = product.maxQuantity;
                var type = $(this).attr('data-type');
                var quantity = parseFloat($quantity.val());
                var step = parseFloat($quantity.attr('step')) || 1;
                if (type === 'add') quantity += step;
                else if (type === 'remove') quantity -= step;
                quantity = Math.max(minQuantity || 0, quantity);
                if (maxQuantity >= 0) quantity = Math.min(maxQuantity, quantity);
                $quantity.val(quantity).trigger('change'); // Actualise la valeur et déclenche la fonction de mise à jour
            });
        });
    </script>
<?php
}


// Ajoute les boutons + et - dans le panier
add_action('woocommerce_before_quantity_input_field', 'ts_quantity_minus_sign');
add_action('woocommerce_after_quantity_input_field', 'ts_quantity_plus_sign');

add_action('woocommerce_cart_actions', 'add_cart_plus_minus_buttons');

function add_cart_plus_minus_buttons()
{
?>
    <script>
        jQuery(function($) {
            $('.woocommerce').on('click', 'form.woocommerce-cart-form .change_quantity_button', function() {
                var $quantity = $("input.qty", $(this).parent(".quantity"));
                var type = $(this).attr('data-type');
                var quantity = parseFloat($quantity.val());
                var step = parseFloat($quantity.attr('step')) || 1;
                if (type === 'add') quantity += step;
                else if (type === 'remove') quantity -= step;
                quantity = Math.max(0, quantity);
                $quantity.val(quantity).trigger('change'); // Actualise la valeur et déclenche la fonction de mise à jour
            });
        });
    </script>
<?php
}


// Affiche un message disponible sur une fiche produit

// The hook in function $availability is passed via the filter!
function custom_override_get_availability($availability, $_product)
{
    if ($_product->is_in_stock()) $availability['availability'] = __('Disponible', 'woocommerce');
    return $availability;
}


// Déplie le bandeau code promo automatiquement (attention le toggl est masqué en css)

function woocommerce_show_coupon()
{
    echo ' <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(\'.checkout_coupon\').show();
            });
            </script>
        ';
}


// Ajout du champ civilité sur le formulaire d'inscription    

function mv_add_register_form_field()
{
    woocommerce_form_field(
        'civilite',
        array(
            'type'        => 'radio',
            'required'    => true, // just adds an "*"
            'label'       => 'Civilité',
            'options'     => array('H' => 'Monsieur', 'F' => 'Madame')
        ),
        (isset($_POST['civilite']) ? $_POST['civilite'] : '')
    );
}


// Validation et verification du champ civilité

function mv_validate_field($username, $email, $errors)
{

    if (empty($_POST['civilite'])) {
        $errors->add('civilite_error', 'Ce champ est obligatoire');
    }
}


// Enregistrement de la valeur du champ civilité

function mv_save_register_fields($customer_id)
{

    if (isset($_POST['civilite'])) {
        update_user_meta($customer_id, 'civilite', wc_clean($_POST['civilite']));
    }
}


// Ajoute un champ un nom au formulaire d'inscription

function mv_add_name_woo_account_registration()
{
?>

    <p class="form-row form-row-first">
        <label for="reg_billing_first_name"><?php _e('First name', 'woocommerce'); ?> <span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if (!empty($_POST['billing_first_name'])) esc_attr_e($_POST['billing_first_name']); ?>" />
    </p>

    <p class="form-row form-row-first">
        <label for="reg_billing_last_name"><?php _e('Last name', 'woocommerce'); ?> <span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if (!empty($_POST['billing_last_name'])) esc_attr_e($_POST['billing_last_name']); ?>" />
    </p>

    <div class="clear"></div>

<?php
}


// Sauvegarde les champs nom et prénom du formulaire d'inscription

add_filter('woocommerce_registration_errors', 'mv_validate_name_fields', 10, 3);

function mv_validate_name_fields($errors, $username, $email)
{
    if (isset($_POST['billing_first_name']) && empty($_POST['billing_first_name'])) {
        $errors->add('billing_first_name_error', __('<strong>Error</strong>: First name is required!', 'woocommerce'));
    }
    if (isset($_POST['billing_last_name']) && empty($_POST['billing_last_name'])) {
        $errors->add('billing_last_name_error', __('<strong>Error</strong>: Last name is required!.', 'woocommerce'));
    }
    return $errors;
}


// Rend les champs noms et prénom obligatoire dans le formulaire d'inscription

function mv_save_name_fields($customer_id)
{
    if (isset($_POST['billing_first_name'])) {
        update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']));
    }
    if (isset($_POST['billing_last_name'])) {
        update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));
        update_user_meta($customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']));
    }
}


// Ajouter une checkbox pour les CGV sur le formulaire d'inscription

function mv_add_registration_privacy_policy()
{

    woocommerce_form_field('privacy_policy_reg', array(
        'type'          => 'checkbox',
        'class'         => array('form-row privacy'),
        'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
        'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
        'required'      => true,
        'label'         => '<span class="privacy-txt">J\'accepte les <a href="/cgv" target="_blank">conditions générales de vente</a></span>',
    ));
}

// Rend le champ checkbox des CGV obligatoire

function bbloomer_validate_privacy_registration($errors, $username, $email)
{
    if (!is_checkout()) {
        if (!(int) isset($_POST['privacy_policy_reg'])) {
            $errors->add('privacy_policy_reg_error', __('Privacy Policy consent is required!', 'woocommerce'));
        }
    }
    return $errors;
}


// Enleve le champ d'infos additionnels dans le checkout

function remove_order_notes($fields)
{
    unset($fields['order']['order_comments']);
    //unset($fields['billing']['billing_country']);
    return $fields;
}


// Redirection des pages de catégories (inutiles) vers la page du catalog

function wc_redirect_to_shop()
{
    // Only on product category archive pages (redirect to shop)
    if (is_product_category()) {
        wp_redirect(wc_get_page_permalink('shop'));
        exit();
    }
}


// redirection du dashboard "mon compte" vers la page orders

function misha_redirect_to_orders_from_dashboard()
{

    if (is_account_page() && empty(WC()->query->get_current_endpoint())) {
        wp_safe_redirect(wc_get_account_endpoint_url('edit-account'));
        exit;
    }
}


// changement du label pour le code promo

function bt_rename_coupon_field_on_cart($translated_text, $text, $text_domain)
{
    // bail if not modifying frontend woocommerce text
    if (is_admin() || 'woocommerce' !== $text_domain) {
        return $translated_text;
    }
    if ('Coupon:' === $text) {
        $translated_text = 'Code promo';
    }


    return $translated_text;
}


// disable le champs pays

function checkout_billing_country_field_disabled($fields)
{
    $fields['billing_country']['custom_attributes']['disabled'] = 'disabled';

    return $fields;
}


// disable le champs pays (shipping)

function checkout_shipping_country_field_disabled($fields)
{
    $fields['shipping_country']['custom_attributes']['disabled'] = 'disabled';

    return $fields;
}


// disable le champs pays

function checkout_country_hidden_fields_replacement($fields)
{
    $billing_country = WC()->customer->get_billing_country();
    $shipping_country = WC()->customer->get_shipping_country();
?>
    <input type="hidden" name="billing_country" value="<?php echo $billing_country; ?>">
    <input type="hidden" name="shipping_country" value="<?php echo $shipping_country; ?>">
<?php
}


// champ téléphone optionnel sur formulaire checkout

function ts_unrequire_wc_phone_field($fields)
{
    $fields['billing_phone']['required'] = false;
    return $fields;
}

// Changement du label pour le champ entreprise sur formulaire checkout

//add_filter( 'woocommerce_checkout_fields' , 'mv_change_label_company_form_checkout' );

function mv_change_label_company_form_checkout( $fields ) {
    unset($fields['billing']['billing_company']);
    $fields['shipping']['shipping_company']['label'] = 'Nom de l\'entreprise (pour une livraison sur votre lieu de travail)';
    return $fields;
}


// Redirection sur la homepage après logout

function redirect_after_logout($logout_url, $redirect)
{
    return $logout_url . '&amp;redirect_to=' . home_url();
}


// Change le label du bouton de paiement de la commande

function custom_order_button_text($order_button_text)
{

    $order_button_text = 'Commander et payer';

    return $order_button_text;
}

// Mise à jour dynamique du total panier dans le header
add_filter('woocommerce_add_to_cart_fragments', 'misha_add_to_cart_fragment');

function misha_add_to_cart_fragment($fragments)
{

    global $woocommerce;

    $fragments['.mini-cart-count'] = '<span class="mini-cart-count">' . $woocommerce->cart->cart_contents_count . '</span>';
    return $fragments;
}


// Affiche le prix du mode de livraison dans le recap total panier…

//add_action('woocommerce_cart_totals_before_order_total', 'mv_show_price_for_cart_in_totals', 10);
add_action('woocommerce_review_order_before_order_total', 'mv_show_price_for_cart_in_totals', 10);

function mv_show_price_for_cart_in_totals()
{

    $shipping_total = WC()->cart->get_shipping_total();
    $shipping_tax = WC()->cart->get_shipping_tax();

    $shipping_ttc = $shipping_total + $shipping_tax;

    $shipping_ttc_format = "";

    // formatage du prix pour avoir 2 décimals ou Gratuit si = 0;
    if ($shipping_total != 0) {
        $shipping_ttc_format = number_format((float)$shipping_ttc, 2, ',', '') . "€";
    } else {
        $shipping_ttc_format = "Gratuit";
    }

?>

    <tr class="mv_shipping_totals">
        <th>Frais de port TTC</th>
        <td> <?php echo $shipping_ttc_format; ?></td>
    </tr>

<?php
}


// Affiche les informations des zones de livraisons sous les modes de livraisons dans le checkout
add_action('woocommerce_cart_totals_after_shipping', 'mv_show_shipping_infos_cart', 10);
function mv_show_shipping_infos_cart()
{
?>
    <div class="mv_shipping_infos">
        <td>Livraison France métropolitaine + Corse, hors DOM-TOM
        <td>
            </tr>
        <?php
    }

    // Affiche les informations des zones de livraisons sous les modes de livraisons dans le panier
    add_action('woocommerce_review_order_after_shipping', 'mv_show_shipping_infos_checkout', 10);
    function mv_show_shipping_infos_checkout()
    {
        ?>
            <tr class="mv_shipping_infos">
                <td colspan="2">Livraison France métropolitaine + Corse, hors DOM-TOM
                <td>
            </tr>
        <?php
    }


    // Message en cas d'aucune option de livraison dispo

    function my_custom_no_shipping_message($message)
    {
        if(is_cart()) {
            if(is_user_logged_in()) {
                return __(get_template_part('parts/shipping-item-guest'));
            } else {
                return __(get_template_part('parts/shipping-item-guest'));
            }
        } elseif(is_checkout()) {
            return __('<div class="mv_shipping_message_pro woocommerce-error">
            Aucune méthode de livraison est disponible pour votre adresse.<br/>
            Les livraisons sont disponibles pour la France métropolitaine et la Corse, hors DOM-TOM </div>');
        }

    }


    // Desactive les modes de livraison dans le panier 

    add_filter( 'woocommerce_cart_needs_shipping', 'filter_cart_needs_shipping' );
    function filter_cart_needs_shipping( $needs_shipping ) {
        if ( is_cart() ) {
            $needs_shipping = false;
        }
        return $needs_shipping;
    }


    // Message d'information pour le numéro de téléphone au niveau du checkout

    function mv_show_info_for_phone_pickup()
    {
        echo '<span class="mv_billing_infos">Nous devons vous demander un numéro de téléphone valide pour les commandes en points relais Colissimo.
Votre numéro de téléphone ne sera pas utilisé pour des actions commerciales</span>';
    }


    // Affiche les horaires d'ouverture (utilisé dans l'email local pickup)

    function mv_display_open_hours()
    {

        $string_open_hours_from_acf = get_field('options_info_mag_hours', 'options');

        echo $string_open_hours_from_acf;
    }


    // Dans le panier, ajouter le message pour les commandes types CE

    function mv_pro_order_message() {

        echo '<div class="mv_shipping_message_pro">Pour toute commande professionnelle (CE, association...), 
        nous vous invitons à prendre contact avec le gestionnaire de la boutique à <a href="mailto:eboutique@armor-delices.com">eboutique@armor-delices.com</a> 
        qui reviendra vers vous sous 24h ouvrées</div>';

    }

    // Paramètres du flexslider woocommerce sur la page détail produit

    function customslug_single_product_carousel_options( $options ) {

        $options['animation'] = 'fade';
        $options['animationSpeed'] = 400;
        $options['slideshowSpeed'] = 2000;
        $options['slideshow'] = true;
        $options['animationLoop'] = true;
        return $options;

    }

    // masque le caption sur la visionneuse d'image des photos produits

    add_filter( 'woocommerce_single_product_photoswipe_options', function( $options ) {
        // Disable caption element
        $options['captionEl'] = false;
        // Other options
        // $options['shareEl'] = true;
        // $options['counterEl'] = false;
        // $options['arrowEl'] = false;
        // $options['preloaderEl'] = true; // For browsers that do not support CSS animations
        // $options['closeOnScroll'] = false; // Already defaults to false
        // $options['clickToCloseNonZoomable'] = false;
        // $options['closeOnVerticalDrag'] = false;
        // $options['maxSpreadZoom'] = 1;
        // $options['hideAnimationDuration'] = 500;
        // $options['showAnimationDuration'] = 500;
        // $options['barsSize'] = array("top" => 0, "bottom" => "auto");
        return $options;
      } );


      // Ajoute un message après l'historique de commande d'un client

      function mv_add_message_order_history() {
          get_template_part('parts/woocommerce-mv/order-history-message');
      }


      // Modifie les labels et ajoute le montant de la tva pour la livraison sur détail commande

      function renaming_shipping_order_item_totals( $total_rows, $order, $tax_display ){

            if(is_account_page()) {

                $shipping_vat = $order->get_shipping_tax();
                $shipping_vat_format = str_replace('.',',',$shipping_vat) . '€';
    
                $shipping_value = $total_rows['shipping']['value'];
    
                $shipping_with_vat = "";
    
                // si tva = 0 alors la livraison est gratuite donc on ne change pas la string
                if($shipping_vat != 0) {
                    $shipping_with_vat = $shipping_value . '<small class="mv_shipping_vat"> (dont TVA 20% : ' . $shipping_vat_format . ')</small>';
                } else {
                    $shipping_with_vat = $shipping_value;
                }
    
                $total_rows['cart_subtotal']['label'] = __('Sous-total TTC', 'woocommerce');
                $total_rows['shipping']['label'] = __('Livraison TTC', 'woocommerce');
                $total_rows['shipping']['value'] = $shipping_with_vat;
                $total_rows['order_total']['label'] = __('Total TTC', 'woocommerce');

            }

          return $total_rows;
      }


    // affiche les informations du vendeur, utilisé sur les factures et note de remboursement

    function show_shop_infos_on_invoice() {
        ?>
      <div class="shop-name"><h3>GOÛTERS MAGIQUES</h3></div>
      <div class="shop-infos">SAS au capital de 25 549 180 €</div>
      <div class="shop-address">
          <div class="shop-address-city">Lieu-dit Kerichelard – ZA de Keranna</div>
          <div class="shop-address-street">56500 PLUMELIN</div>
      </div>
      <div class="shop-siren">Siren : 499 185 783</div>
      <div class="shop-tva">TVA intracommunautaire : FR66499185783</div>
      <?php 
    }

    
    // modifie le label des status de commande
    add_filter( 'wc_order_statuses', 'ts_rename_order_status_msg', 20, 1 );

      function ts_rename_order_status_msg( $order_statuses ) {
          $order_statuses['wc-on-hold']  = _x( 'Commande en attente', 'Order status', 'woocommerce' );
          $order_statuses['wc-ready-pickup']  = _x( 'Prêt pour retrait boutique', 'Order status', 'woocommerce' );
          $order_statuses['wc-pickup'] = _x( 'Retirée en boutique', 'Order status', 'woocommerce' );
          return $order_statuses;
      }


    // modifie le label des status dans la page commande (filtre au dessus de la liste)

    foreach( array( 'post', 'shop_order' ) as $hook )
        add_filter( "views_edit-shop_order", 'ts_order_status_top_changed' );
        function ts_order_status_top_changed( $views ){
            if( isset ($views['wc-on-hold'] ) ) 
                $views['wc-on-hold'] = str_replace( 'En attente', __( 'Commande en attente', 'woocommerce'), $views['wc-on-hold'] );
            if( isset( $views['wc-ready-pickup'] ) )
                $views['wc-ready-pickup'] = str_replace( 'Prêt pour le ramassage', __( 'Prêt pour retrait boutique', 'woocommerce'), $views['wc-ready-pickup'] );
            if( isset( $views['wc-pickup'] ) )
                $views['wc-pickup'] = str_replace( 'Ramassé', __( 'Retirée en boutique', 'woocommerce'), $views['wc-pickup'] );                
                return $views;
        }


    // modifie le label des status dans le menu déroulant des actions sur liste de commande
    add_filter( 'bulk_actions-edit-shop_order', 'ts_bulk_actions_order_status', 100, 1 );

      function ts_bulk_actions_order_status( $actions ) {
          $actions['mark_ready-pickup'] = __( 'Marquer comme prêt pour retrait boutique', 'woocommerce' );
          $actions['mark_pickup'] = __( 'Marquer comme retirée en boutique', 'woocommerce' );
          return $actions;
      }


    // Ajoute l'image à la une du produit au dessus du caroussel photo sur détail produit 

    add_action('woocommerce_before_single_product_summary', 'mv_add_first_image_alone_on_product_detail', 15, 1); 

    function mv_add_first_image_alone_on_product_detail() {

        global $product;
        $id = $product->get_id();
        $name = $product->get_name();

        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );

        ?>
        <div class="mv_product_image_hero">            
            <?php get_template_part('parts/woocommerce-mv/ribbon-special-offer'); ?>
            <?php get_template_part('parts/woocommerce-mv/ribbon-new'); ?>
            <img src="<?php echo $image[0]; ?>" alt="Photo de <?php echo $name; ?>">
        </div>
        <?php
    }


    // Enleve l'image à la une du carrousel sur détail produit

    add_filter('woocommerce_single_product_image_thumbnail_html', 'remove_featured_image', 10, 2);
    function remove_featured_image($html, $attachment_id ) {
        global $post, $product;
    
        $featured_image = get_post_thumbnail_id( $post->ID );
    
        if ( $attachment_id == $featured_image )
            $html = '';
    
        return $html;
    }

    
    // reset le choix du mode de livraison du client au moment du checkout

    add_filter( 'woocommerce_shipping_chosen_method', '__return_false', 99);
    add_action( 'template_redirect', 'reset_previous_chosen_shipping_method' );
    
    function reset_previous_chosen_shipping_method() {

        if( is_checkout() && ! is_wc_endpoint_url() && is_user_logged_in() 
        && get_user_meta( get_current_user_id(), 'shipping_method', true ) ) {
            delete_user_meta( get_current_user_id(), 'shipping_method' );
            WC()->session->__unset( 'chosen_shipping_methods' );
        }

    }


    // Modifie le tag H des produits sur la page d'accueil pour SEO

    remove_action( 'woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title', 10 );
    add_action('woocommerce_shop_loop_item_title', 'soChangeProductsTitle', 10 );

    function soChangeProductsTitle() {
        if(is_front_page()) {
            echo '<h3 class="' . esc_attr( apply_filters( 'woocommerce_product_loop_title_classes', 'woocommerce-loop-product__title' ) ) . '">' . get_the_title() . '</h3>';
        } else {
            echo '<h2 class="' . esc_attr( apply_filters( 'woocommerce_product_loop_title_classes', 'woocommerce-loop-product__title' ) ) . '">' . get_the_title() . '</h2>';
        }
    }



    // génere une facture lors du passage d'une commande en "Colissimo en transit"
    add_action( 'woocommerce_order_status_changed', 'action_woocommerce_process_shop_order_meta', 99, 1 );

    function action_woocommerce_process_shop_order_meta( $order_id ) { 

        $order = wc_get_order( $order_id );        

        if ($order !== false) {

            $order_status = $order->get_status();
            $invoice = wcpdf_get_document( 'invoice', $order );
            
            if( $order_status == "lpc_transit" && !$invoice->exists() ) {
                $generatedInvoice = wcpdf_get_document( 'invoice', $order, true );
            }

        }

    }; 
    
    

    add_action('woocommerce_before_account_orders', 'show_message_order_waiting_for_payment_on_history', 99 );

    function show_message_order_waiting_for_payment_on_history( $has_orders ) {

        if($has_orders) {

            $order_waiting_url = get_user_order_waiting_for_payment_url();

            // verif si le client à une commande en attente de paiement
            if($order_waiting_url != "") {

                $current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $history_url = wc_get_account_endpoint_url( 'orders' );
            
                // verif si on est sur la page historique de commande
                if( $current_url == $history_url ) {

                    $args = array (
                        'order_url' => $order_waiting_url,
                    );

                    get_template_part("parts/message-order-waiting-payment", null, $args);
                    

                }

            }
        }

    }


    // retourne l'url de détail de la dernière commande en attente de paiement pour un client

    function get_user_order_waiting_for_payment_url() {

        $order_waiting_payment = wc_get_orders(array(
            'limit'=>1,
            'customer' => get_current_user_id(),
            'type'=> 'shop_order',
            'status'=> array( 'wc-pending' )
            )
        );

        $view_order_url = "";

        if(!empty($order_waiting_payment)) {
            //$view_order_url = ($order_waiting_payment[0]->get_view_order_url());            
            $view_order_url = ($order_waiting_payment[0]->get_checkout_payment_url());
        }

        return $view_order_url;

    }


    // Affiche le message d'aide en cas d'erreur de paiement

    add_action('before_woocommerce_pay', 'show_message_contact_if_error_payment');

    function show_message_contact_if_error_payment() {

        get_template_part('parts/message-order-trouble-payment');
        
    }
    

    // Ajoute un container sur le formulaire de login et un titre

    add_action('woocommerce_login_form_start', 'checkout_login_form_container_open');

    function checkout_login_form_container_open() {

        echo '<div class="mv-woocommerce-form-login-container"><span class="mv-woocommerce-form-login-title">Déjà client, je m’identifie</span>';

    }


    // Fermeture du container sur formulaire de connexion

    add_action('woocommerce_login_form_end', 'checkout_login_form_container_close');

    function checkout_login_form_container_close() {

        echo '</div>';

    }

    // Affiche le bloc création de compte après le formulaire de login

    add_action('woocommerce_login_form_end', 'show_create_account_checkout', 20);

    function show_create_account_checkout() {
        
        get_template_part('parts/woocommerce-mv/create-account-checkout');

    }


    // Charge le template custom pour le selection de variation de produit

    add_action('woocommerce_before_variations_form', 'show_variation_custom_mv');

    function show_variation_custom_mv() {

        get_template_part('parts/woocommerce-mv/variations');

    }



    // Affiche le bandeau offre spéciale si activé

    add_action('woocommerce_before_shop_loop_item', 'show_ribbon_special_offer');

    function show_ribbon_special_offer() {
        
         get_template_part('parts/woocommerce-mv/ribbon-special-offer');

    }

    // Affiche le bandeau offre spéciale si activé

    add_action('woocommerce_before_shop_loop_item', 'show_ribbon_new');

    function show_ribbon_new() {
        
         get_template_part('parts/woocommerce-mv/ribbon-new');

    }


    // Affiche un bandeau promo à un meilleur endroit que par défaut dans wc
        

    add_action('woocommerce_single_product_summary', 'show_sale_tag_mv', 0);

    add_action('woocommerce_shop_loop_item_title',  'show_sale_tag_mv', 11);

    function show_sale_tag_mv() {
        
        global $product;

        if ( $product->is_on_sale() )  {  
            echo '<span class="sale-mv">promo</span>';
        }
    }
    

    // Affiche le prix unitaire en version html dans page panier pour prix barrés

    add_filter('woocommerce_cart_item_price', 'price_html_in_cart', 10, 3); 

    function price_html_in_cart( $price, $cart_item, $cart_item_key ) {

        $price_html = "";

        // on verifie si il s'agit d'une déclinaison
        if( $cart_item['variation_id'] != 0 ) {
            $product = wc_get_product( $cart_item['variation_id'] );
            $price_html = $product->get_price_html();
        } else {
            $product = wc_get_product( $cart_item['product_id'] );
            $price_html = $product->get_price_html();
        }

        return $price_html;

    }


    // Dans les factures, affiche le prix unitaire HT barré en cas de promo sur un produit

    add_action('mv_unit_price_invoice', 'strike_unit_price_in_invoice_for_discount', 10, 3); 

    function strike_unit_price_in_invoice_for_discount( $order, $item_id, $item ) {

        $product_id = 0;
        $ex_product_price_from_catalog = (float)0;

        
        // s'agit-il d'une variation ?
        if( $item['variation_id'] != 0 ) {
            $product_id = $item['variation_id'];
        } else {
            $product_id = $item['product_id'];
        }


        if( wc_get_product( $product_id ) ) {
            // on recupere le produit dans le catalogue
            $product = wc_get_product( $product_id );

            // on recupere le prix unitaire du produit avant promo et HT
            $ex_product_price_from_catalog = (float) wc_get_price_excluding_tax( $product, array('price'=>$product->get_regular_price()) );
            $ex_product_price_from_catalog_display = number_format((float)$ex_product_price_from_catalog, 2, ',', '');
            $ex_product_price_from_catalog = number_format((float)$ex_product_price_from_catalog, 2, '.', '');
        }


        // on recupere le prix unitaire du produit dans la commande(déjà formatté ex: 3,00€) pour le comparer
        $ex_item_single_price = $item['ex_single_price'];           
        // on remplace les , par des .
        $ex_item_single_price = str_replace(',', '.', $ex_item_single_price);                
        // on supprime les symboles ni chiffre ni .
        $ex_item_single_price = preg_replace("/[^0-9\.]/", "", $ex_item_single_price);    


        // on compare les 2 valeurs float au format 3.00
        if( (float)$ex_product_price_from_catalog > (float)$ex_item_single_price ) {
            // si le prix du catalogue est supérieur au prix d'achat dans la commande il y a une promo on affiche le prix barré
            echo '<del>' . $ex_product_price_from_catalog_display . '€</del> <br/> <span>' . $item['ex_single_price'] . '</span>';
        } else {
            // sinon affiche le prix normal
            echo $item['ex_single_price'];
        }

    }


    // Modifie le nombre de produit affichés par page

    add_filter( 'loop_shop_per_page', 'loop_shop_per_page', 30 );

    function loop_shop_per_page( $products ) {

     $products = 999;
     return $products;

    }


    // Ajoute une icone custom pour le mode de paiement Stripe

    add_filter( 'woocommerce_gateway_icon', 'stripe_icon_payement', 10, 2 );

    function stripe_icon_payement ( $icon, $id ) {
        if ( $id === 'stripe' ) {
            return '<img src="' . get_template_directory_uri() . '/img/icons/CB.svg">';
        } else {
            return $icon;
        }
    }