<?php

/*
 * Template Name: Template Contact : Magasin
 */

$subtitlePage = get_field("subtitle_page");

?>

<?php get_header(); ?>

	<div id="primary" class="content-area">

		<main role="main" id="Main" class="TemplateContact">

			<!-- Container -->
			<div class="Container">

				<!-- section -->
				<section>

                    <div class="PageTitle">
                        <h1 class="page-title"><?php the_title();?></h1>
                        <?php if($subtitlePage) : ?>
                            <p class="PageTitle-txt"><?php echo $subtitlePage; ?></p>
                        <?php endif; ?>
                     </div>

					<!-- contenu -->
					<section id="post-<?php the_ID(); ?>" class="PageContent">

							<div class="Container">

                                <div class="Row Row--center">

                                    <div class="Col Col-10 Col-md-12">

                                        <?php echo do_shortcode('[contact-form-7 id="126" title="Formulaire contact : magasin"]'); ?>

                                    </div>

                                </div>

                            </div>

					</section>
					<!-- /contenu -->


				</section>
				<!-- /section -->

			</div>
			<!-- /Container -->

		</main>

	</div>

<?php get_footer(); ?>

