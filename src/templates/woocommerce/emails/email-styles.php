<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load colors.
$bg        = get_option( 'woocommerce_email_background_color' );
$body      = get_option( 'woocommerce_email_body_background_color' );
$base      = get_option( 'woocommerce_email_base_color' );
$base_text = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text      = get_option( 'woocommerce_email_text_color' );

// Pick a contrasting color for links.
$link_color = wc_hex_is_light( $base ) ? $base : $base_text;

if ( wc_hex_is_light( $body ) ) {
	$link_color = wc_hex_is_light( $base ) ? $base_text : $base;
}

$bg_darker_10    = wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );
$text_lighter_40 = wc_hex_lighter( $text, 40 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
// body{padding: 0;} ensures proper scale/positioning of the email in the iOS native email app.
?>
body {
	padding: 0;
}

#wrapper {
	background-color: <?php echo esc_attr( $bg ); ?>;
	background-image: url("<?php echo get_template_directory_uri() . "/img/email"; ?>/bg_toile_email.jpg");
	margin: 0;
	padding: 70px 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
}

#template_header {
	background-color: transparent;
	border-radius: 3px 3px 0 0 !important;
	color: <?php echo esc_attr( $base_text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_header h1,
#template_header h1 a {
	color: <?php echo esc_attr( $base ); ?>;
	background-color: inherit;
	font-weight: bold;
	text-align: center;
}

#template_header_image img {
	margin-left: 0;
	margin-right: 0;
}

#template_footer td {
	padding: 0;
	border-radius: 6px;
}

#template_footer #credit {
	border: 0;
	color: <?php echo esc_attr( $base ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 14px;
	font-weight: bold;
	line-height: 150%;
	text-align: center;
	padding: 8px 0;
}

#template_footer #credit p {
	margin: 0 0 16px;
}

#template_body {
	background-color: #FFFFFF;
}

#body_content {
	background-color: transparent;
}

#body_content table td {
	padding: 48px 48px 0;
}

#body_content table td td {
	padding: 20px;
}

#body_content table td th {
	padding: 20px;
}

#body_content td ul.wc-item-meta {
	font-size: small;
	margin: 1em 0 0;
	padding: 0;
	list-style: none;
}

#body_content td ul.wc-item-meta li {
	margin: 0.5em 0 0;
	padding: 0;
}

#body_content td ul.wc-item-meta li p {
	margin: 0;
}

#body_content p {
	margin: 0 0 30px;
}

#body_content_inner {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 16px;
	line-height: 150%;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

.td {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	border: 1px solid <?php echo esc_attr( $body_darker_10 ); ?>;
	vertical-align: middle;
}

.address {
	font-size: 14px;
	font-style: normal;
	line-height: 20px;
	padding: 0;
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
}

.button {
	background-color: <?php echo esc_attr( $base ); ?>;
	border-radius: 50px;
	color: #FFFFFF;
	font-size: 12px;
	font-weight: bold;
	padding: 14px 30px 14px 30px;
	text-decoration: none;
	text-transform: uppercase;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

.link {
	color: <?php echo esc_attr( $link_color ); ?>;
}

#header_wrapper {
	padding: 0 48px 36px;
	display: block;
}

h1 {
	color: <?php echo esc_attr( $base ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 30px;
	font-weight: 300;
	line-height: 150%;
	margin: 0;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h2 {
	color: <?php echo esc_attr( $base ); ?>;
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 18px;
	font-weight: bold;
	line-height: 130%;
	margin: 0 0 18px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h3 {
	color: <?php echo esc_attr( $base ); ?>;
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 16px;
	font-weight: bold;
	line-height: 130%;
	margin: 16px 0 8px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

a {
	color: <?php echo esc_attr( $link_color ); ?>;
	font-weight: normal;
	text-decoration: underline;
}

img {
	border: none;
	display: inline-block;
	font-size: 14px;
	font-weight: bold;
	height: auto;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
	vertical-align: middle;
	margin-<?php echo is_rtl() ? 'left' : 'right'; ?>: 10px;
	max-width: 100%;
	height: auto;
}


/* Custom MV */


#body_content #abandonnedCart tr td {
	padding: 0 !important;
}

#body_content #abandonnedCart table tr td {
	padding: 0 !important;
}

#body_content #abandonnedCart table tr.abandonnedCartLine-name td {
	padding: 10px !important;
}

#body_content #abandonnedCart #abandonnedCart-total td {
	border-bottom:1px solid #DDDDDD;
	border-top:1px solid #DDDDDD;
	padding-bottom: 20px;
	padding-top: 20px;
}


#body_content #abandonnedCart #abandonnedCart-total td h4 {
	margin: 0 !important;
}


#order-mv {
	border: 0 !important;
}

#order-mv thead tr th {
	text-align: right !important;
}

#order-mv thead tr th.mv-thead-product-title {
	text-align: left !important;
}

#order-mv .td {
	border: 0 !important;
	padding-left: 0;
}

#order-mv tbody tr.order_item td.mv-order-item-name {
	border-bottom: 1px solid #DDDDDD !important;
	color: <?php echo esc_attr( $base ); ?>;
	font-size: 13px;
}

#order-mv tbody tr.order_item td.mv-order-item-qty {
	border-bottom: 1px solid #DDDDDD !important;
	text-align: right !important;
}

#order-mv tbody tr.order_item td.mv-order-item-price {
	border-bottom: 1px solid #DDDDDD !important;
	text-align: right !important;
}

#order-mv tfoot tr td {
	text-align: right !important;
}

#order-mv tfoot tr td .woocommerce-Price-amount {
	font-weight: bold;
}

#footer-link {
	text-align: center;
}

#footer-link a {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 12px;
}

/* Advanced local pickup */

.local_pickup_email_title {
	margin-bottom: 30px !important;
}

.wclp_mail_address {
	margin: 30px 0 30px 0 !important;
}

.wclp_location_box_content {
	color: <?php echo esc_attr( $text ); ?>;
	font-size: 13px;
	line-height: 20px;
	margin: 0 !important;
}
<?php
