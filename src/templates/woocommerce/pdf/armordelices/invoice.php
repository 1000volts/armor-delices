<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<table class="head container">
	<tr>
		<td class="header">
		<?php
		if( $this->has_header_logo() ) {
			$this->header_logo();
		} else {
			echo $this->get_title();
		}
		?>
		</td>
		<td class="shop-info">
			<?php do_action( 'mv_shop_info_invoice' ); ?>
			<?php /*
			<?php do_action( 'wpo_wcpdf_before_shop_name', $this->type, $this->order ); ?>
			<div class="shop-name"><h3>GOÛTERS MAGIQUES</h3></div>
			<div class="shop-infos">SAS au capital de 25 549 180 €</div>
			<?php do_action( 'wpo_wcpdf_after_shop_name', $this->type, $this->order ); ?>
			<?php do_action( 'wpo_wcpdf_before_shop_address', $this->type, $this->order ); ?>
			<div class="shop-address">
				<div class="shop-address-city">Lieu-dit Kerichelard – ZA de Keranna</div>
				<div class="shop-address-street">56500 PLUMELIN</div>
			</div>
			<?php do_action( 'wpo_wcpdf_after_shop_address', $this->type, $this->order ); ?>
			<div class="shop-siren">Siren : 499 185 783</div>
			<div class="shop-tva">TVA intracommunautaire : FR66499185783</div>
			*/ ?>
		</td>
	</tr>
</table>

<h1 class="document-type-label">
<?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
</h1>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

<table class="order-data-addresses">
	<tr>
		<td class="address billing-address">
			<!-- <h3><?php _e( 'Billing Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3> -->
			<?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
			<?php $this->billing_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>
			<?php if ( isset($this->settings['display_email']) ) { ?>
			<div class="billing-email"><?php $this->billing_email(); ?></div>
			<?php } ?>
			<?php if ( isset($this->settings['display_phone']) ) { ?>
			<div class="billing-phone"><?php $this->billing_phone(); ?></div>
			<?php } ?>
		</td>
		<td class="address shipping-address">
			<?php if ( !empty($this->settings['display_shipping_address']) && ( $this->ships_to_different_address() || $this->settings['display_shipping_address'] == 'always' ) ) { ?>
			<h3><?php _e( 'Ship To:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
			<?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
			<?php $this->shipping_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?>
			<?php } ?>
		</td>
		<td class="order-data">
			<table>
				<?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
				<?php if ( isset($this->settings['display_number']) ) { ?>
				<tr class="invoice-number">
					<th><?php echo $this->get_number_title(); ?></th>
					<td><?php $this->invoice_number(); ?></td>
				</tr>
				<?php } ?>
				<?php if ( isset($this->settings['display_date']) ) { ?>
				<tr class="invoice-date">
					<th><?php echo $this->get_date_title(); ?></th>
					<td><?php $this->invoice_date(); ?></td>
				</tr>
				<?php } ?>
				<tr class="order-number">
					<th><?php _e( 'Order Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->order_number(); ?></td>
				</tr>
				<tr class="order-date">
					<th><?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->order_date(); ?></td>
				</tr>
				<?php // ajout mv ?>
				<tr class="invoice-date">
					<th>Date d’échéance :</th>
					<td><?php $this->invoice_date(); ?></td>
				</tr>
				<?php // fin ajout mv ?>
				<tr class="payment-method">
					<th><?php _e( 'Payment Method:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->payment_method(); ?></td>
				</tr>
				<?php if( $payment_date = $this->order->get_date_paid() ) : ?>
    				<tr class="payment-date">
    					<th>Date de paiement :</th>
    					<td><?php echo $payment_date->format ('Y-m-d'); ?></td>
    				</tr>
					<?php endif; ?>
				<?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
			</table>			
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>



<table class="order-details">
	<thead>
		<tr>
			<th class="product"><?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="quantity"><?php _e('Quantity', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="unit_price_ht"><?php _e('Prix unitaire HT', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="tax_rate"><?php _e('Taux TVA', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="tax_value"><?php _e('Total TVA', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="price"><?php _e('Prix TTC Total', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
		<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', 'item-'.$item_id, $this->type, $this->order, $item_id ); ?>">
			<td class="product">
				<?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
				<span class="item-name"><?php echo $item['name']; ?></span>
				<?php do_action( 'wpo_wcpdf_before_item_meta', $this->type, $item, $this->order  ); ?>
				<span class="item-meta"><?php echo $item['meta']; ?></span>
				<dl class="meta">
					<?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
					<?php if( !empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo $item['sku']; ?></dd><?php endif; ?>
					<?php if( !empty( $item['weight'] ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
				</dl>
				<?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
			</td>
			<td class="quantity"><?php echo $item['quantity']; ?></td>
			<td class="unit_price_ht">
				<?php do_action( 'mv_unit_price_invoice', $this->order, $item_id, $item ); ?>
				<?php //echo $item['ex_single_price']; /*modif mv pour afficher sous total sans la promo*/ ?>
			</td>
			<td class="tax_rate"><?php echo $item['tax_rates']; ?></td>
			<td class="tax_value"><?php echo $item['line_subtotal_tax']; ?></td>
			<td class="price"><?php echo $item['order_price']; ?></td>
		</tr>
		<?php endforeach; endif; ?>
	</tbody>
	<tfoot>
		<tr class="no-borders">
			<td class="no-borders" colspan="2">
				<div class="document-notes">
					<?php do_action( 'wpo_wcpdf_before_document_notes', $this->type, $this->order ); ?>
					<?php if ( $this->get_document_notes() ) : ?>
						<h3><?php _e( 'Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
						<?php $this->document_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_document_notes', $this->type, $this->order ); ?>
				</div>
				<div class="customer-notes">
					<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
					<?php if ( $this->get_shipping_notes() ) : ?>
						<h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
						<?php $this->shipping_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>
				</div>		
				<?php /* AJOUT MV */ ?>
				<div>
					<?php if ( $this->get_footer() ): ?>
						<div id="footer">
							<!-- hook available: wpo_wcpdf_before_footer -->
							<?php $this->footer(); ?>
							<!-- hook available: wpo_wcpdf_after_footer -->
						</div><!-- #letter-footer -->
					<?php endif; ?>					
				</div>		
				<?php /* FIN AJOUT MV */ ?>
			</td>
			<td class="no-borders" colspan="4">
				<table class="totals">
					<tfoot>
						<?php foreach( $this->get_woocommerce_totals() as $key => $total ) : ?>
							
							<?php /* AJOUT MV */ ?>
							<?php 						
								if($key == "discount") { 
									$order_discount_without_vat = $this->get_order_discount($tax = 'excl');
									$total['label'] = "Remise HT";
									$total['value'] = '- ' . $order_discount_without_vat['value'];
								}
								if($key == "cart_subtotal") { 
									$order_subtotal_without_vat = $this->get_order_subtotal($tax = 'excl');
									$total['label'] = "Sous-total HT";
									$total['value'] = $order_subtotal_without_vat['value'];
								}
								if($key == "shipping") { 
									$order_shipping_without_vat = $this->order->get_shipping_to_display('excl');
									$total['label'] = "Livraison HT (TVA 20%)";			
									$total['value'] = $order_shipping_without_vat;
								}
								if($key == "order_total") { 
									// ajout total tva
									$order_tax_total = $this->get_order_taxes();
									foreach($order_tax_total as $key=>$tax) {
										?>
										<tr class="tva">
											<th class="description">Total TVA</th>
											<td class="price"><?php echo $tax['value']; ?></td>
										</tr>
										<?php
									}
									// total commande
									$order_total_with_vat = $this->get_order_grand_total();
									$total['label'] = "Total TTC"; 
									$total['value'] = $order_total_with_vat['value'];								
								}
							?>
							<?php /* fin AJOUT MV */ ?>

						<tr class="<?php echo $key; ?>">
							<th class="description"><?php echo $total['label']; ?></th>
							<td class="price"><span class="totals-price"><?php echo $total['value']; ?></span></td>
						</tr>
						<?php endforeach; ?>
					</tfoot>
				</table>
			</td>
		</tr>
	</tfoot>
</table>

<div class="bottom-spacer"></div>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>


<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>
