<?php

/*
 * Template Name: Template des recettes
 */

?>


<?php get_header(); ?>

	<main id="Main" role="main" class="TemplateRecettesList">


		<!-- section -->
		<section>
            
            <div class="Container">
                
                <div class="PageTitle">
                    <h1 class="page-title"><?php the_title();?></h1>
                </div>    

                <?php 

                    // query de la dernière recette pour la mettre à a la une

                    $args_query_last_recette = array(
                        'post_type' => 'postrecette',
                        'posts_per_page' => 1,
                    );

                    $query_last_recette = new WP_Query( $args_query_last_recette );

                ?>

                <?php if ($query_last_recette->have_posts()): // affichage recette à la une ?>

                    <?php while ($query_last_recette->have_posts()) : $query_last_recette->the_post(); ?>

                        <?php get_template_part('parts/recette-details', null, array( 'show_header' => true )); ?>

                    <?php endwhile; ?> 

                <?php endif; ?> 


                <?php

                    // query des recettes avec un offset de 1 pour eviter la recette à la une

                    $current_page = get_query_var('paged');
                    $current_page = max( 1, $current_page );
                    
                    $per_page = 6;
                    $offset_start = 1;
                    $offset = ( $current_page - 1 ) * $per_page + $offset_start;
                    
                    $args_query_all_recette = array(
                        'post_type' => 'postrecette',
                        'posts_per_page' => $per_page,
                        'paged' => $current_page,
                        'offset' => $offset
                    );

                    $query_recettes = new WP_Query( $args_query_all_recette );

                    $total_rows = max( 0, $query_recettes->found_posts - $offset_start );
                    $total_pages = ceil( $total_rows / $per_page );
                    
                ?>

                <?php if ($query_recettes->have_posts()): // affichage liste recettes ?>
                
                    <section class="RecettesList">

                        <div class="Row">

                            <span class="RecettesList-hashtag Hashtag Hashtag--plusdebeurre">#plusdeBeurrequedeMal</span>

                            <?php $transition_delay_recette = 100; ?>

                            <?php while ($query_recettes->have_posts()) : $query_recettes->the_post(); ?>
                                <div class="RecettesList-item Col Col-4 Col-sm-6 Col-xs-12" data-aos="fade-up" data-aos-delay="<?php echo $transition_delay_recette; ?>">
                                    <?php get_template_part('parts/recette-item'); ?>
                                </div>
                                <?php $transition_delay_recette+=100; ?>
                            <?php endwhile; ?>                        

                        </div>

                        <div class="RecettesList-pagination Pagination">
                            <?php echo paginate_links( array( 'total'   => $total_pages, 'current' => $current_page, ) ); ?>
                        </div>

                    </section>                

                    <?php wp_reset_postdata(); ?>                
                
                <?php endif; ?>     

            </div>

		</section>
		<!-- /section -->

	</main>

<?php get_footer(); ?>
