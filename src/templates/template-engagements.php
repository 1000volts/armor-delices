<?php

/*
 * Template Name: Template nos engagements
 */

$subtitlePage = get_field("subtitle_page");

$engagementSection1Title = get_field("engagements_section_1_title");
$engagementSection1Txt = get_field("engagements_section_1_txt");

$engagementSection2Title = get_field("engagements_section_2_title");
$engagementSection2Txt = get_field("engagements_section_2_txt");

$engagementSection3Grp1 = get_field("engagements_section_3_paragraphe_1");
$engagementSection3Grp2 = get_field("engagements_section_3_paragraphe_2");
$engagementSection3Grp3 = get_field("engagements_section_3_paragraphe_3");

?>


<?php get_header(); ?>

	<main role="main" class="TemplateEngagements" id="Main">


		<!-- section -->
		<section>

            <div class="PageTitle">
                <h1 class="page-title"><?php the_title();?></h1>
                <?php if($subtitlePage) : ?>
                    <p class="PageTitle-txt"><?php echo $subtitlePage; ?></p>
                <?php endif; ?>
            </div>

            <div class="TemplateEngagements-section1">
                <div class="Container">
                    <div class="Row">
                        <div class="Col Col-6 Col-sm-12">
                            <div class="TemplateEngagements-content TemplateEngagements-content--section1 Gutenberg"  data-aos="fade-right">

                                <?php if($engagementSection1Title) : ?>
                                    <h2><?php echo($engagementSection1Title); ?></h2>
                                <?php endif; ?>

                                <?php if($engagementSection1Txt) : ?>
                                    <?php echo($engagementSection1Txt); ?>
                                <?php endif; ?>

                                <img src="<?php echo get_template_directory_uri(); ?>/img/engagements/engagements_illus.png" 
                                 srcset="<?php echo get_template_directory_uri(); ?>/img/engagements/engagements_illus@2x.png" alt="">

                            </div>
                        </div>
                        <div class="Col Col-6 Col-sm-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/engagements/engagements_montage_1.png" 
                                 srcset="<?php echo get_template_directory_uri(); ?>/img/engagements/engagements_montage_1@2x.png" alt="" data-aos="fade-left">
                        </div>
                    </div>
                </div>
            </div>

            <div class="TemplateEngagements-section2 FullWidthBlueBG">
                <div class="Container">
                    <div class="Row">
                        <div class="Col Col-6 Col-sm-12">
                            <div class="TemplateEngagements-section2-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/engagements/engagements_polaroid.png" 
                                 srcset="<?php echo get_template_directory_uri(); ?>/img/engagements/engagements_polaroid@2x.png" alt="" data-aos="fade-right">
                            </div>
                        </div>
                        <div class="Col Col-6 Col-sm-12">
                            <div class="TemplateEngagements-content TemplateEngagements-content--section2 Gutenberg" data-aos="fade-left">

                                <?php if($engagementSection2Title) : ?>
                                    <h2><?php echo($engagementSection2Title); ?></h2>
                                <?php endif; ?>

                                <?php if($engagementSection2Txt) : ?>
                                    <?php echo($engagementSection2Txt); ?>
                                <?php endif; ?>

                                <div class="TemplateEngagements-content--section2--rse">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/engagements/engagements_rse@2x.jpg"  alt="Logo engagé RSE Modèle AFAQ 26000 - Certification AFNOR">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="TemplateEngagements-section3">
                <div class="Container">
                    <div class="Row">
                        <div class="Col Col-6 Col-sm-12">
                            <div class="TemplateEngagements-content  TemplateEngagements-content--section3 Gutenberg" data-aos="fade-right">

                                <div class="TemplateEngagements-territoire-1">
                                    <?php if($engagementSection3Grp1) : ?>

                                        <?php if($engagementSection3Grp1["engagements_section_3_paragraphe_1_title"]) : ?>
                                            <h2  class="TemplateEngagements-territoire-title"><?php echo($engagementSection3Grp1["engagements_section_3_paragraphe_1_title"]); ?></h2>
                                        <?php endif; ?>

                                        <?php if($engagementSection3Grp1["engagements_section_3_paragraphe_1_txt"]) : ?>
                                            <?php echo($engagementSection3Grp1["engagements_section_3_paragraphe_1_txt"]); ?>
                                        <?php endif; ?>

                                    <?php endif; ?>
                                </div>

                                <div class="TemplateEngagements-territoire-2">
                                    <?php if($engagementSection3Grp2) : ?>

                                        <?php if($engagementSection3Grp2["engagements_section_3_paragraphe_2_title"]) : ?>
                                            <p class="TemplateEngagements-territoire-title"><?php echo($engagementSection3Grp2["engagements_section_3_paragraphe_2_title"]); ?></p>
                                        <?php endif; ?>

                                    <?php endif; ?>

                                    <div class="TemplateEngagements-stats">
                                    
                                        <?php if($engagementSection3Grp2['engagements_section_3_paragraphe_2_stat_1']['url']) : ?>
                                            <img src="<?php echo esc_url( $engagementSection3Grp2['engagements_section_3_paragraphe_2_stat_1']['url'] ); ?>" 
                                                  alt="<?php echo esc_attr( $engagementSection3Grp2['engagements_section_3_paragraphe_2_stat_1']['alt'] ); ?>" />
                                        <?php endif; ?>

                                        <?php if($engagementSection3Grp2['engagements_section_3_paragraphe_2_stat_2']['url']) : ?>
                                            <img src="<?php echo esc_url( $engagementSection3Grp2['engagements_section_3_paragraphe_2_stat_2']['url'] ); ?>" 
                                              alt="<?php echo esc_attr( $engagementSection3Grp2['engagements_section_3_paragraphe_2_stat_2']['alt'] ); ?>" />
                                        <?php endif; ?>

                                        <?php if($engagementSection3Grp2['engagements_section_3_paragraphe_2_stat_3']['url']) : ?>
                                            <img src="<?php echo esc_url( $engagementSection3Grp2['engagements_section_3_paragraphe_2_stat_3']['url'] ); ?>" 
                                              alt="<?php echo esc_attr( $engagementSection3Grp2['engagements_section_3_paragraphe_2_stat_3']['alt'] ); ?>" />
                                        <?php endif; ?>

                                    </div>
                                </div>

                                <div class="TemplateEngagements-territoire-3">

                                    <?php if($engagementSection3Grp3) : ?>

                                        <?php if($engagementSection3Grp3["engagements_section_3_paragraphe_3_title"]) : ?>
                                            <p class="TemplateEngagements-territoire-title"><?php echo($engagementSection3Grp3["engagements_section_3_paragraphe_3_title"]); ?></p>
                                        <?php endif; ?>

                                        <?php if($engagementSection3Grp3["engagements_section_3_paragraphe_3_txt"]) : ?>
                                            <?php echo($engagementSection3Grp3["engagements_section_3_paragraphe_3_txt"]); ?>
                                        <?php endif; ?>

                                    <?php endif; ?>

                                </div>
                            
                            </div>
                        </div>
                        <div class="Col Col-6 Col-sm-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/engagements/engagements_montage_2.png" 
                                 srcset="<?php echo get_template_directory_uri(); ?>/img/engagements/engagements_montage_2@2x.png" alt="" data-aos="fade-left">
                        </div>
                    </div>
                </div>
            </div>

            <div class="TemplateEngagements-backtohome" data-aos="fade-up">
                <a href="<?php echo (get_permalink(64)); ?>" class="Button">retour à l’accueil</a>
            </div>




		</section>
		<!-- /section -->

	</main>

<?php get_footer(); ?>
