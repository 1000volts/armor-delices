<?php get_header(); ?>

	<main role="main" class="TemplateHome">

    <h1 class="HiddenSEO">Armor Délices, pâtisserie et biscuiterie bretonne</h1>

    <!-- slider produits -->
        <section class="TemplateHome-section TemplateHome-section--products">
            <div class="Container">
                <?php
                    $sectionTitle = get_field('home_section_products_title');
                    $sectionSubtitle = get_field('home_section_products_subtitle');
                    get_template_part('parts/page-title', null, array('title' => $sectionTitle, 'subtitle' => $sectionSubtitle));
                ?>
            </div>
            <div class="Container">            
                <?php get_template_part('parts/slider-products'); ?>
            </div>
            <div class="Container">
                <div class="TemplateHome-seeAll" data-aos="fade-up">
                    <a href="<?php echo get_permalink( get_option( 'woocommerce_shop_page_id' ) ); ?>"
                     title="<?php _e('Voir tous nos produits','woothemes'); ?>" class="Button">
                     <?php _e('Voir tous nos produits','woothemes'); ?>
                    </a>
                </div>
            </div>
        </section>
<!-- /slider produits -->


<!-- la marque -->
        <section class="TemplateHome-section TemplateHome-section--brand">

            <div class="TemplateHome-section--brand-globalMask">
            <div class="TemplateHome-section--brand-slidermask"></div>
            <div class="Container">
                <div class="Row">
                    <div class="Col Col-6 Col-sm-12">
                        <div class="TemplateHome-section--brand-content">
                            <?php
                                $sectionTitle = get_field('home_section_brand_title');
                                $sectionSubtitle = get_field('home_section_brand_subtitle');
                                get_template_part('parts/page-title', null, array('title' => $sectionTitle, 'subtitle' => $sectionSubtitle));
                            ?> 

                            <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/madeleine_n1.png" 
                                 srcset="<?php echo get_template_directory_uri(); ?>/img/accueil/madeleine_n1@2x.png"      
                                 alt="La madeleine numéro 1 en Bretagne" class="TemplateHome-section--brand-img">

                            <?php $brandPresentation = get_field('home_section_brand_presentation'); ?>
                            <?php if($brandPresentation) : ?>
                                <div class="TemplateHome-section--brand-presentation">
                                    <?php echo ($brandPresentation); ?>
                                </div>
                            <?php endif; ?>

                            <div class="TemplateHome-section--brand-sliderNav TemplateHome-section--brand-sliderNav--desktop">
                                <button class="PolaroidSlider-btn PolaroidSlider-left" name="slide précédente"><img src="<?php echo get_template_directory_uri(); ?>/img/accueil/slider_brand_nav_left.png" alt="slide précédente"></button>
                                <button class="PolaroidSlider-btn PolaroidSlider-right" name="slide suivante"><img src="<?php echo get_template_directory_uri(); ?>/img/accueil/slider_brand_nav_right.png" alt="slide suivante"></button>
                            </div>
                        </div>
                    </div>

                    <div class="Col Col-6 Col-sm-12">
                        <div class="TemplateHome-section--brand-slider">
                            <div class="PolaroidSlider">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/polaroid_slider_1.jpg" 
                                        srcset="<?php echo get_template_directory_uri(); ?>/img/accueil/polaroid_slider_1@2x.jpg 2x"
                                        alt="Écouter le bruit des vagues se dérouler"
                                        class="skip-lazy">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/polaroid_slider_2.jpg"
                                        srcset="<?php echo get_template_directory_uri(); ?>/img/accueil/polaroid_slider_2.jpg 2x"
                                        alt="Réspirer les embruns salés"
                                        class="skip-lazy">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/polaroid_slider_3.jpg"
                                        srcset="<?php echo get_template_directory_uri(); ?>/img/accueil/polaroid_slider_3@2x.jpg 2x"
                                        alt="Sentir le sable sous ses pieds"
                                        class="skip-lazy">
                            </div>
                            <div class="TemplateHome-section--brand-sliderNav TemplateHome-section--brand-sliderNav--mobile">
                                <button class="PolaroidSlider-btn PolaroidSlider-left" name="slide précédente"><img src="<?php echo get_template_directory_uri(); ?>/img/accueil/slider_brand_nav_left.png" alt="slide précédente"></button>
                                <button class="PolaroidSlider-btn PolaroidSlider-right" name="slide suivante"><img src="<?php echo get_template_directory_uri(); ?>/img/accueil/slider_brand_nav_right.png" alt="slide suivante"></button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>
        </section>
<!-- /la marque -->


<!-- catégorie -->
<section class="TemplateHome-section TemplateHome-section--categories">


    <!-- parallax -->
    <div class="TemplateHome-section--categories-paralax">
        <div class="rellax shadow " data-rellax-speed="2"> 
            <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/paralax_madeleine.png" 
            srcset="<?php echo get_template_directory_uri(); ?>/img/accueil/paralax_madeleine@2x.png 2x"
            class="TemplateHome-section--categories-paralax-img">
        </div>

        <div class="rellax" data-rellax-speed="2.2"> 
            <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/paralax_madeleine.png" 
            srcset="<?php echo get_template_directory_uri(); ?>/img/accueil/paralax_madeleine@2x.png 2x"
            class="TemplateHome-section--categories-paralax-img">
        </div>
    </div>

            <div class="Container">
                <div class="Row Row-slick">
                <?php
                
                if( have_rows('home_section_categories_repeater') ):

                    $transitionDelay = 100;
                    $itemNum = 0;
                
                    while( have_rows('home_section_categories_repeater') ) : the_row();

                        switch($itemNum) {
                            case 0 : $transitionDelay = 100; break;
                            case 1 : $transitionDelay = 300; break;
                            case 2 : $transitionDelay = 500; break;
                        }
                
                        $pageImage = get_sub_field('home_section_categories_img');
                        $pageImage2x = get_sub_field('home_section_categories_img_retina');
                        $pageName = get_sub_field('home_section_categories_name');
                        $pageDescription = get_sub_field('home_section_categories_description');
                        $pageLink = get_sub_field('home_section_categories_link');

                        ?>

                        <div class="Col Col-4" data-aos="fade-up" data-aos-delay="<?php echo $transitionDelay; ?>">

                        <article class="CategoryCard">
                            <a href="<?php echo esc_url($pageLink)?>" >
                                <div class="CategoryCard-img">
                                    <img src="<?php echo($pageImage); ?>" 
                                            <?php if($pageImage2x) : ?> srcset="<?php echo($pageImage2x); ?>"<?php endif; ?>
                                            alt="<?php echo($pageName)?>">
                                </div>
                                <div class="CategoryCard-content">
                                    <h2 class="CategoryCard-title"><?php echo($pageName)?></h2>
                                    <div class="CategoryCard-hover">                                    
                                        <p class="CategoryCard-txt"><?php echo($pageDescription)?></p>                                                                        
                                        <span class="Button CategoryCard-cta">En savoir plus</span>                                    
                                    </div>
                                </div>
                            </a>
                        </article>

                        </div>

                        <?php $itemNum++; ?>

                
                    <?php endwhile;
                
                else :

                endif;
                
                ?>

                </div>
            </div>
        </section>
<!-- /catégorie -->

<!-- instagram -->
    <section class="TemplateHome-section TemplateHome-section--instagram">

        <div class="Container Container--instagram">

                <div class="InstagramFlux" data-aos="fade-up">
                    <span class="InstagramFlux-hashtag-armordelices Hashtag Hashtag--armordelices">#armordelices</span>
                    <?php echo(do_shortcode('[instagram-feed feed=1]')); ?>
                    <span class="Hashtag Hashtag--plusdebeurre">#plusdeBeurrequedeMal</span>
                    <h2 class="InstagramFlux-title">Nous suivre sur les réseaux</h2>
                    <?php get_template_part('parts/socials', null, array('color' => 'blue', 'size' => 'big')); ?>
                </div>

        </div>

    </section>
<!-- /instagram -->

<!-- nous retrouver -->
<section class="TemplateHome-section TemplateHome-section--findUs">
            
            <div class="Container">
                    <?php
                        $sectionTitle = get_field('home_section_findUs_title');
                        $sectionSubtitle = get_field('home_section_findUs_subtitle');
                        get_template_part('parts/page-title', null, array('title' => $sectionTitle, 'subtitle' => $sectionSubtitle));
                    ?> 
                <div class="Row">
                    <?php 
                        $imageFindUs = get_field('home_section_findUs_image');
                        $imageFindUs2x = get_field('home_section_findUs_image_2x');
                        $presentationFindUsBoutique = get_field('home_section_findUs_description');                       
                        $presentationFindUsFrance = get_field('home_section_findUs_description_2');                       
                    ?>

                    <div class="Col Col-6 Col-sm-12">
                        <div class="TemplateHome-section--findUs-imageContainer">
                            <img src="<?php echo($imageFindUs['url']); ?>" 
                                <?php if($imageFindUs2x['url']) : ?> srcset="<?php echo($imageFindUs2x['url']); ?> 2x" <?php endif; ?>
                                alt="<?php echo $imageFindUs['alt']; ?>" class="TemplateHome-section--findUs-image">     
                            <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/findus_map_ouest.png" alt="" 
                                 class="TemplateHome-section--findUs-image-map">
                        </div>
                    </div>
                    
                    <div class="Col Col-6 Col-sm-12">
                        <div class="TemplateHome-section--findUs-content">

                            <?php if($presentationFindUsBoutique) : ?>
                                <div class="TemplateHome-section--findUs-presentation"><?php echo($presentationFindUsBoutique); ?></div>
                            <?php endif; ?>

                            <?php

                                if( have_rows('home_section_findUs_boutique_grp') ): while ( have_rows('home_section_findUs_boutique_grp') ) : the_row(); 

                                    if( have_rows('home_section_findUs_boutique_repeater') ): while ( have_rows('home_section_findUs_boutique_repeater') ) : the_row();       

                                        $shopName = get_sub_field('home_section_findUs_boutique_repeater_name');
                                        $shopAddress = get_sub_field('home_section_findUs_boutique_repeater_address');
                                        $shopLinkGmap = get_sub_field('home_section_findUs_boutique_repeater_gmap');
                                        $shopPhone = get_sub_field('home_section_findUs_boutique_repeater_phone');
                                        $shopInfos = get_sub_field('home_section_findUs_boutique_repeater_infos');

                                        ?>

                                            <div class="ShopInfoHome" data-aos="fade-up">

                                                <p class="ShopInfoHome-name"><strong><?php echo($shopName); ?></strong></p>
                                                <address class="ShopInfoHome-address"><?php echo($shopAddress); ?></address>
                                                <div class="ShopInfoHome-infos">
                                                    <?php echo $shopInfos; ?>
                                                </div>                                                                                                              
                                                
                                                <div class="ShopInfoHome-ctaContainer">  
                                                
                                                <?php if($shopLinkGmap) : ?>                                                     
                                                        <a href="<?php echo($shopLinkGmap); ?>" class="ShopInfoHome-link" target="_blank" rel="noreferrer noopener">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_map_blue.png" class="ShopInfoHome-link-icon" alt="itinéraire">
                                                            <span class="ShopInfoHome-link-txt">Itinéraire</span>
                                                        </a>                                                    
                                                <?php endif; ?>

                                                <?php if($shopPhone) : ?>
                                                        <?php $phoneLink = str_replace(' ', '', $shopPhone); ?>
                                                        <a href="tel:+33<?php echo($shopPhone); ?>" class="ShopInfoHome-link">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_tel_blue.png" class="ShopInfoHome-link-icon" alt="telephone">
                                                            <span class="ShopInfoHome-link-txt"><?php echo $shopPhone; ?></span>
                                                        </a>
                                                <?php endif; ?>                                                

                                                </div>

                                            </div>

                                        <?php 

                                    endwhile; endif;
                                
                                endwhile; endif;
                            ?>

                            <hr>

                            <?php if( $presentationFindUsFrance ) : ?>
                                <div class="TemplateHome-section--findUs-presentation-2" data-aos="fade-up">
                                    <div class="TemplateHome-section--findUs-presentation-2-txt">
                                        <?php echo($presentationFindUsFrance); ?>
                                    </div>
                                    <div class="TemplateHome-section--findUs-presentation-2-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/findus_map_france@2x.png" 
                                             alt="Carte de France">
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="TemplateHome-seeAll TemplateHome-seeAll--findUs" data-aos="fade-up">
                                <a href="<?php echo get_permalink( 1184 ); ?>"
                                    title="Tous nos points de vente" class="Button">Tous nos points de vente</a>
                            </div>

                        </div>
                    </div>



                </div>
            </div>
        </section>
<!-- /nous retrouver -->


<!-- newsletter -->

        <section class="TemplateHome-section TemplateHome-section--newsletter">



        <!-- parallax -->
        <div class="TemplateHome-section--newsletter-paralax">
        <div class="rellax shadow " data-rellax-speed="2"> 
            <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/paralax_madeleine.png" 
            srcset="<?php echo get_template_directory_uri(); ?>/img/accueil/paralax_madeleine@2x.png 2x"
            class="TemplateHome-section--newsletter-paralax-img">
        </div>

        <div class="rellax" data-rellax-speed="2.2"> 
            <img src="<?php echo get_template_directory_uri(); ?>/img/accueil/paralax_madeleine.png" 
            srcset="<?php echo get_template_directory_uri(); ?>/img/accueil/paralax_madeleine@2x.png 2x"
            class="TemplateHome-section--newsletter-paralax-img">
        </div>
    </div>


            <?php get_template_part('parts/newsletter-form'); ?>

        </section>


<!-- /newsletter -->





	</main>

<?php get_footer(); ?>
