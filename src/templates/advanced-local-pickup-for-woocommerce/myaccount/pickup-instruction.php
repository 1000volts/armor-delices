<?php
if (!empty($w_day)) { 	
	$n = 0;
	$new_array = [];
	$previousValue = [];
	
	foreach ($w_day as $day=>$value) {				
		if (isset($value['checked']) && 1 == $value['checked']) {																	
			if ($value != $previousValue) {
				$n++;
			}
			$new_array[$n][$day] = $value;					
			$previousValue = $value;
		} else {
			$n++;
			$new_array[$n][$day] = '';	
			$previousValue = '';
		}							
	}
}
	
$alp = new WC_Local_Pickup_admin();
$wclp_pickup_instruction_customizer = new wclp_pickup_instruction_customizer();

$hide_instruction_heading = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'hide_instruction_heading', '');

$hide_table_header = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'hide_table_header', '');

$location_box_heading = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'location_box_heading', $wclp_pickup_instruction_customizer->defaults['location_box_heading']);

$header_address_text = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'header_address_text', $wclp_pickup_instruction_customizer->defaults['header_address_text']);

$header_business_text = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'header_business_text', $wclp_pickup_instruction_customizer->defaults['header_business_text']);

$header_background_color = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'header_background_color', $wclp_pickup_instruction_customizer->defaults['header_background_color']);

$header_font_size = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'header_font_size', $wclp_pickup_instruction_customizer->defaults['header_font_size']);

$location_box_font_size = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'location_box_font_size', $wclp_pickup_instruction_customizer->defaults['location_box_font_size']);

$location_box_content_line_height = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'location_box_content_line_height', $wclp_pickup_instruction_customizer->defaults['location_box_content_line_height']);

$location_box_border_size = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'location_box_border_size', $wclp_pickup_instruction_customizer->defaults['location_box_border_size']);

$location_box_font_color = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'location_box_font_color', $wclp_pickup_instruction_customizer->defaults['location_box_font_color']);

$header_font_color = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'header_font_color', $wclp_pickup_instruction_customizer->defaults['header_font_color']);

$location_box_border_color = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'location_box_border_color', $wclp_pickup_instruction_customizer->defaults['location_box_border_color']);

$location_box_background_color = $alp->get_option_value_from_array('pickup_instruction_display_settings', 'location_box_background_color', $wclp_pickup_instruction_customizer->defaults['location_box_background_color']);	
?>
<?php if ('yes' != $hide_instruction_heading) { ?>
	<?php /*
	<h2 class="local_pickup_email_title"><?php echo esc_html($location_box_heading); ?></h2>
	*/ ?>
<?php } ?>
<div class="wclp_mail_address">
	<div class="wclp_location_box 
		<?php 
		if (!empty($new_array)) { 
			echo 'wclp_location_box1';
		} 
		?>
		">
		<?php if ('yes' != $hide_table_header) { ?>
			<div class="wclp_location_box_heading">				
				<?php /* MV - Commentaire trad ne fonctionne
				esc_html_e($header_address_text, 'woocommerce'); */?>
				Adresse de retrait boutique
			</div>
		<?php } ?>
		<?php if (class_exists('Advanced_local_pickup_PRO')) { ?>
				<?php do_action('wclp_location_address_display_html', $location, $store_state, $store_country); ?>
		<?php } else { ?>
			<div class="wclp_location_box_content">
				<p class="wclp_pickup_adress_p">
					<?php 
					if (!empty($location->store_name)) {
						echo esc_html($location->store_name);
						echo ', '; 
					} 
					?>
				</p>
				<p class="wclp_pickup_adress_p">
					<?php 
					if (!empty($location->store_address)) {
						echo esc_html($location->store_address);
						if (!empty($location->store_address_2)) {
							echo ', '; 
						}
					} 
					if (!empty($location->store_address_2)) {
						echo esc_html($location->store_address_2);
						echo ', ';
					}
					?>
				</p>
				<p class="wclp_pickup_adress_p">
					<?php
					if (!empty($location->store_city)) {
						echo esc_html($location->store_city);
						if ('' != $store_state) {
							echo ', ';
						}
					}
					if ('' != $store_state) {
						echo esc_html(WC()->countries->get_states( $store_country )[$store_state]);
					} 
					if ($store_country) {
						echo ', ';
					}
					if ($store_country) {
						echo esc_html(WC()->countries->countries[$store_country]);
						if (!empty($location->store_postcode)) {
							echo ', ';
						}
					} 
					if (!empty($location->store_postcode)) {
						echo esc_html($location->store_postcode);
					}
					?>
				</p>
				
				<?php if (!empty($location->store_phone)) { ?>
					<p class="wclp_pickup_adress_p"><?php echo esc_html($location->store_phone); ?></p>
				<?php } ?>
				<?php if (!empty($location->store_instruction)) { ?>
					<p class="wclp_pickup_adress_p"><?php echo esc_html($location->store_instruction); ?></p>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
<?php 
if (!empty($w_day)) { 	
	if (!empty($new_array)) {	
		$resultEmpty = array_filter(array_map('array_filter', $new_array)); //echo count( $resultEmpty ); 
		?>	
	<div class="wclp_location_box 
		<?php 
		if (!empty($new_array)) {
			echo 'wclp_location_box2';
		} 
		?>
		"   
		<?php
		if (0 == count($resultEmpty)) {
			echo 'style="display:none;"';
		} 
		?>
		>
		<?php if ('yes' != $hide_table_header) { ?>
			<div class="wclp_location_box_heading">
				<?php esc_html_e($header_business_text, 'advanced-local-pickup-for-woocommerce'); ?>
			</div>
		<?php } ?>
		<div class="wclp_location_box_content">

            <?php
				// Ajout MV - voir functions-woocom
				do_action('mv_advanced_local_pickup_custom_hours');
			?>
            
            <?php /* COMMENTAIRE MV (le plugin ne gère pas la coupure de midi)
			<?php
			foreach ($new_array as $key => $data) {
				if (1 == count($data)) {							
					if (isset(reset($data)['wclp_store_hour']) && '' != reset($data)['wclp_store_hour'] && isset(reset($data)['wclp_store_hour_end']) && '' != reset($data)['wclp_store_hour_end']) {
						reset($data);
						?>		
						<p class="wclp_work_hours_p">
							<?php 
							echo esc_html(ucfirst(key($data)), 'default') . ' <span>: ' . esc_html(reset($data)['wclp_store_hour']) . ' - ' . esc_html(reset($data)['wclp_store_hour_end']);
							do_action('wclp_get_more_work_hours_contents', $data);
							echo '</span>';
							?>
						</p>								
				<?php } } ?>						
				<?php
				if (2 == count($data)) {
					if (isset(reset($data)['wclp_store_hour']) && '' != reset($data)['wclp_store_hour'] && isset(reset($data)['wclp_store_hour_end']) && '' != reset($data)['wclp_store_hour_end']) {
						reset($data);
						$array_key_first = key($data);
						end($data);
						$array_key_last = key($data);
						?>
						<p class="wclp_work_hours_p">
							<?php 
							echo esc_html(ucfirst($array_key_first), 'default') . '<span> - </span>' . esc_html(ucfirst($array_key_last), 'default') . ' <span>: ' . esc_html(reset($data)['wclp_store_hour']) . ' - ' . esc_html(reset($data)['wclp_store_hour_end']);
							do_action('wclp_get_more_work_hours_contents', $data);
							echo '</span>';
							?>	
						</p>
				<?php } } ?>									
				<?php 
				if (count($data) > 2) { 
					if (isset(reset($data)['wclp_store_hour']) && '' != reset($data)['wclp_store_hour'] && isset(reset($data)['wclp_store_hour_end']) && '' != reset($data)['wclp_store_hour_end']) {
					reset($data);
					$array_key_first = key($data);
					end($data);
					$array_key_last = key($data);		
						?>
						<p class="wclp_work_hours_p">
							<?php 
							echo esc_html(ucfirst($array_key_first), 'default') . esc_html(' To ', 'advanced-local-pickup-for-woocommerce') . esc_html(ucfirst($array_key_last), 'default') . ' <span>: ' . esc_html(reset($data)['wclp_store_hour']) . ' - ' . esc_html(reset($data)['wclp_store_hour_end']); 
							do_action('wclp_get_more_work_hours_contents', $data);
							echo '</span>';
							?>	
						</p>
						<?php 										
					}
				}	
			}*/
			if (class_exists('Advanced_local_pickup_PRO')) {
				if (!empty($location->store_holiday_message)) {
					?>
					<p class="wclp_pickup_adress_p"><?php echo esc_html($location->store_holiday_message); ?></p>
					<?php 
				}
			}
			?>	
		</div>		
	</div>
<?php } } ?>
</div>
<style>
	.wclp_mail_address{
		margin: 10px 0;
		display: table;
		width: 100%;
	}
	.local_pickup_email_title{
		margin-bottom: 10px !important;
	}
	.wclp_location_box{
		display: table-cell;
		width:50%;	
		border: <?php echo esc_html($location_box_border_size); ?> solid <?php echo esc_html($location_box_border_color); ?> !important;
	}
	.wclp_location_box2{
		border-left: 0 !important;
	}
	.wclp_location_box_heading {
		border-bottom: <?php echo esc_html($location_box_border_size); ?> solid <?php echo esc_html($location_box_border_color); ?> !important;
		border-top:0 !important;
		border-left:0 !important;
		border-right:0 !important;
		padding: 10px;
		font-size: <?php echo esc_html($header_font_size); ?>;
		color: <?php echo esc_html($header_font_color); ?>;
		font-weight: bold;
		background:<?php echo esc_html($header_background_color); ?>;
		text-align: inherit;
	}
	.wclp_location_box_content{												
		padding: 10px;		
	}
	.wclp_work_hours_p{
		margin: 0 !important;
		line-height: <?php echo esc_html($location_box_content_line_height); ?>;
		color: <?php echo esc_html($location_box_font_color); ?>;
		font-size: <?php echo esc_html($location_box_font_size); ?>;
	}
	.wclp_pickup_adress_p{
		margin: 0 !important;
		line-height: <?php echo esc_html($location_box_content_line_height); ?>;
		color: <?php echo esc_html($location_box_font_color); ?>;
		font-size: <?php echo esc_html($location_box_font_size); ?>;
	}
	.wclp_mail_address{
		background: <?php echo esc_html($location_box_background_color); ?>; 
	}
	@media screen and (max-width: 500px) {
	.wclp_location_box2{
		border-left: <?php echo esc_html($location_box_border_size); ?> solid <?php echo esc_html($location_box_border_color); ?> !important;
		border-top: 0 !important;
	}
	.wclp_location_box{
		display: block;
		width: 100%;
	}
	}
</style>
