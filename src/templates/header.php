<!doctype html>
<html <?php language_attributes(); ?> class="no-js">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?></title>

	<link href="//www.google-analytics.com" rel="dns-prefetch">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<script src="https://cdn.jsdelivr.net/gh/dixonandmoe/rellax@master/rellax.min.js"></script>

	<?php wp_head(); ?>
	<script>
		// conditionizr.com
		// configure environment tests
		conditionizr.config({
			assets: '<?php echo get_template_directory_uri(); ?>',
			tests: {}
		});
	</script>
	<!-- Auto set via html-webpack-plugin: <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/bundle.js"></script> -->

</head>

<body <?php body_class(); ?>>

	<?php 


	?>

	<!-- wrapper -->
	<div id="Root">

		<?php get_template_part('parts/menu-burger'); ?>

		<?php if ( !is_user_logged_in() ): ?>
		<div class="Sidebar hidden" id="login-sidebar" style="display:none;">
			<div class="Sidebar-mask"></div>
			<div class="Sidebar-content">
				<div class="Sidebar-container">
					<button class="Sidebar-closeBtn"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_close_blue.png" alt="Fermer"></button>
					<?php get_template_part('parts/woocommerce-mv/login-sidebar'); ?>
					<?php get_template_part('parts/woocommerce-mv/register-sidebar'); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<!-- popin newsletter -->
		<div class="PopinNewsletter hidden" id="popin-newsletter" style="display: none;">			
			<div class="PopinNewsletter-mask" id="popin-newsletter-mask"></div>	
			<div class="Container">
				<div class="PopinNewsletter-content">
					<div class="PopinNewsletter-img"></div>
					<div class="PopinNewsletter-formContainer">
						<button class="PopinNewsletter-closeBtn" id="popin-newsletter-close"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_close_blue.png" alt="Fermer"></button>
						<?php get_template_part('parts/newsletter-form', null, array('class'=>'Newsletter--mobile')); ?>	
					</div>		
				</div>
			</div>
		</div>
		<!-- /popin newsletter -->

		

		<!-- header -->
		<header class="Header clear" role="banner" id="header">

			<?php get_template_part('parts/header-promo'); ?>

			<div class="Header-mobile">
				<div class="HeaderMobile">
					<div class="Container">
						<div class="HeaderMobile-container">
							<div class="HeaderMobile-nav">
								<button class="HeaderMobile-burger HeaderMobile-icon HeaderMobile-icon--burger" id="burgeropen">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_menu_mobile.png" alt="Menu mobile">
								</button>
							</div>
							<div class="HeaderMobile-logo">
								<a href="<?php echo home_url(); ?>">
									<img src="<?php echo get_template_directory_uri(); ?>/img/logo_armor_delices_mobile.png" alt="Logo Armor Délice">
								</a>
							</div>
							<div class="HeaderMobile-tools">
								<?php if ( is_user_logged_in() ) : ?>
									<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" 
										title="<?php _e('Mon compte','woothemes'); ?>" 
										class="HeaderMobile-icon HeaderMobile-icon--login HeaderMobile-login">
										<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_account_blue.png" alt="Mon compte">
									 </a>
								<?php else : ?>
									<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" 
										title="<?php _e('Connexion','woothemes'); ?>" 
										class="HeaderMobile-icon HeaderMobile-icon--login HeaderMobile-login" id="mobile-login-open">
										<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_account_blue.png" alt="Connexion">
									</a>
								<?php endif; ?>
								<a href="<?php echo get_permalink( get_option('woocommerce_cart_page_id') ); ?>" 
									class="HeaderMobile-icon HeaderMobile-icon--cart HeaderMobile-cart" 
									id="mobile-cart-open">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_cart_blue.png" alt="Panier">
									<?php get_template_part('parts/cart-count'); ?>
								</a>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="Header-logobar">						
				<div class="Container">	
					<div class="MenuBar">							
						<nav class="MenuBar-left">
							<?php get_template_part('parts/menu-secondary'); ?>
						</nav>			
						<nav class="MenuBar-right">							
							<?php get_template_part('parts/menu-wc'); ?>
						</nav>				
					</div>
				</div>
			<!-- LogoBar -->
			<div class="LogoBar">
				<div class="Container">
					<div class="LogoBar-madeleine">
						<img src="<?php echo get_template_directory_uri() . load_random_madeleine_image_url(); ?>"
							 alt="">
					</div>
					<div class="LogoBar-logo">
						<!-- logo -->
						<div class="Logo">
							<a href="<?php echo home_url(); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/logo_armor_delices.png" alt="Logo" class="logo-img">
							</a>
						</div>
						<!-- /logo -->
					</div>

					<nav class="LogoBar-links">

						<?php get_template_part('parts/menu-big-icon'); ?>

					</nav>
				</div>
			</div>
			<!-- /logobar -->
			</div>
			

		</header>
		<!-- /header -->

		<?php get_template_part('parts/header-sticky'); ?>

		
		<?php if( !is_page(array(64)) ) : ?>									
			<?php if( !is_shop() ) : ?>									
				<div class="Container">
					<!-- breadcrumb -->
					<div class="BreadCrumb">
						<?php get_template_part('parts/breadcrumb'); ?>
					</div>
					<!-- /breadcrumb -->
				</div>
			<?php endif; ?>			
		<?php endif; ?>			

		<?php get_template_part('parts/sliders'); ?>



