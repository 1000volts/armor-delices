<?php defined( 'ABSPATH' ) || exit; ?>


<?php

    $color = "";
    $size = "";
    $logo_fb = "";
    $logo_insta = "";
    $logo_pinterest = "";

    // par défaut en blanc
    $array_defaults = array( 
        'color' => 'white',
        'size' => 'normal',
      ); 

    // verif si param est passé 
    $args = wp_parse_args( $args, $array_defaults );


    if ( $args['color'] ) {
        $color = $args['color'];        
    } else {
        $color = "white";
    }

    if ( $args['size'] ) {
        $size = $args['size'];        
    } else {
        $size = "normal";
    }

    if ($color == "white") {
        $logo_fb = "/img/icons/facebook.svg";
        $logo_insta = "/img/icons/instagram.svg";
        $logo_pinterest = "/img/icons/pinterest.svg";
    } elseif ($color == "blue") {
        $logo_fb = "/img/icons/facebook-white.svg";
        $logo_insta = "/img/icons/instagram-white.svg";
        $logo_pinterest = "/img/icons/pinterest-white.svg";
    }

?>

<div class="Socials Socials--<?php echo $color; ?> Socials--<?php echo $size; ?>">
	<a href="https://www.facebook.com/Armor-D%C3%A9lices-112108574688340/" target="_blank" rel="noreferrer noopener"><img src="<?php echo get_template_directory_uri() . $logo_fb; ?>" alt="Facebook"></a>
	<a href="https://www.instagram.com/armordelices/" target="_blank" rel="noreferrer noopener"><img src="<?php echo get_template_directory_uri() . $logo_insta; ?>" alt="Instagram"></a>
	<a href="https://www.pinterest.fr/armordelices/" target="_blank" rel="noreferrer noopener"><img src="<?php echo get_template_directory_uri() . $logo_pinterest; ?>" alt="Pinterest"></a>
</div>	