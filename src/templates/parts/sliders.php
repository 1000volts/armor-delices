<?php defined( 'ABSPATH' ) || exit; ?>


<?php if(is_front_page()) : ?>
    
    <section class="Slider Slider--home">

        <?php echo do_shortcode('[metaslider id="424"]'); ?>

    </section>

<?php endif; ?>


<?php if(is_shop()) : ?>
    
    <section class="Slider Slider--shop">

        <?php ////// ATTENTION ID DE PROD ET PREPROD DIFFERENT 591 pour prod ?>
        <?php echo do_shortcode('[metaslider id="591"]'); ?>

    </section>

<?php endif; ?>