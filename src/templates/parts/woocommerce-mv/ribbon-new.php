<?php defined( 'ABSPATH' ) || exit; ?>

<?php if(get_field('show_new_ribbon')) : ?>

    <span class="RibbonProduct">
        <img src="<?php echo get_template_directory_uri(); ?>/img/badge_nouveau.png" alt="Nouveau !">
    </span>

<?php endif; ?>