<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div class="LoginSidebarContent" id="LoginSidebarContent">

    <div class="LoginSidebarContent-title">
        <span class="Sidebar-title">Connexion</span>
    </div>

    <div class="LoginSidebarContent-log">
        <?php if (isset($_POST['login'])): wc_print_notices(); endif; ?>
        <div class="woocommerce">
            <?php woocommerce_login_form(); ?>
        </div>
    </div>

    <div class="LoginSidebarContent-separator"><span>Ou</span></div>

    <div class="LoginSidebarContent-create">

        <span class="LoginSidebarContent-create-title">NOUVEAU CLIENT</span>
        <p>C'est votre première commande sur notre site ?<br>
            Créez votre compte en toute simplicité !</p>
        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="Button" id="login-button-create-account">Créer un compte</a>
        <span class="LoginSidebarContent-infosForm">* Champs obligatoires</span>
    </div>
    
</div>