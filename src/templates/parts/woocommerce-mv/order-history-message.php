<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>


<div class="mv-woocommerce-orders-message">Si vous souhaitez annuler une commande vous pouvez utiliser notre <a href="<?php echo get_permalink(76); ?>">formulaire de contact</a></div>