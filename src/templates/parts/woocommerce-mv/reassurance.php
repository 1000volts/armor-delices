<div class="Reassurance">

    <div class="Reassurance-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon-lock-grey.png">
        <span>Paiement sécurisé</span>
    </div>
    

    <div class="Reassurance-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon-expedition-grey.png">
        <span>Expédition sous 72h ouvrées</span>
    </div>


</div>