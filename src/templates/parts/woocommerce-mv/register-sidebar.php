<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div class="RegisterSidebarContent" id="RegisterSidebarContent" style="display:none">

    <div class="LoginSidebarContent-title">
        <p class="Sidebar-title">S'enregistrer</p>
    </div>    

    <div class="RegisterSidebarContent-log">
        <?php if (isset($_POST['register'])): wc_print_notices(); endif; ?>
        <div class="woocommerce">
            <?php get_template_part('woocommerce/myaccount/form-login'); ?>
        </div>

        <div class="RegisterSidebarContent-footer">
            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Connexion','woothemes'); ?>" id="back-to-login-button">
                Revenir à la connexion
            </a>
        </div>
    </div>

    
</div>