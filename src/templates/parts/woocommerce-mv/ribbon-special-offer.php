<?php defined( 'ABSPATH' ) || exit; ?>

<?php if(get_field('show_special_offer_ribbon')) : ?>

    <span class="RibbonProduct">
        <img src="<?php echo get_template_directory_uri(); ?>/img/badge_offre_speciale.png" alt="Offre spéciale">
    </span>

<?php endif; ?>