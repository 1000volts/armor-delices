<?php defined( 'ABSPATH' ) || exit; ?>

<div class="CartSidebarContent">

<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) { 

    $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
        ?>
        <div class="CartSidebarItem">
            <div class="CartSidebarItem-imgContainer">
        <?php
		    $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
		    if ( ! $product_permalink ) {
		    	echo $thumbnail; // PHPCS: XSS ok.
		    } else {
		    	printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
		    }
		    ?>
            </div>

            <div class="CartSidebarItem-content">
                <div class="CartSidebarItem-name">
                <?php
                    if ( ! $product_permalink ) {
                        echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
                    } else {
                        echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
                    }
                ?>
                </div>
                <div class="CartSidebarItem-qty">
                    <?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input(
								array(
									'input_name'   => "cart[{$cart_item_key}][qty]",
									'input_value'  => $cart_item['quantity'],
									'max_value'    => $_product->get_max_purchase_quantity(),
									'min_value'    => '0',
									'product_name' => $_product->get_name(),
								),
								$_product,
								false
							);
						}
						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
                    ?>
                </div>
                <div class="CartSidebarItem-price">
                    <?php
                     	echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                    ?>
                </div>
            </div>
            <!-- CartSidebarItem-content -->
            <div class="CartSidebarItem-removeContainer">
                <?php
			    	echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			    		'woocommerce_cart_item_remove_link',
			    		sprintf(
			    			'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
			    			esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
			    			esc_html__( 'Remove this item', 'woocommerce' ),
			    			esc_attr( $product_id ),
			    			esc_attr( $_product->get_sku() )
			    		),
			    		$cart_item_key
			    	);
			    ?>
            </div>
        </div>
        <!-- CartSidebarItem -->

    <?php
    }
} //FOREACH CART
?>


</div>

<?php if ( wc_coupons_enabled() ) { ?>
    <div class="CartSidebarCodePromo">
    	<div class="coupon">
    		<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
    		<?php do_action( 'woocommerce_cart_coupon' ); ?>
    	</div>					
    </div>
<?php } ?>

<button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

<?php do_action( 'woocommerce_cart_actions' ); ?>
<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>