<?php defined( 'ABSPATH' ) || exit; ?>


<?php 
    if ( WC()->cart->get_cart_contents_count() == 0 ) {
        get_template_part('parts/woocommerce-mv/cart-sidebar-empty');
    } else {
        get_template_part('parts/woocommerce-mv/cart-sidebar');
    }
?>

