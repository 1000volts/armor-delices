<?php defined( 'ABSPATH' ) || exit; ?>

<?php 

    global $product;

    // recup les variation et les attributs
    $available_variations = $product->get_available_variations();
    $attributes = $product->get_variation_attributes();

?>


<?php if(!empty( $available_variations ) ) : ?>

    <span class="Variations-title">Votre sélection</span>

    <ul class="Variations">

    <?php foreach( $available_variations as $variation ) : ?>

        <?php 
            $variation_attribute = $variation['attributes'];
            $variation_name = reset($variation_attribute);        
        ?>

        <li class="Variations-item">
            <div class="Variations-item-imageWrapper">
                <input type="image" alt=""
                    src="<?php echo $variation['image']['url']; ?>"
                    data-type="variation-select"
                    data-id="<?php echo $variation['variation_id'] ?>">
                <span class="Variations-item-bg"></span>
                <span class="Variations-item-bg Variations-item-bg--active"></span>
            </div>
            <span class="Variations-item-name"><?php if($variation_name != "") { echo $variation_name; } ?></span>
        </li>

    <?php endforeach; ?>

    </ul>

<?php endif; ?>    
