<?php defined( 'ABSPATH' ) || exit; ?>


<div class="BrandValues">
    <div class="BrandValues-container">
        <div class="BrandValues-item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/illus_carte_france.png" 
                 srcset="<?php echo get_template_directory_uri(); ?>/img/illus_carte_france@2x.png 2x">
            <span class="BrandValues-value">Fabriqué en Bretagne</span>
        </div>
    </div>

    <div class="BrandValues-container">
        <div class="BrandValues-item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/illus_moulin.png" 
                 srcset="<?php echo get_template_directory_uri(); ?>/img/illus_moulin@2x.png 2x">
            <span class="BrandValues-value">Farine locale</span>
        </div>
    </div>

    <div class="BrandValues-container">
        <div class="BrandValues-item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/illus_panier.png" 
                 srcset="<?php echo get_template_directory_uri(); ?>/img/illus_panier@2x.png 2x">
            <span class="BrandValues-value">Oeufs frais français</span>
        </div>
    </div>

    <div class="BrandValues-container">
        <div class="BrandValues-item BrandValues-item--conservateur">
            <img src="<?php echo get_template_directory_uri(); ?>/img/illus_conservateur.png" 
                 srcset="<?php echo get_template_directory_uri(); ?>/img/illus_conservateur@2x.png 2x">
        </div>
    </div>

</div>