<div class="InfosNutritionnelles">

    <?php $infoReglement = get_field('tab_info_nutri_reglement'); ?>

    <p class="InfosNutritionnelles-title">Valeurs nutritionnelles</p>
    
    <?php if($infoReglement) : ?>
        <p class="InfosNutritionnelles-infos">Valeurs nutritionnelles selon règlement 1169/2011 :</p>
    <?php endif; ?>
    
    <?php $nutriTable = get_field('tab_info_nutri'); ?>

    <?php if($nutriTable) : ?>

        <?php echo(do_shortcode('[table id='. $nutriTable .' /]')); ?>

    <?php endif; ?>

</div>