<div class="Ingredients">

    <div class="Ingredients-content">
        <p class="Ingredients-title">Ingrédients</p>
        <?php the_field('tab_ingredient'); ?>
    </div>

    <div class="Ingredients-enSavoirPlus">
        <span>En savoir plus sur le produit ?</span>
        <p class="Ingredients-faq-contact">
            Consultez notre <a href="<?php echo get_permalink(80); ?>">FAQ</a> ou <a href="<?php echo get_permalink(74); ?>">contactez nous</a>
        </p>
    </div>

</div>