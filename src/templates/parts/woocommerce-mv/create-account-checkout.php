<?php defined( 'ABSPATH' ) || exit; ?>

<?php 

    $is_registration_enabled = version_compare('3.0', WC()->version, '<=') ? WC()->checkout()->is_registration_enabled() : get_option( 'woocommerce_enable_signup_and_login_from_checkout' ) == 'yes'; 
    $stop_at_login = ( ! $is_registration_enabled && WC()->checkout()->is_registration_required() && ! is_user_logged_in() ) ? true : false;

?>

    <?php if(is_checkout()): ?>

        <div class="wpmc-mv-createAccount">
            <div class="CheckoutCreateAccount">
                <span class="CheckoutCreateAccount-title">Je crée un compte</span>
                <p class="CheckoutCreateAccount-txt">C'est votre première commande sur notre site ?<br/> Enregistrez-vous pour suivre vos commandes !</p>
                <div class="CheckoutCreateAccount-btnContainer">
                    <button class="Button login-button-create-account-checkout">Créer un compte</button>

                    <?php if(!$stop_at_login) : // on affiche le bouton skip si le paramètrage est ok ?>
                        <button id="wpmc-skip-login" class="Button Button--secondary wpmc-nav-button" type="button">Continuer sans compte</button>
                    <?php endif; ?>

                </div>
            </div>
        </div>        
        
        <div class="wpmc-mv-rgpd">
            <span class="CheckoutCreateAccount-obligatoire">* Champs obligatoires</span>
            <p class="CheckoutCreateAccount-legal">Vos données personnelles seront utilisées pour vous accompagner 
                au cours de votre visite du site web, gérer l’accès à votre compte,
                 et pour d’autres raisons décrites dans notre politique de confidentialité.</p>
        </div>

    <?php endif; ?>

