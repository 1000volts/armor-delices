<?php defined( 'ABSPATH' ) || exit; ?>


<?php 

    $bandeauPromoGroup = get_field('bandeau_promo_group', 'option');

    if(is_checkout()) {
        $messageBandeauPromo = $bandeauPromoGroup['bandeau_promo_message_checkout'];
        $colorBandeauPromo = $bandeauPromoGroup['bandeau_promo_color_checkout'];
        $colorTextBandeauPromo = $bandeauPromoGroup['bandeau_promo_color_text_checkout'];
    } else {
        $messageBandeauPromo = $bandeauPromoGroup['bandeau_promo_message'];
        $colorBandeauPromo = $bandeauPromoGroup['bandeau_promo_color'];
        $colorTextBandeauPromo = $bandeauPromoGroup['bandeau_promo_color_text'];

    }

    //par défaut en blanc si non renseigné
    if($colorBandeauPromo == "") {
        $colorBandeauPromo = "#FFFFFF";
    }

    if($colorTextBandeauPromo == "") {
        $colorTextBandeauPromo = "#000000";
    }

?>


<?php if($messageBandeauPromo) : ?>
<div class="HeaderPromo" style="background-color: <?php echo ($colorBandeauPromo) ?>;">
	<div class="Container">
		<div class="HeaderPromo-txt" style="color: <?php echo ($colorTextBandeauPromo) ?>; "><?php echo ($messageBandeauPromo); ?></div>
	</div>
</div>
<?php endif; ?>