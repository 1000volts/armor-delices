<?php defined( 'ABSPATH' ) || exit; ?>

<ul class="NavMyAccount">
    <li><a href="<?php echo wc_get_account_endpoint_url( 'edit-account' ); ?>">Mes informations</a></li>
    <li><a href="<?php echo wc_get_account_endpoint_url( 'orders' ); ?>">Mes commandes</a></li>
    <li><a href="<?php echo wc_get_account_endpoint_url( 'edit-address' ); ?>">Mes adresses</a></li>
    <li class="NavMyAccount-logout"><a href="<?php echo wc_get_account_endpoint_url( 'customer-logout' ); ?>">Déconnexion</a></li>
</ul>