<?php defined( 'ABSPATH' ) || exit; ?>

<?php 
    // on recupère les custom field de ACF
    $recette_resume = get_field('recette_resume');
?>

<article class="RecetteItem">

    <a href="<?php the_permalink(); ?>" class="RecetteItem-contentWrapper">

        <div class="RecetteItem-image">

        <?php if(has_post_thumbnail()) : ?>
            <?php the_post_thumbnail( 'full' ); ?>
        <?php else : ?>
            <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder_recettes.jpg" alt="">
        <?php endif; ?>

        </div>

        <div class="RecetteItem-infos">
            <h3 class="RecetteItem-title"><?php the_title(); ?></h3>
            <p class="RecetteItem-resume"><?php echo $recette_resume; ?></p>
        </div>

    </a>
    

</article>