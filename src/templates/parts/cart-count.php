<?php defined( 'ABSPATH' ) || exit; ?>

<?php global $woocommerce; ?>

<span class="mini-cart-count"><?php echo $woocommerce->cart->cart_contents_count ?></span>