<?php defined( 'ABSPATH' ) || exit; ?>

<div class="MessageOrderWaitingPayment">

    <div class="MessageOrderWaitingPayment-titleContainer">

        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/alert_cart.png" alt="" class="MessageOrderWaitingPayment-icon">

        <span class="MessageOrderWaitingPayment-title">
            Vous rencontrez un problème lors du paiement ?
        </span>

    </div>

    <p class="MessageOrderWaitingPayment-info">Faites nous part de votre problème en <a href="<?php echo esc_url(link_to_contact_form_with_url_parameter()); ?>">cliquant ici</a>, nous ferons le nécessaire pour y remédier</p>

</div>