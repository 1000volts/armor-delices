<?php defined( 'ABSPATH' ) || exit; ?>

<?php

    $sectionTitle = "";
    $sectionSubtitle = "";

    if ($args['title']) {
        $sectionTitle = $args['title'];
    }

    if ($args['subtitle']) {
        $sectionSubtitle = $args['subtitle'];
    }
   
?>

<?php if($sectionTitle) : ?>

<div class="PageTitle">

    <h2 class="PageTitle-h2 page-title"><?php echo($sectionTitle); ?></h2>

    <?php if($sectionSubtitle) : ?>
        <p class="PageTitle-txt"><?php echo($sectionSubtitle); ?></p>
    <?php endif; ?>

</div>

<?php endif; ?>