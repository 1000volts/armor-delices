<?php defined( 'ABSPATH' ) || exit; ?>

<a href="<?php echo get_permalink(1184); ?>" class="MenuWC <?php echo active_menu_class(1184); ?>">
	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_position_blue.png"  
 	alt="" class="MenuWC-link-icon">
 	Où nous trouver ?
</a>

<?php if ( is_user_logged_in() ) : ?>
	
	<div class="Dropdown">
		<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" 
			class="MenuWC Dropdown-button <?php echo active_menu_class( get_option('woocommerce_myaccount_page_id') ); ?>" >
			
			<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_account_blue.png"  
 			alt="" class="MenuWC-link-icon">
 			Mon compte
			<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icons_dropdown_blue.png"  
 			alt="" class="MenuWC-link-icon MenuWC-link-icon--dropdown">
		</a>								
		<div class="Dropdown-menuContainer">
			<div class="Dropdown-menu">
				<?php get_template_part('parts/nav-my-account'); ?>
			</div>
		</div>
	</div>

<?php else : ?>

	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"  id="desktop-login-open" class="MenuWC">
		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_account_blue.png"  
 		alt="" class="MenuWC-link-icon">
 		Connexion
	</a>

<?php endif; ?>

<a href="<?php echo get_permalink( get_option('woocommerce_cart_page_id') ); ?>" id="desktop-cart-open" class="MenuWC">
	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_cart_blue.png"  
 	alt="" class="MenuWC-link-icon MenuWC-link-icon--cart">
 	Panier
	<?php get_template_part('parts/cart-count'); ?>
</a>