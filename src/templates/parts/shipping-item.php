<?php

    // vient de functions -> mv_show_shipping_item

    defined( 'ABSPATH' ) || exit;

    $id_retrait_locmine = 1;
    $for_retrait_locmine = "shipping_method_0_local_pickup1";

    $id_gratuit = 2;

    $id_colissimo_domicile = 5;
    $for_colissimo_domicile = "shipping_method_0_lpc_nosign";
    
    $id_colissimo_points_retrait = 7;
    $for_colissimo_points_retrait = "shipping_method_0_lpc_relay";

    $url_logo_shipping = "";
    $label_for = "";
    // ajout du délai de livraison
    $shipping_time = "";

    switch($args['id']) {
        case 1:
            $url_logo_shipping = get_template_directory_uri(). "/img/icons/icon-shop-blue@2x.png";
            $label_for = "shipping_method_0_local_pickup1";
            $shipping_time = "Sous 48h ouvrées";
            break;
        case 2:
            $url_logo_shipping = get_template_directory_uri(). "/img/icons/icon-shop-blue@2x.png";
            break;
        case 5:
            $url_logo_shipping = get_template_directory_uri(). "/img/logo-colissimo@2x.png";
            $label_for = "shipping_method_0_lpc_nosign";
            $shipping_time = "Sous 72h ouvrées";
            break;
        case 7:
            $url_logo_shipping = get_template_directory_uri(). "/img/logo-colissimo@2x.png";  
            $label_for = "shipping_method_0_lpc_relay";            
            $shipping_time = "Sous 72h ouvrées";          
            break;
        default: 
            $url_logo_shipping = get_template_directory_uri(). "/img/icons/icon-shop-blue@2x.png";
            $label_for = "shipping_method_0_local_pickup1";
    }

    $shippingName = $args['label'];
    $shippingFor = $args['for'];

    $shippingCost = $args['cost'];
    $shippingCostFloat = (float)$shippingCost;

    $shippingTax = $args['tax'];
    
    if( is_array($shippingTax) && !empty($shippingTax) ) {
        $shippingTTC = $shippingCostFloat + $shippingTax[12];
        $shippingTTCFormat = number_format((float)$shippingTTC, 2, ',', '');
    }

       
    $shippingCostWithDevise = "";

    if($shippingCost == "0.00") {
        $shippingCostWithDevise = "Gratuit";
    } else {
        $shippingCostWithDevise = $shippingTTCFormat . "€";
    }
    ?> 

     <label class="ShippingItem" for="<?php echo $label_for; ?>">
         <div class="ShippingItem-img">
             <img src="<?php echo($url_logo_shipping); ?>" alt="<?php echo $shippingName; ?>">
         </div>
         <div class="ShippingItem-name"><?php echo ($shippingName . " - " .$shippingCostWithDevise) . " TTC"; ?></div>
         <div class="ShippingItem-time"><?php echo ($shipping_time); ?></div>
     </label>
     

    <?php 