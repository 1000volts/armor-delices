<?php defined( 'ABSPATH' ) || exit; ?>

<div class="MenuBurger hidden" id="burger" style="display:none;">

    <div class="MenuBurger-panel">

    <button class="MenuBurger-close" id="burgerclose">
        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_close_white.png" alt="Fermer le menu">
    </button>

    <div class="MenuBurger-container">

        <nav class="MenuBurger-nav">

            <a href="<?php echo get_home_url(); ?>" title="Accueil" class="<?php echo active_menu_class(get_option('page_on_front')); ?>" >
	    		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_triskel_white.png" alt=""> 
                <span>Accueil<span>
	    	</a>

            <a href="<?php echo get_permalink( get_option('woocommerce_shop_page_id') ); ?>" title="Boutique" class="<?php echo active_menu_class(get_option('woocommerce_shop_page_id')); ?>">
	    		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_menu_shop_white.png" alt=""> 
                <span>Boutique<span>
	    	</a> 

            <a href="<?php echo wc_get_cart_url(); ?>" title="Panier" class="<?php echo active_menu_class(get_option('woocommerce_cart_page_id')); ?>">
	    		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_cart_white.png" alt=""> 
                <span>Panier<span>
	    	</a>
            
            <?php if(show_ecolodge()) : ?>
                <a href="https://www.jeu-bien-etre-armor-delices.fr/" target="_blank" rel="noreferrer noopener" 
                   title="Jeu" class="MenuBurger-nav-link--jeu">
	    	    	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_menu_contest_white.png" alt=""> 
                    <span>Jeu<span>
	    	    </a>
            <?php endif; ?>

            <a href="<?php echo get_permalink(1188); ?>" title="Recettes" class="<?php echo active_menu_class(1188); ?>">
	    		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_menu_recipe_white.png" alt=""> 
                <span>Recettes<span>
	    	</a>     

            <a href="<?php echo get_permalink(1184); ?>" title="Où nous trouver ?" class="<?php echo active_menu_class(1184); ?>">
	    		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_position_white.png" alt=""> 
                <span>Où nous trouver ?<span>
	    	</a>            

            <?php if ( is_user_logged_in() ) : ?>

                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Mon compte','woothemes'); ?>" 
                    class="<?php echo active_menu_class(get_option('woocommerce_myaccount_page_id')); ?>">
	    		    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_account_white.png" alt=""> 
                    <span>Mon compte<span>
	    		</a>

            <?php else : ?>

                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Connexion','woothemes'); ?>" 
                    class="<?php echo active_menu_class(get_option('woocommerce_myaccount_page_id')); ?>" id="mobile-burger-login-open">
	    			<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_account_white.png" alt="">
                    <span>Connexion<span>
	    		</a>

	    	<?php endif; ?>

            <div class="MenuBurger-nav-secondary">
                <?php get_template_part('parts/menu-secondary'); ?>
            </div>

        </nav>

    </div>

    </div>

    <div class="MenuBurger-mask" id="burgermask"></div>

</div>