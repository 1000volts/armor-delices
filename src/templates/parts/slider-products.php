<?php defined( 'ABSPATH' ) || exit; ?>

<?php 

    $productsListForSlider = get_field('products_slider_homepage'); 
    
?>



<?php if( $productsListForSlider ): ?>

<div class="ProductsSlider" data-aos="fade-up">

    <ul class="ProductsSlider-slides">

    <?php

        $args = array(
            'post_type' => 'product',
            'post__in' => $productsListForSlider
            );

        $loop = new WP_Query( $args );

        if ( $loop->have_posts() ) {
            while ( $loop->have_posts() ) : $loop->the_post();
                wc_get_template_part( 'content', 'product' );
            endwhile;
        } else {
            echo __( 'No products found' );
        }
        wp_reset_postdata();
        
    ?>

    </ul>

</div>

<?php endif; ?>