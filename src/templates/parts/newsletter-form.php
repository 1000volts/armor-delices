<?php defined( 'ABSPATH' ) || exit; ?>


<?php 

$args = wp_parse_args(
    $args,
    array(
        'class' => '',
    )
);

?>

<div class="Newsletter <?php if(!empty($args)) : ?> <?php echo $args['class']; ?> <?php endif; ?>">
    
    <div class="Newsletter-container">

        <div class="Newsletter-content">

            <span class="Newsletter-title">Newsletter</span>

            <?php echo do_shortcode("[sibwp_form id=1]"); ?>
        
        </div>

    </div>

</div>