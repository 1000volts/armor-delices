<?php defined( 'ABSPATH' ) || exit; ?>

<a href="<?php echo get_permalink(66); ?>" class="<?php echo active_menu_class(66); ?>">Notre histoire</a>
<a href="<?php echo get_permalink(68); ?>" class="<?php echo active_menu_class(68); ?>">Notre savoir-faire</a>
<a href="<?php echo get_permalink(70); ?>" class="<?php echo active_menu_class(70); ?>">Nos engagements</a>