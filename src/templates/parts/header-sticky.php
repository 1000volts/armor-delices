		<!-- headerSticky -->
		<div class="HeaderSticky" id="headerSticky" style="display: none;">

			<div class="HeaderSticky-container">

				<div class="HeaderSticky-logo">
					<!-- logo -->
					<div class="Logo">
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo_armor_delices.png" alt="Logo" class="logo-img">
						</a>
					</div>
					<!-- /logo -->
				</div>

				<div class="HeaderSticky-navPrimary">
					<!-- nav -->
					<nav class="Nav" role="navigation">
						<a href="<?php echo home_url(); ?> " class="MenuBigIcon MenuBigIcon--accueil <?php echo active_menu_class(get_option('page_on_front')); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_triskel_blue.png" alt="" class="MenuBigIcon-icon">
							<span class="MenuBigIcon-label">Accueil</span>
						</a>
                        <?php get_template_part('parts/menu-big-icon'); ?>
					</nav>
					<!-- /nav -->
				</div>

				<div class="HeaderSticky-navSecondary">
                    <nav>
						<a href="<?php echo get_permalink(1184); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_position_white.png" alt="">
						</a>

						<?php if ( is_user_logged_in() ) : ?>
							<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_account_white.png" alt="" >
							</a>
						<?php else : ?>
							<a href="#" class="desktop-sticky-login-open">
								<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_account_white.png" alt="" >
							</a>							
						<?php endif; ?>

						<a href="<?php echo wc_get_cart_url(); ?>" class="desktop-sticky-cart-open">
							<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_cart_white.png" alt="">
							<?php get_template_part('parts/cart-count'); ?>
						</a>

                    </nav>
				</div>

			</div>
		</div>
		<!-- /headerSticky -->