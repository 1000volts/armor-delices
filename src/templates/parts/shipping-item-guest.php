<?php     defined( 'ABSPATH' ) || exit; ?>

<?php

        $url_logo_shipping = "";
        $label_for = "";
        // ajout du délai de livraison
        $shipping_time = "";

        $delivery_zones = WC_Shipping_Zones::get_zones();

        ?> 
        
        
        
        <?php

        foreach ((array) $delivery_zones as $key => $the_zone) {
            
            if($the_zone['zone_name'] == "France") {

                

                ?> 
                <div class="mv_cart_shipping">

                    <h3 class="woocommerce-shipping-methods-title">Mode de livraison</h3>
                    <p class="mv_shipping_infos mv_shipping_infos--guest">Voici nos modes de livraison disponibles, vous pourrez en sélectionner un en continuant votre commande</p>
                    
                    <ul id="shipping_method"> 
                    
                
                <?php

                foreach ($the_zone['shipping_methods'] as $value) {

                    $value_enabled = $value->enabled;

                    if($value_enabled == "yes") {

                        switch($value->instance_id) {
                            case 1:
                                $url_logo_shipping = get_template_directory_uri(). "/img/icons/icon-shop-blue@2x.png";
                                $shipping_time = "Sous 48h ouvrées";
                                break;
                            case 2:
                                $url_logo_shipping = get_template_directory_uri(). "/img/icons/icon-shop-blue@2x.png";
                                break;
                            case 5:
                                $url_logo_shipping = get_template_directory_uri(). "/img/logo-colissimo@2x.png";
                                $shipping_time = "Sous 72h ouvrées";
                                break;
                            case 7:
                                $url_logo_shipping = get_template_directory_uri(). "/img/logo-colissimo@2x.png";  
                                $shipping_time = "Sous 72h ouvrées";          
                                break;
                            default: 
                                $url_logo_shipping = get_template_directory_uri(). "/img/icons/icon-shop-blue@2x.png";
                        }
                        
                        ?>

                            <li>
                                <label class="ShippingItem ShippingItem--guest">
                                    <div class="ShippingItem-img">
                                        <img src="<?php echo($url_logo_shipping); ?>" alt="<?php echo $shippingName; ?>">
                                    </div>
                                    <div class="ShippingItem-name"><?php echo ($value->title); ?></div>
                                    <div class="ShippingItem-time"><?php echo ($shipping_time); ?></div>
                                </label>
                            </li>
                            
                        <?php

                    }

                }
                ?> </ul></div> <?php
            }
        }

?>




