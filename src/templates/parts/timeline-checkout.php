<?php defined( 'ABSPATH' ) || exit; ?>

<ul class="TimelineCheckout">

    <li class="TimelineCheckout-item" id="timeline-item-cart">
        <span class="TimelineCheckout-num">1</span>
        <span class="TimelineCheckout-name">Récapitulatif</span>
    </li>

    <li class="TimelineCheckout-item" id="timeline-item-login">
        <span class="TimelineCheckout-num">2</span>
        <span class="TimelineCheckout-name">Connexion</span>
    </li>

    <li class="TimelineCheckout-item" id="timeline-item-addresses">
        <span class="TimelineCheckout-num">3</span>
        <span class="TimelineCheckout-name">Adresse de livraison</span>
    </li>

    <li class="TimelineCheckout-item" id="timeline-item-payment">
        <span class="TimelineCheckout-num">4</span>
        <span class="TimelineCheckout-name">Paiement</span>
    </li>

    <li class="TimelineCheckout-item" id="timeline-item-confirm">
        <span class="TimelineCheckout-num">5</span>
        <span class="TimelineCheckout-name">Confirmation</span>
    </li>

</ul>