<?php defined( 'ABSPATH' ) || exit; ?>

<?php /*
<a href="#" class="MenuBigIcon">
	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_menu_blog_blue.png" alt="Logo" class="MenuBigIcon-icon">
	<span class="MenuBigIcon-label">Blog</span>
</a>
*/ ?>

<a href="<?php echo get_permalink(1188); ?>" class="MenuBigIcon MenuBigIcon--recettes <?php echo active_menu_class(1188); ?>">
	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_menu_recipe_blue.png" alt="" class="MenuBigIcon-icon">
	<span class="MenuBigIcon-label">Recettes</span>
</a>

<a href="<?php echo get_permalink( get_option('woocommerce_shop_page_id') ); ?>" class="MenuBigIcon MenuBigIcon--shop <?php echo active_menu_class( get_option('woocommerce_shop_page_id') ); ?>">
	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_menu_shop_blue.png" alt="" class="MenuBigIcon-icon">
	<span class="MenuBigIcon-label">Boutique</span>
</a>

<?php if(show_ecolodge()) : ?>
<a href="https://www.jeu-bien-etre-armor-delices.fr/" target="_blank" rel="noreferrer noopener"
   class="MenuBigIcon MenuBigIcon--jeu">
	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/icon_menu_contest_blue.png" alt="" class="MenuBigIcon-icon">
	<span class="MenuBigIcon-label">Jeu</span>
</a>
<?php endif; ?>