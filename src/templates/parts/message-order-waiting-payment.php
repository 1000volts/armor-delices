<?php defined( 'ABSPATH' ) || exit; ?>


<?php 
    
    $order_url = "";

    if ( $args['order_url'] ) {
        $order_url = $args['order_url'];
    }

?>

<div class="MessageOrderWaitingPayment <?php if(is_front_page()) : ?>MessageOrderWaitingPayment--frontpage<?php endif; ?>">

    <div class="MessageOrderWaitingPayment-titleContainer">
        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/alert_cart.png" alt="" class="MessageOrderWaitingPayment-icon">
        <span class="MessageOrderWaitingPayment-title">Commande en attente de paiement</span>    
    </div>

    <p class="MessageOrderWaitingPayment-info">Vous pouvez reprendre votre dernière commande en attente de paiement en cliquant <a href="<?php echo esc_url($order_url); ?>">ici</a>.</p>
    <p class="MessageOrderWaitingPayment-sav">
        Si vous avez rencontré un problème technique lors du paiement et que le problème persiste,
        <a href="<?php echo esc_url(link_to_contact_form_with_url_parameter()); ?>">contactez-nous</a>
    </p>

</div>