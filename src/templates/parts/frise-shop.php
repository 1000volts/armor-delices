<?php defined( 'ABSPATH' ) || exit; ?>


<?php if(is_shop()) : ?>
    
    <section class="FriseShop">
        <img src="<?php echo get_template_directory_uri(); ?>/img/boutique/bandeau_catalogue_armor.png" alt="La gamme de produit de la boutique Armor Délices"
            srcset="<?php echo get_template_directory_uri(); ?>/img/boutique/bandeau_catalogue_armor@2x.png 2x">
    </section>

<?php endif; ?>