<?php

use WPO\WC\PDF_Invoices_Pro\Vendor\Illuminate\Support\Arr;

 defined( 'ABSPATH' ) || exit; ?>

<?php 
    // on recupère les custom field de ACF
    $recette_resume = get_field('recette_resume');
    $recette_video = get_field('recette_video');
    $recette_informations_complementaires = get_field('recette_informations_complementaires');

    $class_video = "";
    $attr_video = array();

    if( $recette_video ) {    
        $attr_video =  array( 'src' => esc_url( $recette_video ));        
    } elseif( !has_post_thumbnail() ) {
        $class_video = "RecetteDetails--noMedias";
    }


    //paramètre args pour savoir si on affiche le titre la recette du moment (par defaut non)
    $array_arg_default = array(
        'show_header' => false
    );

    $args = wp_parse_args( $args, $array_arg_default );

?>

<article class="RecetteDetails <?php echo $class_video; ?>">

    <div class="RecetteDetails-contentWrapper"  data-aos="fade-up">

        <?php if( has_post_thumbnail() ) : ?>
            <div class="RecetteDetails-imageCircle" data-aos="zoom-in" data-aos-delay="500">
                <?php the_post_thumbnail( 'full' ); ?>
            </div>
        <?php endif; ?>

        <?php if( $args['show_header'] ) : ?>            
            <img src="<?php echo get_template_directory_uri(); ?>/img/recette_beurre@2x.png" alt="" class="RecetteDetails-illusHeader">
            <span class="RecetteDetails-title">La recette du moment</span>
        <?php endif; ?>

        <div class="RecetteDetails-infos">

            <h2 class="RecetteDetails-name"><?php the_title(); ?></h2>

            <?php if( have_rows('recette_etapes') ): // boucle repeater etape de la recette ?>

                <?php $recette_etapes_num = 0; ?>

                <?php while ( have_rows('recette_etapes') ) : the_row(); ?>

                    <?php 
                        $recette_etapes_txt = get_sub_field('recette_etapes_txt'); 
                        $recette_etapes_num++;
                    ?>

                    <div class="RecetteDetails-etape">                    
                        <span class="RecetteDetails-etape-num">
                            <span class="RecetteDetails-etape-num-value"><?php echo $recette_etapes_num; ?></span>
                        </span>
                        <div class="RecetteDetails-etape-txt">
                            <?php echo $recette_etapes_txt; ?>
                        </div>
                    </div>

                <?php endwhile; ?>

            <?php endif; ?>

            <?php if($recette_informations_complementaires) : // infos complémentaires ?>

                <span class="RecetteDetails-infosComplementaires"><?php echo $recette_informations_complementaires; ?></span>
            
            <?php endif; ?>

            <span class="RecetteDetails-signature" data-aos="fade-up">Bonne dégustation !</span>
        </div>

    </div>
    
    <?php // video ?>
    <?php if( $recette_video ) : ?>

        <div class="RecetteDetails-videoWrapper">
            <img src="<?php echo get_template_directory_uri(); ?>/img/paper_clip.png" alt="" class="RecetteDetails-paperclip">
            <?php echo wp_video_shortcode( $attr_video ); ?>
        </div>
        <?php // fin video ?>

    <?php elseif( has_post_thumbnail() ) : ?>

        <div class="RecetteDetails-imageWrapper">
            <img src="<?php echo get_template_directory_uri(); ?>/img/paper_clip.png" alt="" class="RecetteDetails-paperclip">
            <div class="RecetteDetails-imageWrapper-img"><?php the_post_thumbnail( 'full' ); ?></div>
        </div>

    <?php endif; ?>

</article>