<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

// empeche le redimensionnement des images
add_filter( 'big_image_size_threshold', '__return_false' );

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

add_filter( 'intermediate_image_sizes_advanced', 'prefix_remove_default_images' );
// This will remove the default image sizes medium and large. 
function prefix_remove_default_images( $sizes ) {
 unset( $sizes['small']); // 100px
 unset( $sizes['medium']); // 300px
 unset( $sizes['large']); // 1024px
 return $sizes;
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('aos', get_template_directory_uri() . '/js/lib/aos-master/dist/aos.js'); // Conditional script(s)
        wp_enqueue_script('aos'); // Enqueue it!

    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    wp_register_script('slick', get_template_directory_uri() . '/js/lib/slick/slick.min.js', array('jquery'), '1.0.0'); // Conditional script(s)
    wp_enqueue_script('slick'); // Enqueue it!
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('slick', get_template_directory_uri() . '/js/lib/slick/slick.css', array(), '1.0', 'all');
    wp_enqueue_style('slick'); // Enqueue it!

    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!

    wp_register_style('aos', get_template_directory_uri() . '/js/lib/aos-master/dist/aos.css');
    wp_enqueue_style('aos'); // Enqueue it!


    wp_register_style('typekit', 'https://use.typekit.net/rzb2uvd.css', false);
    wp_enqueue_style('typekit'); // Enqueue it!


}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}


// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;

    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether


/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Creation des custom post types
function create_post_type_html5()
{
    register_post_type('postrecette', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Recettes', 'html5blank'), // Rename these to suit
            'singular_name' => __('Recette', 'html5blank'),
            'add_new' => __('Ajouter une recette', 'html5blank'),
            'add_new_item' => __('Ajouter', 'html5blank'),
            'edit' => __('Editer', 'html5blank'),
            'edit_item' => __('Editer', 'html5blank'),
            'new_item' => __('Nouveau', 'html5blank'),
            'view' => __('Voir', 'html5blank'),
            'view_item' => __('Voir', 'html5blank'),
            'search_items' => __('Rechercher', 'html5blank'),
            'not_found' => __('Introuvable', 'html5blank'),
            'not_found_in_trash' => __('Introuvable', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
        'rewrite' => array('slug' => 'recettes'),
        
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
    ));
}



/*------------------------------------*\
	ACF
\*------------------------------------*/

// Ajoute une page d'options sur le site pour des champs ACF globaux 

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}



/*------------------------------------*\
	YOAST
\*------------------------------------*/

/**
 * Conditionally Override Yoast SEO Breadcrumb Trail
 * https://plugins.svn.wordpress.org/wordpress-seo/trunk/frontend/class-breadcrumbs.php
 * -----------------------------------------------------------------------------------
 * Further amended by Kent House to set custom landing page for 'project' custom post type
 * and landing page for normal posts to a manually created 'blog' page 
*/

add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

function wpse_100012_override_yoast_breadcrumb_trail( $links ) {
    
    if ( is_singular('postrecette') ) {

        $breadcrumb[] = array(
        'url' => get_permalink( 1188 ),
        'text' => get_the_title( 1188 ),
        );

        array_splice( $links, 1, -2, $breadcrumb );

    }
    return $links;
}


/*------------------------------------*\
	ADMINISTRATION PERMISSIONS & VISIBILITE
\*------------------------------------*/

add_action( 'admin_menu', 'remove_menu_pages', 10000);

function remove_menu_pages() {
    global $current_user;
     
    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);

    if($user_role != "administrator") {
      $remove_submenu = remove_submenu_page('woocommerce', 'wc-settings');
      $remove_submenu = remove_submenu_page('woocommerce', 'wc-addons');
      $remove_submenu = remove_submenu_page('woocommerce', 'wc-status');
      $remove_submenu = remove_submenu_page('woocommerce', 'wpo_wcpdf_options_page');
      $remove_submenu = remove_submenu_page('woocommerce', 'wc-reports');
      $remove_submenu = remove_submenu_page('edit.php?post_type=product', 'edit-tags.php?taxonomy=product_tag&amp;post_type=product' );
      $remove_submenu = remove_submenu_page('edit.php?post_type=product', 'edit-tags.php?taxonomy=product_cat&amp;post_type=product' );
      $remove_submenu = remove_submenu_page('edit.php?post_type=product', 'product_attributes');
      $remove_submenu = remove_menu_page('edit.php');
      $remove_submenu = remove_menu_page('edit-comments.php');
      $remove_submenu = remove_menu_page('wpcf7');
      $remove_submenu = remove_menu_page( 'wpseo_dashboard' );
      $remove_submenu = remove_menu_page( 'wpseo_workouts' );
      $remove_submenu = remove_menu_page('themes.php');
      $remove_submenu = remove_menu_page('tools.php');      
      $remove_submenu = remove_menu_page('itsec');      
      $remove_submenu = remove_menu_page('itsec-dashboard');      
      $remove_submenu = remove_submenu_page('itsec', 'itsec-dashboard');    
      $remove_submenu = remove_submenu_page('metaslider', 'metaslider-settings');    
      $remove_submenu = remove_submenu_page('metaslider', 'metaslider-theme-editor');    
      $remove_submenu = remove_submenu_page( 'admin.php', 'itsec' );
      $remove_submenu = remove_submenu_page( 'admin.php', 'toplevel_page_itsec' );
      $remove_submenu = remove_submenu_page( 'admin.php', 'itsec-logs' );
      $remove_submenu = remove_submenu_page('post-new.php?post_type=popup', 'product_attributes');
      $remove_submenu = remove_submenu_page('themes.php', 'edit.php?post_type=popup_theme');
      $remove_submenu = remove_submenu_page('edit.php?post_type=popup', 'edit.php?post_type=popup_theme');
      $remove_submenu = remove_submenu_page('edit.php?post_type=popup', 'pum-subscribers');
      $remove_submenu = remove_submenu_page('edit.php?post_type=popup', 'pum-extensions');
      $remove_submenu = remove_submenu_page('edit.php?post_type=popup', 'pum-settings');
      $remove_submenu = remove_submenu_page('edit.php?post_type=popup', 'pum-tools');
      $remove_submenu = remove_submenu_page('edit.php?post_type=popup', 'pum-support');
    }
  }


//redirection de certaines pages d'admin
add_action('admin_init', 'admin_redirects');

  function admin_redirects() {
    global $pagenow;

    //popupmaker
    if($pagenow == 'edit.php' 
    && isset($_GET['post_type']) 
    && isset($_GET['page']) 
    && $_GET['post_type'] == 'popup' 
    && $_GET['page'] == 'pum-support')        
        {
        wp_redirect(admin_url('edit.php?post_type=popup', 'http'), 301);
        exit;
    }
    //popupmaker
    if($pagenow == 'edit.php' 
    && isset($_GET['post_type']) 
    && isset($_GET['page']) 
    && isset($_GET['view']) 
    && $_GET['post_type'] == 'popup' 
    && $_GET['page'] == 'pum-extensions'        
    && $_GET['view'] == 'integrations')        
        {
        wp_redirect(admin_url('edit.php?post_type=popup', 'http'), 301);
        exit;
    }
    //popupmaker
    if($pagenow == 'edit.php' 
    && isset($_GET['post_type']) 
    && $_GET['post_type'] == 'popup_theme')
        {
        wp_redirect(admin_url('edit.php?post_type=popup', 'http'), 301);
        exit;
    }
}


//masque des liens & onglet de popupmaker
add_action('admin_head', 'hide_popupmaker_links_css');

function hide_popupmaker_links_css() {
  echo '<style>
    a[href="'.get_admin_url().'post-new.php?post_type=popup_theme"] {
        display: none;
    }
    a[href="'.get_admin_url().'edit.php?post_type=popup_theme"].nav-tab {
        display: none;
    }
    a[href="'.get_admin_url().'edit.php?post_type=popup&page=pum-extensions&view=integrations"].nav-tab {
        display: none;
    }
    .pum-notice-bar-wrapper {
        display: none;
    }
    .pum-field #edit_theme_link {
        display: none;
    }
    .pum-upgrade-tip {
        display: none;
    }
  </style>';
}



  // iThemes Security Pro - replace virtual user capability 'itsec_manage' with the really used 'manage_options'
add_filter('itsec_cap_required', 'replace_itsec_cap', 10, 1 );
function replace_itsec_cap( $itsec_cap ) {
    $itsec_cap = is_multisite() ? 'manage_network_options' : 'manage_options';
 
    return $itsec_cap;
}


//  // supprime le menu marketing de l'admin
//  add_filter( 'woocommerce_admin_features', function( $features ) {
//    /**
//     * Filter list of features and remove those not needed     *
//     */
//    return array_values(
//        array_filter( $features, function($feature) {
//            return $feature !== 'marketing';
//        } ) 
//    );
//} );

  add_action( 'admin_menu', 'misha_hide_product_tags_metabox' );
// masque le product tag et la categorie sur une page produit
function misha_hide_product_tags_metabox() {
    remove_meta_box( 'product_catdiv','product','side' ); // Categories Metabox
	remove_meta_box( 'tagsdiv-product_tag', 'product', 'side' );
}

// masque la colonne product tag sur la liste produit
add_filter('manage_product_posts_columns', 'misha_hide_product_tags_column', 999 );
function misha_hide_product_tags_column( $product_columns ) {
	unset( $product_columns['product_cat'] );
	unset( $product_columns['product_tag'] );
	return $product_columns;
}

// masque le product tag sur l'édition rapide d'un produit
add_filter( 'quick_edit_show_taxonomy', 'misha_hide_product_tags_quick_edit', 10, 2 );
function misha_hide_product_tags_quick_edit( $show, $taxonomy_name ) {
    if ( 'product_tag' == $taxonomy_name )
        $show = false;

    if ( 'product_cat' == $taxonomy_name )
        $show = false;

    return $show;
}

// Supprime des blocs du dashboard d'accueil de l'admin
function disable_default_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	// yoast seo
    remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal' );
}
add_action('wp_dashboard_setup', 'disable_default_dashboard_widgets', 999);


// Autorise le shop_manager d'éditer la page de politique de confidentialité
add_action('map_meta_cap', 'custom_manage_privacy_options', 1, 4);
function custom_manage_privacy_options($caps, $cap, $user_id, $args)
{
  if (!is_user_logged_in()) return $caps;

  $user_meta = get_userdata($user_id);
  if (array_intersect(['shop_manager', 'administrator'], $user_meta->roles)) {
    if ('manage_privacy_options' === $cap) {
      $manage_name = is_multisite() ? 'manage_network' : 'manage_options';
      $caps = array_diff($caps, [ $manage_name ]);
    }
  }
  return $caps;
}



/*------------------------------------*\
	GUTENBERG
\*------------------------------------*/

add_filter( 'allowed_block_types_all', 'mv_allow_2_woo_blocks' );

// types de bloc dispo dans gutenberg
function mv_allow_2_woo_blocks( $allowed_blocks ){

	$allowed_blocks = array(
		'core/paragraph',
		'core/image',
		'core/heading',
		'core/gallery',
		'core/list',
		'core/quote',
		'core/file',
		'core/video',
		'core/table',
		'core/pullquote',
		'core/buttons',
		'core/columns',
		'core/separator',
		'core/spacer',
        'pb/accordion-item',
	);

	return $allowed_blocks;
	
}


// Customisation de la palette de couleur Gutenberg et supprime l'option couleur personnalisée
function mytheme_setup_theme_supported_features() {

	add_theme_support( 'editor-color-palette',
		array(
			array( 'name' => 'bleu', 'slug'  => 'blue', 'color' => '#0d5aa2' ),
			array( 'name' => 'noir', 'slug'  => 'black', 'color' => '#3b3b3b' ),
			array( 'name' => 'jaune', 'slug'  => 'yellow', 'color' => '#eab80f' ),
			array( 'name' => 'vert', 'slug'  => 'green', 'color' => '#6dc499' ),
			array( 'name' => 'rouge', 'slug'  => 'red', 'color' => '#e7405c' ),
		)
	);
    add_theme_support( 'disable-custom-colors' );
}
add_action( 'after_setup_theme', 'mytheme_setup_theme_supported_features' );


// Desactive Gutenberg pour les recettes
add_filter('use_block_editor_for_post_type', 'prefix_disable_gutenberg', 10, 2);
function prefix_disable_gutenberg($current_status, $post_type)
{
    // Use your post type key instead of 'product'
    if ($post_type === 'postrecette') return false;
    return $current_status;
}

// Desactive l'éditeur classique sur les recettes
add_action( 'init', function() {remove_post_type_support( 'postrecette', 'editor' ); }, 99);


/*------------------------------------*\
	UTILS
\*------------------------------------*/


function get_id_by_slug($slug) {
    $page_url_id = get_page_by_path( $slug );
    return $page_url_id;
}


// Fait un lien vers le formulaire de contact avec un paramètre pour sélectionner l'objet Problème technique

function link_to_contact_form_with_url_parameter() {

    $id_page_contact_boutique = 76;

    $url_contact_obj_probleme = add_query_arg('type', 'Problème technique', get_permalink($id_page_contact_boutique));

    return $url_contact_obj_probleme;

}


function active_menu_class( $link_id ) {

    $class = 'activ';
    $current_page_id = 0;

    if( $link_id ) {

        if( get_post_type() === 'page' ) {

            $current_page_id = get_queried_object_id();

            if ( $link_id == $current_page_id ) {
                return $class;				
            }

        } elseif ( is_shop() ) {

            $current_page_id = get_option( 'woocommerce_shop_page_id' );

            if ( $link_id == $current_page_id ) {
                return $class;				
            }

        } elseif ( is_product() ) {

            // si c'est une fiche produit on activ le menu boutique
            $current_page_id = get_option( 'woocommerce_shop_page_id' );

            if ( $link_id == $current_page_id ) {
                return $class;				
            }

        } elseif ( is_singular('postrecette') ) {

            // si c'est une recette on declare la page categorie Recette comme page active
            $current_page_id = 1188;

            if ( $link_id == $current_page_id ) {
                return $class;				
            }

        } elseif ( get_option('woocommerce_myaccount_page_id') ) {

            $current_page_id = get_option('woocommerce_myaccount_page_id');

            if ( $link_id == $current_page_id ) {
                return $class;				
            }

        }

    }

}


// array avec les url des images de madeleines présentent dans le header du site

function load_random_madeleine_image_url() {

    $array_madeleine_url = array(
		'/img/madeleines_header/mad_header_cara.png',
		'/img/madeleines_header/mad_header_choco_sarrasin.png',
		'/img/madeleines_header/mad_header_nat.png',
	);

    $random_madeleine = $array_madeleine_url[array_rand($array_madeleine_url)];

    return $random_madeleine;
}


// active les éléments pour l'opération eco-lodge

function show_ecolodge() {
    return false;
}


require_once(get_template_directory() . '/functions-woocommerce.php');

require_once(get_template_directory() . '/functions-email.php');


// Desactive le lazyload sur la homepage (casse le caroussel sinon)

function deactivate_on_front_page() {
	if ( is_front_page() ) {
		add_filter( 'do_rocket_lazyload', '__return_false' );
	}
}
add_filter( 'wp', __NAMESPACE__ . '\deactivate_on_front_page' );


// change le label du bouton mettre à jour le panier

function change_update_cart_text( $translated, $text, $domain ) {

    if( is_cart() && $translated == 'Mettre à jour le panier' ){
        $translated = 'Mettre à jour les quantités';
    }
    return $translated;
    }

add_filter( 'gettext', 'change_update_cart_text', 20, 3 );


/*------------------------------------*\
	GMAP (store locator)
\*------------------------------------*/


// change le dossier pour les marker

add_filter( 'wpsl_admin_marker_dir', 'custom_admin_marker_dir' );

function custom_admin_marker_dir() {

    $admin_marker_dir = get_stylesheet_directory() . '/img/wpsl-markers/';
    
    return $admin_marker_dir;
}


// change le dossier pour les marker

define( 'WPSL_MARKER_URI', dirname( get_bloginfo( 'stylesheet_url') ) . '/img/wpsl-markers/' );


// Ajoute un custom marker selon la catégorie de la boutique

add_filter( 'wpsl_store_meta', 'custom_store_meta', 10, 2 );

function custom_store_meta( $store_meta, $store_id ) {

        $terms = wp_get_post_terms( $store_id, 'wpsl_store_category' );

        if ( $terms ) {
            if ( !is_wp_error( $terms ) ) {
                if ( isset( $_GET['filter'] ) && $_GET['filter'] ) {
                    $filter_ids = explode( ',', $_GET['filter'] );

                    foreach ( $terms as $term ) {
                        if ( in_array( $term->term_id, $filter_ids ) ) {
                            $cat_marker = get_field( 'gmap_marker', $term );

                            if ( $cat_marker ) {
                                $store_meta['categoryMarkerUrl'] = $cat_marker;
                            }
                        }
                    }
                } else {
                    $store_meta['categoryMarkerUrl'] = get_field( 'gmap_marker', $terms[0] );
                }
            }
        } else {
            // si la boutique n'est pas liée à une catégorie on l'ajoute à la volée dans celle des magasins pour avoir un marker
            wp_set_object_terms( $store_id, 'magasin-grandes-surfaces', 'wpsl_store_category');
        }

    return $store_meta;
}


// Modifie les infos par défaut des marker

add_filter( 'wpsl_marker_props', 'custom_marker_props' );

function custom_marker_props( $marker_props ) {
            
    $marker_props['scaledSize'] = '33,48'; // Set this to 50% of the original size
    $marker_props['origin'] = '0,0';
    $marker_props['anchor'] = '12,35';
    
    return $marker_props;
}

// Masque le nom de la rue si il est simillaire au nom du magasin (utile pour grande surface)

add_filter( 'wpsl_listing_template', 'custom_listing_template' );

function custom_listing_template( $listing_template ) {
            
    $listing_template_ret = str_replace('<span class="wpsl-street"><%= address %></span>', '<% if ( address != store ) { %><span class="wpsl-street"><%= address %></span><% } %>', $listing_template);
    
    return $listing_template_ret;
}


// Masque le nom de la rue si il est simillaire au nom du magasin dans popin détail (utile pour grande surface)

add_filter( 'wpsl_info_window_template', 'custom_info_window_template' );

function custom_info_window_template( $info_window_template ) {
            
    $info_window_template_ret = str_replace('<span><%= address %></span>', '<% if ( address != store ) { %><span><%= address %></span><% } %>', $info_window_template);
    
    return $info_window_template_ret;
}


/*------------------------------------*\
	PAGINATION
\*------------------------------------*/


// Corrige la pagination pour les types d'url /%category%/%postname%/ (evite 404 sur page 2)

add_action('init', function() { // Permet de gérer la pagination pour la page projets https://www.grzegorowski.com/wordpress-rewrite-rules
    add_rewrite_rule('(.?.+?)/page/?([0-9]{1,})/?$', 'index.php?pagename=$matches[1]&paged=$matches[2]', 'top');
  });


