<?php

/*
 * Template Name: Template savoir faire
 */

$subtitlePage = get_field("subtitle_page");

$step1Title = get_field("savoir_faire_section_1_title");
$step1Txt = get_field("savoir_faire_section_1_txt");

$step2Title = get_field("savoir_faire_section_2_title");
$step2Txt = get_field("savoir_faire_section_2_txt");

$step3Title = get_field("savoir_faire_section_3_title");
$step3Txt = get_field("savoir_faire_section_3_txt");


?>


<?php get_header(); ?>

	<main role="main" id="Main" class="TemplateSavoirFaire">


		<!-- section -->
		<section>

            <div class="PageTitle">
                <h1 class="page-title"><?php the_title();?></h1>
                <?php if($subtitlePage) : ?>
                    <p class="PageTitle-txt"><?php echo $subtitlePage; ?></p>
                <?php endif; ?>
            </div>

            <div class="TemplateSavoirFaire-section1">
                <div class="Container">
                    <div class="Row">
                        <div class="Col Col-6 Col-sm-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/savoir-faire/savoir_montage.png" 
                                 srcset="<?php echo get_template_directory_uri(); ?>/img/savoir-faire/savoir_montage@2x.png" alt="" data-aos="fade-right">
                        </div>

                        <div class="Col Col-6 Col-sm-12">
                            <div class="TemplateSavoirFaire-content Gutenberg" data-aos="fade-left">
                                <h2><?php echo $step1Title; ?></h2>
                                <?php if($step1Txt): ?>
                                    <?php echo $step1Txt; ?>
                                <?php endif; ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/savoir-faire/savoir_illus.png" 
                                     srcset="<?php echo get_template_directory_uri(); ?>/img/savoir-faire/savoir_illus@2x.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="TemplateSavoirFaire-section2 FullWidthBlueBG">

                <div class="Container">
                    <div class="Row TemplateSavoirFaire-section2-bloc TemplateSavoirFaire-section2-bloc--1">
                        <div class="Col Col-6 Col-sm-12">
                            <div class="TemplateSavoirFaire-content TemplateSavoirFaire-content--gout Gutenberg" data-aos="fade-right">
                                <?php if($step2Title) : ?>
                                    <h2><?php echo $step2Title; ?></h2>
                                <?php endif; ?>
                                <?php if($step2Txt): ?>
                                    <?php echo $step2Txt; ?>
                                <?php endif; ?>
                                <div class="TemplateSavoirFaire-content-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/illus_conservateur_white.png" 
                                     srcset="<?php echo get_template_directory_uri(); ?>/img/illus_conservateur_white@2x.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="Col Col-6 Col-sm-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/savoir-faire/savoir_montage2.png" 
                                 srcset="<?php echo get_template_directory_uri(); ?>/img/savoir-faire/savoir_montage2@2x.png" alt="" data-aos="fade-left">
                        </div>
                    </div>
                    <div class="Row TemplateSavoirFaire-section2-bloc TemplateSavoirFaire-section2-bloc--2">
                        <div class="Col Col-6 Col-sm-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/savoir-faire/savoir_montage3.png" 
                                 srcset="<?php echo get_template_directory_uri(); ?>/img/savoir-faire/savoir_montage3@2x.png" alt="" data-aos="fade-right">
                        </div>
                        <div class="Col Col-6 Col-sm-12">
                            <div class="TemplateSavoirFaire-content TemplateSavoirFaire-content--gout Gutenberg"  data-aos="fade-left">
                                <?php if($step3Title) : ?>
                                    <h2><?php echo $step3Title; ?></h2>
                                <?php endif; ?>
                                <?php if($step3Txt): ?>
                                    <?php echo $step3Txt; ?>
                                <?php endif; ?>
                                <div class="TemplateSavoirFaire-content-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/savoir-faire/plusdebeurre@2x.png" alt="Plus de beurre que de mal">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="TemplateSavoirFaire-backtohome" data-aos="fade-up">
                        <a href="<?php echo (get_permalink(64)); ?>" class="Button Button--white">retour à l’accueil</a>
                    </div>
                </div>

            </div>




		</section>
		<!-- /section -->

	</main>

<?php get_footer(); ?>
