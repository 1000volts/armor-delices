<?php

/*
 * Template Name: Template Woocommerce
 */

?>

<?php get_header(); ?>

<div id="primary" class="content-area">

    <main role="main" id="Main">

        <!-- Container -->
        <div class="Container">

            <!-- section -->
            <section>

            <div class="PageTitle">
                <h1 class="page-title"><?php the_title(); ?></h1>
            </div>


                <?php 
                // Affiche la timeline dans le panier et le checkout
                if(is_checkout() || is_page( 'cart' ) || is_cart()) { get_template_part("parts/timeline-checkout"); } 
                ?>


                <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <!-- contenu -->
                <section id="post-<?php the_ID(); ?>" class="PageContent">

                    <?php the_content(); ?>

                </section>
                <!-- /contenu -->

                <?php endwhile; ?>

                <?php else: ?>

                <?php endif; ?>


            </section>
            <!-- /section -->

        </div>
        <!-- /Container -->

    </main>

</div>

<?php get_footer(); ?>
