<?php
/**
 * Totals
 *
 * This template can be overridden by copying it to yourtheme/templates/side-cart-woocommerce/global/footer/totals.php.
 *
 * HOWEVER, on occasion we will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen.
 * @see     https://docs.xootix.com/side-cart-woocommerce/
 * @version 3.0
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

extract( Xoo_Wsc_Template_Args::footer_totals() );

?>

<?php if( WC()->cart->is_empty() ) return; ?>

<div class="xoo-wsc-ft-totals">

	<?php 

		$subtotal = WC()->cart->subtotal_ex_tax;
		$subtotal_format = number_format((float)$subtotal, 2, ',', '') . "€";

		$subtotal_tax = WC()->cart->get_subtotal_tax();
		$subtotal_tax_format = number_format((float)$subtotal_tax, 2, ',', '') . "€";		

	?>

	<?php // lien code promo = ajout MV ?>
	<div class="xoo-wsc-ft-amt xoo-wsc-ft-amt-coupon">
		<a href="<?php echo wc_get_cart_url(); ?>" title="Panier">Vous avez un code promo ?<span></span></a>
	</div>
	
	<div class="xoo-wsc-ft-amt xoo-wsc-ft-amt-subtotal">
		<span class="xoo-wsc-ft-amt-label">Sous-total</span>
		<span class="xoo-wsc-ft-amt-value"><?php echo $subtotal_format; ?></span>
	</div>

	<div class="xoo-wsc-ft-amt xoo-wsc-ft-amt-vat">
		<span class="xoo-wsc-ft-amt-label">TVA</span>
		<span class="xoo-wsc-ft-amt-value"><?php echo $subtotal_tax_format; ?></span>
	</div>

	<div class="xoo-wsc-ft-amt xoo-wsc-ft-amt-total">
		<span class="xoo-wsc-ft-amt-label">Total</span>
		<span class="xoo-wsc-ft-amt-value"><?php echo $totals["subtotal"]["value"]; ?></span>
	</div>

	<?php do_action( 'xoo_wsc_totals_end' ); ?>

</div>