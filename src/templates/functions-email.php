<?php

// Commande en cours
add_action('mv_mail_processing_order', 'mv_mail_content_processing', 10, 1);
function mv_mail_content_processing($order) {
    ?>
    <p>Bonjour <?php echo (esc_html( $order->get_billing_first_name() )) ; ?>
    <p>Nous avons bien réceptionné votre commande n°<?php echo (esc_html( $order->get_order_number())); ?>.</p>
    <p>Elle est désormais en cours de préparation.</p>
    <p><strong>Vous avez choisi la livraison à domicile ou en point relais ?</strong> Votre commande sera expédiée par notre équipe sous 72h ouvrées.</p>
    <p>Vous pouvez vérifier le statut de vos commandes dans la rubrique <a href="<?php echo wc_get_account_endpoint_url( 'orders' );?>">mes commandes.</a></p>
    <p>Trugarez Vras !</p>
    <?php
}

add_action('mv_mail_on_hold_order', 'mv_mail_content_processing', 10, 1);
// Commande en attente de paiement
function mv_mail_content_on_hold($order) {
    ?>
    <p>Bonjour <?php echo (esc_html( $order->get_billing_first_name() )) ; ?>
    <p>Nous vous informons que votre commande est en attente de paiement pour être validée.</p>
    <p>Vous pouvez reprendre le cours de votre commande dans la rubrique MES COMMANDES.</p>
    <p>Trugarez Vras !</p>
    <?php
}

add_action('mv_mail_refunded_order', 'mv_mail_content_refunded', 10, 1);
// Commande remboursée
function mv_mail_content_refunded($order) {
    ?>
    <p>Bonjour <?php echo (esc_html( $order->get_billing_first_name() )) ; ?>
    <p>Nous vous confirmons le remboursement concernant votre commande n°<?php echo (esc_html( $order->get_order_number())); ?></p>
    <p>Ce remboursement sera effectif sous 14 jours.</p>
    <p>Nous vous invitons à consulter le détail ci-dessous :</p>
    <?php
}




?>