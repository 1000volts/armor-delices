<?php get_header(); ?>

	<main id="Main" role="main" class="TemplateRecetteDetail">

    <section>
		
        <div class="PageTitle">
            <h1 class="page-title">Nos recettes</h1>
        </div>  

        <div class="Container">

            <?php get_template_part('parts/recette-details'); // détail de la recette ?>

            <?php 
                $args_query_random_recette = array(
                    'post_type' => 'postrecette',
                    'orderby' => 'rand',
                    'posts_per_page' => 3,
                );

                $query_recettes_random = new WP_Query( $args_query_random_recette ); ?>
                
                <?php if ($query_recettes_random->have_posts()): // affichage liste recettes ?>
                
                    <section class="RecettesList RecettesList--single">

                        <div class="PageTitle">
                            <h2 class="page-title">Plus de recettes</h2>
                        </div>  

                        <div class="Row">

                            <?php $transition_delay_recette = 100; ?>

                            <?php while ($query_recettes_random->have_posts()) : $query_recettes_random->the_post(); ?>
                                <div class="RecettesList-item Col Col-4 Col-sm-6 Col-xs-12" data-aos="fade-up" data-aos-delay="<?php echo $transition_delay_recette; ?>">
                                    <?php get_template_part('parts/recette-item'); ?>
                                </div>
                                <?php $transition_delay_recette+=100; ?>
                            <?php endwhile; ?>                        

                        </div>

                    </section>                

                    <?php wp_reset_postdata(); ?>                
                
                <?php endif; ?>   

                <div class="TemplateRecetteDetail-backtohome" data-aos="fade-up">
                    <a href="<?php echo (get_permalink(1188)); ?>" class="Button">Toutes nos recettes</a>
                </div>

        </div>
        

    </section>

	</main>

<?php get_footer(); ?>
