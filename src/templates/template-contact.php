<?php

/*
 * Template Name: Template Contact
 */

$subtitlePage = get_field("subtitle_page");

$contactFaq = get_field("contact_faq");

$bloc1 = get_field('contact_bloc_1'); 
$bloc2 = get_field('contact_bloc_2');
?>

<?php get_header(); ?>

	<div id="primary" class="content-area">

		<main role="main" id="Main" class="TemplateContact">

			<!-- Container -->
			<div class="Container">

				<!-- section -->
				<section>

                <div class="PageTitle">
                    <h1 class="page-title"><?php the_title();?></h1>
                        <?php if($subtitlePage) : ?>
                            <p class="PageTitle-txt"><?php echo $subtitlePage; ?></p>
                        <?php endif; ?>
                </div>

					<!-- contenu -->
					<section id="post-<?php the_ID(); ?>" class="PageContent">

							<div class="Container">

                                <?php if($contactFaq) : ?>
                                    <?php echo($contactFaq); ?>
                                <?php endif; ?>

                                <div class="Row">

                                    <div class="Col Col-6 Col-xs-12">

                                        <div class="TemplateContact-bloc">

                                            <div class="TemplateContact-bloc-container">

                                                <?php if($bloc1['contact_bloc_1_image']['url']) : ?>
                                                    <div class="TemplateContact-bloc-img">
                                                        <img src="<?php echo esc_url( $bloc1['contact_bloc_1_image']['url'] ); ?>" alt="<?php echo esc_attr( $hero['contact_bloc_1_image']['alt'] ); ?>">
                                                    </div>
                                                <?php endif; ?>

                                                <?php if($bloc1['contact_bloc_1_txt']) : ?>   
                                                    <div class="TemplateContact-bloc-title">
                                                        <h2><?php echo esc_html( $bloc1['contact_bloc_1_txt'] ); ?></h2>
                                                    </div>
                                                <?php endif; ?>

                                                <div class="TemplateContact-bloc-cta">
                                                    <a href="<?php echo get_permalink(76); ?>" class="Button Button--white">Accéder au formulaire</a>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="Col Col-6 Col-xs-12">

                                        <div class="TemplateContact-bloc">

                                            <div class="TemplateContact-bloc-container">

                                                <?php if($bloc2['contact_bloc_2_image']['url']) : ?>
                                                    <div class="TemplateContact-bloc-img">
                                                        <img src="<?php echo esc_url( $bloc2['contact_bloc_2_image']['url'] ); ?>" alt="<?php echo esc_attr( $hero['contact_bloc_2_image']['alt'] ); ?>">
                                                    </div>
                                                <?php endif; ?>

                                                <?php if($bloc2['contact_bloc_2_txt']) : ?>   
                                                    <div class="TemplateContact-bloc-title">
                                                        <h2><?php echo esc_html( $bloc2['contact_bloc_2_txt'] ); ?></h2>
                                                    </div>
                                                <?php endif; ?>

                                                <div class="TemplateContact-bloc-cta">
                                                    <a href="<?php echo get_permalink(78); ?>" class="Button Button--white">Accéder au formulaire</a>
                                                </div>
                                            
                                            </div>
                                            
                                        </div>

                                    </div>

                                </div>

                            </div>

					</section>
					<!-- /contenu -->


				</section>
				<!-- /section -->

			</div>
			<!-- /Container -->

		</main>

	</div>

<?php get_footer(); ?>

