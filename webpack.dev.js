const { merge } = require("webpack-merge");
const path = require("path");
const { compiler, THEME_NAME } = require("./webpack.common.js");

const proxyConfig = {
  target: {
    host: "localhost",
    protocol: "http:",
    port: 80,
  },
  ignorePath: false,
  changeOrigin: true,
  secure: false,
};

module.exports = merge(compiler("development"), {
  target: "web",
  context: path.resolve(__dirname, "./"),
  devtool: "inline-source-map",
  devServer: {
    contentBase: `./dist/${THEME_NAME}/assets`,
    compress: false,
    proxy: {
      "/": proxyConfig,
      "**": proxyConfig,
    },
    writeToDisk: true,
  },
  plugins: [],
});
