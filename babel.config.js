module.exports = (api) => {
  return {
    plugins: ["babel-plugin-styled-components", "import-graphql"],
    presets: [
      [
        "@babel/preset-env",
        {
          corejs: "3.6",
          useBuiltIns: "usage",
          // caller.target will be the same as the target option from webpack
          targets: api.caller((caller) => caller && caller.target === "node")
            ? { node: "12" }
            : { ie: "10" },
        },
      ],
    ],
  };
};
